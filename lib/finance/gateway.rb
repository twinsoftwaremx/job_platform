module Finance

  class MissingOAuthToken < StandardError; end
  OAuthToken = QuickbooksAuthToken.first rescue nil

  class Gateway
    @plugins = []

    module ClassMethods
      attr_reader :plugins

      def use(plugin, *args, &block)
        unless @plugins.include? plugin
          @plugins << plugin
          extend  plugin::ClassMethods    if plugin.const_defined?(:ClassMethods)
          include plugin::InstanceMethods if plugin.const_defined?(:InstanceMethods)
        end
        self
      end

    end

    extend ClassMethods

    use self
    use Finance::Plugins::ConfigurationPlugin
    use Finance::Plugins::QuickbooksPlugin
    use Finance::Plugins::InvoicePlugin
    use Finance::Plugins::CustomerPlugin

    set :company_id,    OAuthToken.try(:company_id)    or raise MissingOAuthToken, 'No Quickbooks OAuth Company ID Found'
    set :access_token,  OAuthToken.try(:access_token)  or raise MissingOAuthToken, 'No Quickbooks OAuth Token Found'
    set :access_secret, OAuthToken.try(:access_secret) or raise MissingOAuthToken, 'No Quickbooks OAuth secret Found'
  end

end
