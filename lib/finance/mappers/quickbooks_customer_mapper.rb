module Finance
  module Mappers
    class QuickbooksCustomerMapper
      def self.from(employer_contact)
        Quickbooks::Model::Customer.new.tap do |c|
          c.company_name = employer_contact.account.company_name
          c.display_name = employer_contact.account.company_name
          c.primary_email_address = Quickbooks::Model::EmailAddress.new(employer_contact.user.email)
        end
      end
    end
  end
end
