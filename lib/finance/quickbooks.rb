module Finance
  class QuickbooksGateway
    def initialize(gateway, mapper: QuickbooksInvoiceMapper, service: Quickbooks::Service::Invoice)
      @gateway = gateway
      @service = service
      @mapper  = mapper
    end

    def create_invoice(fetch_invoice)
      quickbooks.create mapper.from(fetch_invoice)
    end

    def create_customer(employer_contact)
      quickbooks.create mapper.from(employer_contact)
    end

    def find_customer(company_name)
      quickbooks.find_by(:company_name, company_name)
    end

    def find_term(name)
      quickbooks.find_by(:name, name)
    end

  private
    attr_reader :gateway
    attr_reader :service
    attr_reader :mapper

    def quickbooks
      service.new.tap do |s|
        s.company_id   = gateway.config[:company_id]
        s.access_token = access_token
      end
    end

    def qb_oauth_consumer
      @qb_oauth_consumer ||= OAuth::Consumer.new \
        ENV.fetch('QUICKBOOKS_OAUTH_CONSUMER_KEY'),
        ENV.fetch('QUICKBOOKS_OAUTH_CONSUMER_SECRET'),
        site:               'https://oauth.intuit.com',
        request_token_path: '/oauth/v1/get_request_token',
        authorize_url:      'https://appcenter.intuit.com/Connect/Begin',
        access_token_path:  '/oauth/v1/get_access_token'
    end

    def access_token
      OAuth::AccessToken.new(qb_oauth_consumer, gateway.config[:access_token], gateway.config[:access_secret])
    end
  end
end
