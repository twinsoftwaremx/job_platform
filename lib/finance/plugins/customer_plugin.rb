module Finance
  class UnknownProviderError < StandardError; end

  module Plugins
    module CustomerPlugin
      module ClassMethods

        def create_customer(employer_contact, provider: :quickbooks)
          using_this(provider).create_customer(employer_contact)
        end

        def find_customer(company_name, provider: :quickbooks)
          using_this(provider).find_customer(company_name)
        end

      private

        def using_this(provider)
          raise UnknownProviderError, "#{provider.to_s.capitalize} not found" unless respond_to? "#{provider}_customer"
          method("#{provider}_customer").call
        end

      end
    end
  end
end
