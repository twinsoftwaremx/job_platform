module Finance
  class Gateway
    @config = {}
  end

  module Plugins
    module ConfigurationPlugin
      module ClassMethods
        attr_reader :config

        def set(key, value)
          @config[key] = value
          self
        end
      end
    end
  end
end
