module Finance
  module Plugins
    module QuickbooksPlugin
      module ClassMethods
        def quickbooks_invoice
          QuickbooksGateway.new self
        end

        def quickbooks_customer
          QuickbooksGateway.new self, mapper: Mappers::QuickbooksCustomerMapper, service: Quickbooks::Service::Customer
        end

        def quickbooks_term
          QuickbooksGateway.new self, service: Quickbooks::Service::Term
        end
      end
    end
  end
end
