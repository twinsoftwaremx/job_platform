module Finance

  class UnknownProviderError < StandardError; end

  module Plugins
    module InvoicePlugin
      module ClassMethods

        def create_invoice(invoice, provider: :quickbooks)
          raise UnknownProviderError, "#{provider.to_s.capitalize} not found" unless respond_to? "#{provider}_invoice"
          method("#{provider}_invoice").call.create_invoice(invoice)
        end

        def find_term(name, provider: :quickbooks)
          raise UnknownProviderError, "#{provider.to_s.capitalize} not found" unless respond_to? "#{provider}_term"
          method("#{provider}_term").call.find_term(name)
        end
      end
    end
  end
end
