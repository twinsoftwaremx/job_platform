module Finance

  class QuickbooksInvoiceMapper
    def self.from(invoice)
      Quickbooks::Model::Invoice.new.tap do |i|
        i.customer_id = invoice.customer_id
        i.txn_date    = invoice.due_at.to_date
        i.doc_number  = invoice.id
        i.sales_term_id = Finance::Gateway.find_term('Net 30').first.id
        i.line_items  << Quickbooks::Model::InvoiceLineItem.new.tap do |item|
          item.amount      = invoice.amount
          item.description = invoice.name
          item.sales_item! do |detail|
            detail.unit_price = invoice.amount
            detail.quantity   = 1
          end
        end
      end
    end
  end
end
