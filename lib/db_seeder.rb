class DBSeeder
  def self.seed!
    # Admin Users and Agents
    unless User.find_by(email: "admin@test.com")
      admin = AdminUser.create! email: "admin@test.com", password: "password", password_confirmation: "password"
    end

    jason = Agent.create! name: "Jason Hutson"
    jason.photo_id = self.add_asset("agent-pic.jpg", "Photo").id
    jason.save
    unless User.find_by(email: "jason_test@getfetched.com")
      AdminUser.create! email: "jason_test@getfetched.com", password: "password", password_confirmation: "password", agent: jason
    end

    taylor = Agent.create! name: "Taylor Gwaltney"
    taylor.photo_id = self.add_asset("taylor.jpeg", "Photo").id
    taylor.save
    unless User.find_by(email: 'taylor_test@getfetched.com')
      AdminUser.create! email: "taylor_test@getfetched.com", password: "password", password_confirmation: "password", agent: taylor
    end

    # Company Size
    ['Startup, < 16 employees', 'Small Business, 16-50 employees', 'Medium Size Company, 51-500 employees', 'Large Business, 501+ employees'].each do |text|
      CompanySize.create!(name: text)
    end

    # Salary Range
    i = 0
    ["$0 - $20k", "$20 - $40k", "$40 - $75k", "$75 - $100k", "$100 - $500k"].each do |salary|
      SalaryRange.find_or_create_by!(name: salary, compensation_type: 'Direct Hire', sort_order: i)
      i += 1
    end

    i = 0
    ["$0 - $10","$10 - $20", "$20 - $38", "$38 - $50", "$50 - $250"].each do |salary|
      SalaryRange.find_or_create_by!(name: "#{salary}/hr", compensation_type: 'Contract', sort_order: i)
      i += 1
    end

    # Employment Type
    ["Contract", "Direct Hire"].each do |et|
      EmploymentType.find_or_create_by! name: et
    end

    # Culture
    ["START UP", "PROFESSIONAL", "REMOTE"].each do |name|
      culture = Culture.create name: name
      asset_name = name.downcase.gsub(/\s+/, "")
      culture.photo_id = self.add_asset("culture-#{asset_name}.jpg", "Photo").id
      culture.save
    end

    %w(Birmingham Remote).each do |loc|
      InterestedLocation.first_or_create!(name: loc)
    end

    build_skill_categories
    build_skill_sets

    # States
    alabama = State.create(name: "Alabama", abbreviation: "AL")
    City.create(name: "Birmingham", state: alabama)
    City.create(name: "Huntsville", state: alabama)
    City.create(name: "Mobile", state: alabama)
    City.create(name: "Montgomery", state: alabama)
    tennessee = State.create(name: "Tennessee", abbreviation: "TN")
    City.create(name: "Nashville", state: tennessee)
    City.create(name: "Chattanooga", state: tennessee)
    luisiana = State.create(name: "Luisiana", abbreviation: "LA")
    City.create(name: "New Orleans", state: luisiana)
    georgia = State.create(name: "Georgia", abbreviation: "GA")
    City.create(name: "Atlanta", state: georgia)
    north_carolina = State.create(name: "North Carolina", abbreviation: "NC")
    City.create(name: "Charlotte", state: north_carolina)
    texas = State.create(name: "Texas", abbreviation: "TX")
    City.create(name: "Austin", state: texas)

    # Talents
    if Rails.env.development?
      user1 = User.create!(email: "talent1@test.com", password: "password", password_confirmation: "password", role: 'talent')
      profile = TalentProfile.new(
        name: "Chase Allen Hutson",
        summary: "Chief Development Officer of Janitorial Services",
        time_zone: "EST",
        agent: jason,
        salary_range: SalaryRange.last
      )
      profile.skill_list = "C/C++,Java,Swift"
      profile.culture_list = "START UP,PROFESSIONAL"
      l1 = Location.create! city: "Birmingham", state: "AL", zip: "35201"
      l2 = Location.create! city: "Huntsville", state: "AL", zip: "35801"
      profile.locations_of_interest << [l1, l2]
      profile.cities_of_interest << [City.find_by(name: "Birmingham"), City.find_by(name: "Huntsville")]
      profile.photo_id = self.add_asset("avatar1.png", "Photo").id
      profile.resume_id = self.add_asset("resume1.jpg", "Resume").id
      profile.save
      user1.talent_profile = profile

      user2 = User.create! email: "talent2@test.com", password: "password", password_confirmation: "password"
      profile2 = TalentProfile.new(
        name: "John Doe",
        summary: "Senior Web Developer",
        time_zone: "EST",
        agent: jason,
        salary_range: SalaryRange.last
      )
      profile2.skill_list = "C/C++,Ruby,Python"
      profile2.cities_of_interest << City.find_by(name: "Birmingham")
      profile.salary_range = SalaryRange.first
      profile2.save
      user2.talent_profile = profile2

      user3 = User.create! email: "talent3@test.com", password: "password", password_confirmation: "password"
      profile3 = TalentProfile.new(
        name: "Mike Smith",
        summary: "Front end developer",
        time_zone: "EST",
        agent: jason,
        salary_range: SalaryRange.first
      )
      profile3.skill_list = "JavaScript,CSS,HTML"
      profile3.cities_of_interest << City.find_by(name: "Birmingham")
      profile3.save
      user3.talent_profile = profile3

      # Accounts

      user = User.create! email: "employer_shell@test.com", password: "password", password_confirmation: "password", role: "account_admin"
      account = Account.create! company_name: "Shell", agent: jason, logo_id: self.add_asset("shell.jpeg", "Photo").id
      account.who_we_are = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In dictum, eros sit amet blandit pellentesque, sapien sapien tempus diam, at condimentum leo eros vitae lacus. Sed dolor lacus, dapibus a lectus at, molestie varius justo. Proin ut purus a ipsum eleifend pretium vel sit amet nibh. Sed varius sem id lacus lobortis volutpat. Morbi maximus mi lobortis ante accumsan viverra. Nam ut est condimentum, mattis lorem non, tristique quam. Nam gravida arcu arcu, in volutpat mauris aliquet ut. Praesent fringilla, mauris a finibus accumsan, ipsum neque dapibus libero, vitae iaculis felis nisi a ligula. Maecenas eu eleifend nibh. Aenean sodales blandit libero non ultrices. Cras facilisis et nunc a eleifend. Vestibulum et est metus. Integer et risus nec sapien sagittis vulputate mattis nec magna."
      account.what_we_do = "Donec ac bibendum lorem. Integer nec fringilla velit. Nam sem nisl, viverra vitae sodales non, blandit at quam. Praesent ac dui sagittis, blandit massa vitae, ultrices lorem. Ut pulvinar condimentum arcu ac dapibus. Suspendisse id maximus diam, at fermentum mi. Donec consectetur enim eu interdum ullamcorper. Mauris ac mattis leo, ut hendrerit urna."
      account.perks = "Praesent imperdiet suscipit magna eu tristique. Vivamus bibendum lorem nec risus sollicitudin, vel malesuada purus fringilla. Quisque consectetur lorem quis urna convallis, sagittis tempus diam eleifend. Suspendisse potenti. Morbi eu aliquam erat. Ut sollicitudin ante non pellentesque facilisis. Praesent auctor, mauris eget posuere blandit, neque orci efficitur lectus, non convallis nibh felis et est. Nunc viverra faucibus massa in dignissim."
      account.company_site_url = "www.shellcorp.com"
      account.year_founded = 1996
      account.company_size = CompanySize.where(name:"Series A companies, 16-50 employees").first
      account.revenue = "$1.5 MILLION"
      account.address = FullAddress.create(city: 'Birmingham', state: 'AL', zip: '35201')
      account.save
      account.culture_list = "START UP,PROFESSIONAL"
      ec = EmployerContact.create! user: user, account: account, name: "Joe Shell", title: "CTO", summary: "Information Technology executive, proven strategist and business partner with over 25 years combined online and B2C experience."
      position = Position.new(
        employment_type: EmploymentType.first,
        employer_contact: ec,
        job_title: "Ice Cream Taster",
        salary_range: SalaryRange.last,
        location: l1,
        city: City.find_by(name: "Birmingham"), description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi in metus eget ante egestas commodo. Proin vitae purus nec magna lobortis consectetur nec cursus leo. Sed laoreet dignissim lectus, tristique rhoncus dui semper nec. Cras vestibulum, diam sit amet bibendum tristique, sapien dui pulvinar eros, id facilisis justo tortor et odio. Donec finibus lorem ipsum, malesuada blandit quam placerat at. Aenean sodales nulla in velit viverra, ac dictum leo vestibulum. Nunc sit amet cursus neque, varius sollicitudin orci."
      )
      position.required_skill_list = "C/C++,Java,Swift"
      ec.photo_id = self.add_asset("avatar1.png", "Photo").id
      position.save

      position = Position.new(
        employment_type: EmploymentType.first,
        employer_contact: ec,
        job_title: "Beer drinker",
        salary_range: SalaryRange.last,
        location: l1,
        city: City.find_by(name: "Birmingham")
      )
      position.required_skill_list = "JavaScript,HTML,CSS"
      position.save

      user = User.create! email: "employer_rs@test.com", password: "password", password_confirmation: "password", role: "account_admin"
      account = Account.create! company_name: "Rolling Stone", agent: jason, logo_id: self.add_asset("rollingstone.jpeg", "Photo").id
      account.culture_list = "START UP,PROFESSIONAL"
      ec = EmployerContact.create! user: user, account: account, name: "Roberto Stone"
      position = Position.new(
        employment_type: EmploymentType.first,
        employer_contact: ec,
        job_title: "Coffee Retriever",
        salary_range: SalaryRange.last,
        location: l2,
        city: City.find_by(name: "Birmingham")
      )
      position.required_skill_list = "C/C++,Java,Swift"
      position.save

      user = User.create! email: "employer_vw@test.com", password: "password", password_confirmation: "password", role: "account_admin"
      account = Account.create! company_name: "Volkswagon", agent: jason, logo_id: self.add_asset("volkswagon.jpeg", "Photo").id
      account.culture_list = "START UP,PROFESSIONAL"
      ec = EmployerContact.create! user: user, account: account, name: "Mike Volks"
      position = Position.new(
        employment_type: EmploymentType.first,
        employer_contact: ec,
        job_title: "Laidenhosen seamstress",
        salary_range: SalaryRange.last,
        location: l2,
        city: City.find_by(name: "Birmingham")
      )
      position.required_skill_list = "C/C++,Java,Swift"
      position.save

      user = User.create! email: "employer_nbc@test.com", password: "password", password_confirmation: "password", role: "account_admin"
      account = Account.create! company_name: "NBC", agent: jason, logo_id: self.add_asset("nbc.png", "Photo").id
      account.culture_list = "START UP,PROFESSIONAL"
      ec = EmployerContact.create! user: user, account: account, name: "Randy News"
      position = Position.new(
        employment_type: EmploymentType.first,
        employer_contact: ec,
        job_title: "Intern",
        salary_range: SalaryRange.last,
        location: l1,
        city: City.find_by(name: "Birmingham")
      )
      position.required_skill_list = "C/C++,Java,Swift"
      position.save
    end

    Position.import
    TalentProfile.import
    sleep 1

    TalentProfile.all.each do |talent|
      MatchFinder.call(talent)
    end
  end

  def self.reset_db!

    # Models
    Dir[Rails.root.join('app/models/*.rb').to_s].each do |filename|
      klass = File.basename(filename, '.rb').camelize.constantize
      next unless klass.ancestors.include?(ActiveRecord::Base)
      klass.destroy_all
      klass.unscoped.all.each(&:really_destroy!) if klass.respond_to?(:with_deleted)
      ActiveRecord::Base.connection.reset_pk_sequence!("#{klass.table_name}")
    end

    Position.__elasticsearch__.create_index! force: true
    Position.__elasticsearch__.refresh_index!
    TalentProfile.__elasticsearch__.create_index! force: true
    TalentProfile.__elasticsearch__.refresh_index!
  end

  def self.add_asset(asset_file, type)
    file = File.open(File.join(Rails.root, "lib", "seeds", asset_file))
    asset = Asset.create! file: file, type: type
    asset
  end

  def self.build_skill_categories
    ['Software Developer', 'Helpdesk Support', 'Systems Engineer', 'Network Engineer', 'DBA', 'Virtualization Architect', 'Project Management', 'Business Analyst', 'IT Management (CIO/CTO/Director of IT)', 'Creative Director', 'Graphic Designer'].each do |text|
      SkillCategory.create(name: text)
    end
  end

  def self.build_skill_sets
    ['Java', 'C/C++', 'C#', '.NET', 'PHP', 'Ruby', 'Python', 'Perl', 'Swift', 'JavaScript', 'HTML', 'CSS', 'Scala', 'Elixir'].each do |skill|
      Skill.create! name: skill, skill_category: SkillCategory.find_by(name: 'Software Developer')
    end

    ['Phone/Remote Support', '50 or less calls per day', '100+ calls per day', 'Microsoft Office Suite', 'Windows', 'Apple'].each do |skill|
      Skill.create! name: skill, skill_category: SkillCategory.find_by(name: 'Helpdesk Support')
    end

    ['Desktop Support', 'Server Administration', 'Microsoft Products', 'Apple Products', 'Network Support', 'Printer Support'].each do |skill|
      Skill.create! name: skill, skill_category: SkillCategory.find_by(name: 'Systems Engineer')
    end

   ['Routing', 'Switching', 'Security/Firewall', 'VoIP', 'CCNA', 'CCENT', 'CCNP', 'CCIE', 'Cisco', 'HP', 'Dell'].each do |skill|
     Skill.create! name: skill, skill_category: SkillCategory.find_by(name: 'Network Engineer')
   end

   ['SQL', 'Oracle', 'PostgreSQL', 'MongoDB', 'MySQL'].each do |skill|
     Skill.create! name: skill, skill_category: SkillCategory.find_by(name: 'DBA')
   end

   ['VMware', 'Azure', 'Amazon', 'Citrix', 'Cisco UCS', 'iSCSI', 'Fiber', 'Local Storage'].each do |skill|
     Skill.create! name: skill, skill_category: SkillCategory.find_by(name: 'Virtualization Architect')
   end

   ['Agile', 'Waterfall', 'Scrum', 'PMP Certification'].each do |skill|
     Skill.create! name: skill, skill_category: SkillCategory.find_by(name: 'Project Management')
   end

   ['Requirement Gathering', 'Business Processes', 'Design', 'Mapping', 'Use Cases'].each do |skill|
     Skill.create! name: skill, skill_category: SkillCategory.find_by(name: 'Business Analyst')
   end

   ['Managed 5 or less engineers', 'Managed 10+ engineers', 'Managed 50+ engineers', 'Budgeting', 'Purchasing', 'Vendor Management', 'Prior Development Experience', 'Prior Infrastructure Experience'].each do |skill|
     Skill.create! name: skill, skill_category: SkillCategory.find_by(name: 'IT Management (CIO/CTO/Director of IT)')
   end

   ['Adobe Products', 'UI/UX Experience', 'Mobile Responsiveness', 'Design', 'Motion Graphics ', 'Cinematic Animation', 'Managed Design Teams'].each do |skill|
     Skill.create! name: skill, skill_category: SkillCategory.find_by(name: 'Creative Director')
   end

   ['Adobe Creative Suite', 'Adobe Acrobat', 'Adobe Flash', 'Adobe Illustrator', 'Adobe InDesign', 'Adobe Photoshop', 'CSS', 'Dreamweaver', 'HTML', 'Photoshop', 'QuarkXpress', 'Typography', 'UI Design', 'UX Design'].each do |skill|
     Skill.create! name: skill, skill_category: SkillCategory.find_by(name: 'Graphic Designer')
   end
  end
end
