$qb_oauth_consumer = OAuth::Consumer.new \
    ENV['QUICKBOOKS_OAUTH_CONSUMER_KEY'],
    ENV['QUICKBOOKS_OAUTH_CONSUMER_SECRET'],
    site:               'https://oauth.intuit.com',
    request_token_path: '/oauth/v1/get_request_token',
    authorize_url:      'https://appcenter.intuit.com/Connect/Begin',
    access_token_path:  '/oauth/v1/get_access_token'

if Rails.env.development? || ENV['FETCH_ENV'] == 'staging'
  Quickbooks.sandbox_mode = true
end
