require_relative '../lib/db_seeder'

if Rails.env.development? || ENV['FETCH_ENV'] == 'staging'
  DBSeeder.reset_db!
  DBSeeder.seed!
end
