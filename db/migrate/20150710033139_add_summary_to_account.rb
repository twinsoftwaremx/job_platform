class AddSummaryToAccount < ActiveRecord::Migration
  def change
    add_column :accounts, :summary, :text
  end
end
