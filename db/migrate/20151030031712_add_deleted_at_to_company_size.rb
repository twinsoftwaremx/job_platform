class AddDeletedAtToCompanySize < ActiveRecord::Migration
  def change
    add_column :company_sizes, :deleted_at, :datetime
    add_index :company_sizes, :deleted_at
  end
end
