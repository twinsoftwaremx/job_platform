class AddDeletedAtToSkillAssociations < ActiveRecord::Migration
  def change
    add_column :skill_associations, :deleted_at, :datetime
    add_index :skill_associations, :deleted_at
  end
end
