class CreateVerticalMarkets < ActiveRecord::Migration
  def change
    create_table :vertical_markets do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
