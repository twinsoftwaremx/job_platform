class AddUserIdTypesToMessages < ActiveRecord::Migration
  def change
    add_column :messages, :from_user_type, :string, default: 'User'
    add_column :messages, :to_user_type, :string, default: 'User'
  end
end
