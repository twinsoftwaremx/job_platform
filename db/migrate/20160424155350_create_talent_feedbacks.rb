class CreateTalentFeedbacks < ActiveRecord::Migration
  def change
    create_table :talent_feedbacks do |t|
      t.text :text
      t.integer :talent_profile_id

      t.timestamps null: false
    end
    add_index :talent_feedbacks, :talent_profile_id
  end
end
