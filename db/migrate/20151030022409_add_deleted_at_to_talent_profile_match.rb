class AddDeletedAtToTalentProfileMatch < ActiveRecord::Migration
  def change
    add_column :talent_profile_matches, :deleted_at, :datetime
    add_index :talent_profile_matches, :deleted_at
  end
end
