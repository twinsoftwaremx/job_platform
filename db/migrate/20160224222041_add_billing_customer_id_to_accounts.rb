class AddBillingCustomerIdToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :billing_customer_id, :string
  end
end
