class AddDeletedAtToTalents < ActiveRecord::Migration
  def change
    add_column :talents, :deleted_at, :datetime
    add_index :talents, :deleted_at
  end
end
