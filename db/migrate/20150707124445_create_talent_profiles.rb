class CreateTalentProfiles < ActiveRecord::Migration
  def change
    create_table :talent_profiles do |t|
      t.string :name, null: false
      t.integer :photo_id
      t.integer :resume_id
      t.string :time_zone, null: false
      t.string :phone
      t.text :summary
      t.string :area_of_interest
      t.integer :years_of_experience, default: 0
      t.boolean :usdod_clearance, default: false
      t.text :reason_for_interest_in_new_job_opportunities
      t.integer :job_search_status, default: 0
      t.string :preferred_base_salary
      t.integer :usa_work_auth_type, default: 0
      t.string :linkdin_url
      t.string :personal_website
      t.string :dribble_username
      t.string :so_url
      t.string :blog_url

      t.timestamps null: false
    end
  end
end
