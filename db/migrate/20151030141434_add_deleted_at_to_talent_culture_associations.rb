class AddDeletedAtToTalentCultureAssociations < ActiveRecord::Migration
  def change
    add_column :talent_culture_associations, :deleted_at, :datetime
    add_index :talent_culture_associations, :deleted_at
  end
end
