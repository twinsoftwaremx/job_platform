class CreateQuickbooksAuthTokens < ActiveRecord::Migration
  def change
    create_table :quickbooks_auth_tokens do |t|
      t.string   :access_token,  null: false
      t.string   :access_secret, null: false
      t.string   :company_id,    null: false
      t.datetime :expires_at,    null: false, default: 6.months.from_now
      t.datetime :reconnect_at,  null: false, default: 5.months.from_now
      t.timestamps
    end
  end
end
