class RemoveCultureFromAccounts < ActiveRecord::Migration
  def change
    remove_column :accounts, :culture
  end
end
