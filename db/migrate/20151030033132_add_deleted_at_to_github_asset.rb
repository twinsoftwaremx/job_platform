class AddDeletedAtToGithubAsset < ActiveRecord::Migration
  def change
    add_column :github_assets, :deleted_at, :datetime
    add_index :github_assets, :deleted_at
  end
end
