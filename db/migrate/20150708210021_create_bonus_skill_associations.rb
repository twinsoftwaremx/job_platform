class CreateBonusSkillAssociations < ActiveRecord::Migration
  def change
    create_table :bonus_skill_associations do |t|
      t.integer :position_id
      t.integer :skill_id

      t.timestamps null: false
    end

    add_index :bonus_skill_associations, :position_id
    add_index :bonus_skill_associations, :skill_id
  end
end
