class CreatePreviousEmployments < ActiveRecord::Migration
  def change
    create_table :previous_employments do |t|
      t.string :company
      t.string :position
      t.date :start_date
      t.date :end_date
      t.text :description

      t.timestamps null: false
    end
  end
end
