class AddSortOrderToSalaryRange < ActiveRecord::Migration
  def change
    add_column :salary_ranges, :sort_order, :integer
  end
end
