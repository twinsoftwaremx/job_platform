class AddDeletedAtToAccountCultureAssociation < ActiveRecord::Migration
  def change
    add_column :account_culture_associations, :deleted_at, :datetime
    add_index :account_culture_associations, :deleted_at
  end
end
