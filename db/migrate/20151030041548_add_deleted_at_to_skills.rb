class AddDeletedAtToSkills < ActiveRecord::Migration
  def change
    add_column :skills, :deleted_at, :datetime
    add_index :skills, :deleted_at
  end
end
