class AddDeletedAtToLocationOfInterestAssociations < ActiveRecord::Migration
  def change
    add_column :location_of_interest_associations, :deleted_at, :datetime
    add_index :location_of_interest_associations, :deleted_at
  end
end
