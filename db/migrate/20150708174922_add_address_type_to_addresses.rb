class AddAddressTypeToAddresses < ActiveRecord::Migration
  def change
    add_column :addresses, :address_type, :string, default: :full_address
  end
end
