class CreateLocationOfInterestAssociations < ActiveRecord::Migration
  def change
    create_table :location_of_interest_associations do |t|
      t.integer :talent_profile_id
      t.integer :interested_location_id

      t.timestamps null: false
    end
    add_index :location_of_interest_associations, :talent_profile_id
    add_index :location_of_interest_associations, :interested_location_id, name: "idx_loc_int_on_intlocid"
  end
end
