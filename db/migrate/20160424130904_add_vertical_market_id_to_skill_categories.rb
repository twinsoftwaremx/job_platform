class AddVerticalMarketIdToSkillCategories < ActiveRecord::Migration
  def change
    add_column :skill_categories, :vertical_market_id, :integer, default: nil
    add_index :skill_categories, :vertical_market_id
  end
end
