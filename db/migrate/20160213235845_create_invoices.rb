class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.references :offer,    index: true
      t.string     :name,     null:  false
      t.integer    :category, null:  false
      t.integer    :status,   null:  false, default: 0
      t.decimal    :amount,   null:  false, default: 0.0
      t.timestamps null: false
      t.datetime   :due_at,   null:  false
      t.datetime   :paid_at
      t.datetime   :deleted_at
    end
  end
end
