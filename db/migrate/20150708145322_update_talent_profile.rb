class UpdateTalentProfile < ActiveRecord::Migration
  def change
    rename_column(:talent_profiles, :linkdin_url, :linked_in_url)
    rename_column(:talent_profiles, :so_url, :stack_overflow_url)
    rename_column(:talent_profiles, :dribble_username ,:dribbble_url)
  end
end
