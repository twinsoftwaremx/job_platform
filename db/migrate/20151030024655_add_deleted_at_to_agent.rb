class AddDeletedAtToAgent < ActiveRecord::Migration
  def change
    add_column :agents, :deleted_at, :datetime
    add_index :agents, :deleted_at
  end
end
