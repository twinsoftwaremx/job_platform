class AddAccountFieldsForSignup < ActiveRecord::Migration
  def change
    add_column :accounts, :year_founded, :integer
    add_column :accounts, :revenue, :string
    add_column :accounts, :who_we_are, :text
    add_column :accounts, :what_we_do, :text
  end
end
