class AddPhotoIdToCultures < ActiveRecord::Migration
  def change
    add_column :cultures, :photo_id, :integer
  end
end
