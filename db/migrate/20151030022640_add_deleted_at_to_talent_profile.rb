class AddDeletedAtToTalentProfile < ActiveRecord::Migration
  def change
    add_column :talent_profiles, :deleted_at, :datetime
    add_index :talent_profiles, :deleted_at
  end
end
