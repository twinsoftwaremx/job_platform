class AddAuthTokenToAdminUsers < ActiveRecord::Migration
  def change
    add_column :admin_users, :auth_token, :string, default: ""
    add_index :admin_users, :auth_token, unique: true
  end
end
