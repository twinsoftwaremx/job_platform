class RemoveInterestedLocation < ActiveRecord::Migration
  def up
    rename_column :location_of_interest_associations, :interested_location_id, :location_id
    add_column :accounts, :address_id, :integer
    add_column :talent_profiles, :location_id, :integer
    add_column :positions, :location_id, :integer
    remove_column :addresses, :addressable_id
    remove_column :addresses, :addressable_type
  end

  def down
    rename_column :location_of_interest_associations, :location_id, :interested_location_id
    remove_column :accounts, :address_id
    remove_column :talent_profiles, :location_id
    remove_column :positions, :location_id
    add_column :addresses, :addressable_id, :integer
    add_column :addresses, :addressable_type, :string
  end
end
