class AddProfileToLinkedinAssets < ActiveRecord::Migration
  def change
    add_column :linkedin_assets, :profile, :json
  end
end
