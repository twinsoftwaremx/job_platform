class AddDeletedAtToLinkedinAssets < ActiveRecord::Migration
  def change
    add_column :linkedin_assets, :deleted_at, :datetime
    add_index :linkedin_assets, :deleted_at
  end
end
