class AddDeletedAtToEmploymentType < ActiveRecord::Migration
  def change
    add_column :employment_types, :deleted_at, :datetime
    add_index :employment_types, :deleted_at
  end
end
