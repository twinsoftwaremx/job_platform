class AddDeclineReasonToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :decline_reason, :text
  end
end
