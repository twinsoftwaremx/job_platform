class AddCompensationTypeToSalaryRanges < ActiveRecord::Migration
  def change
    add_column :salary_ranges, :compensation_type, :string, :null => false, :default => 'Direct Hire'
  end
end
