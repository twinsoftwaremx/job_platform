class AddDeletedAtToVerticalMarkets < ActiveRecord::Migration
  def change
    add_column :vertical_markets, :deleted_at, :datetime
    add_index :vertical_markets, :deleted_at
  end
end
