class AddDeletedAtToRequiredSkillAssociations < ActiveRecord::Migration
  def change
    add_column :required_skill_associations, :deleted_at, :datetime
    add_index :required_skill_associations, :deleted_at
  end
end
