class AddTitleAndCompanyToTalentProfiles < ActiveRecord::Migration
  def change
    add_column :talent_profiles, :title, :string
    add_column :talent_profiles, :company, :string
  end
end
