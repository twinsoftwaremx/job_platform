class AddDeletedAtToSkillCategories < ActiveRecord::Migration
  def change
    add_column :skill_categories, :deleted_at, :datetime
    add_index :skill_categories, :deleted_at
  end
end
