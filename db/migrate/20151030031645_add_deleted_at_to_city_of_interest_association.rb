class AddDeletedAtToCityOfInterestAssociation < ActiveRecord::Migration
  def change
    add_column :city_of_interest_associations, :deleted_at, :datetime
    add_index :city_of_interest_associations, :deleted_at
  end
end
