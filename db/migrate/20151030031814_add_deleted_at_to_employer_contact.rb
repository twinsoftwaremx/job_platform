class AddDeletedAtToEmployerContact < ActiveRecord::Migration
  def change
    add_column :employer_contacts, :deleted_at, :datetime
    add_index :employer_contacts, :deleted_at
  end
end
