class CreateAccountCultureAssociations < ActiveRecord::Migration
  def change
    create_table :account_culture_associations do |t|
      t.integer :culture_id
      t.integer :account_id

      t.timestamps null: false
    end
    add_index :account_culture_associations, :culture_id
    add_index :account_culture_associations, :account_id
  end
end
