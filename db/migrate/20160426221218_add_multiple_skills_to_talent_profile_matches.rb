class AddMultipleSkillsToTalentProfileMatches < ActiveRecord::Migration
  def change
    add_column :talent_profile_matches, :multiple_skills, :boolean, default: true
  end
end
