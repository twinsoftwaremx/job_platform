class CreateInterestAssociations < ActiveRecord::Migration
  def change
    create_table :interest_associations do |t|
      t.integer :talent_profile_id
      t.integer :interest_id

      t.timestamps null: false
    end
    add_index :interest_associations, :talent_profile_id
    add_index :interest_associations, :interest_id
  end
end
