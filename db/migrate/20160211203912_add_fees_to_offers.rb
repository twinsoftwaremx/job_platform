class AddFeesToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :upfront_fee,   :decimal, default: 0.0
    add_column :offers, :recurring_fee, :decimal, default: 0.0
  end
end
