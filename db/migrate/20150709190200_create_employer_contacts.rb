class CreateEmployerContacts < ActiveRecord::Migration
  def change
    create_table :employer_contacts do |t|
      t.string :name, null: false
      t.integer :photo_id
      t.string :title
      t.string :linked_in_url
      t.text :summary
      t.string :phone
      t.string :time_zone
      t.integer :account_id, null: false

      t.timestamps null: false
    end

    add_index :employer_contacts, :photo_id
    add_index :employer_contacts, :account_id
  end
end
