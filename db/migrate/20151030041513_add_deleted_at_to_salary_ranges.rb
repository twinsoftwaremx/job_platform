class AddDeletedAtToSalaryRanges < ActiveRecord::Migration
  def change
    add_column :salary_ranges, :deleted_at, :datetime
    add_index :salary_ranges, :deleted_at
  end
end
