class AddCultureToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :culture, :int, default: 0
  end
end
