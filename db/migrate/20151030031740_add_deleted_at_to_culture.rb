class AddDeletedAtToCulture < ActiveRecord::Migration
  def change
    add_column :cultures, :deleted_at, :datetime
    add_index :cultures, :deleted_at
  end
end
