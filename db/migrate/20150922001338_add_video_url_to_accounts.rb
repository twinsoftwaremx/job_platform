class AddVideoUrlToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :video_url, :string
  end
end
