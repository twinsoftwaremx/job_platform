class AddLastActivityAtToAdminUsers < ActiveRecord::Migration
  def change
    add_column :admin_users, :last_activity_at, :datetime

    add_index :admin_users, :last_activity_at
  end
end
