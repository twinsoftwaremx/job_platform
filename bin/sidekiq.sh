#!/bin/bash

env_variables=`cat .env`

for line in $env_variables
  do export $line
done

bundle exec $1 -e $2
