class Finance::CreateCustomer < Imperator::Command
  attribute :employer_contact_id

  def action
    begin
      Finance::Gateway.create_customer(employer_contact).tap do |customer|
        employer_contact.account.billing_customer_id = customer.id
        employer_contact.account.save!
      end
    rescue => e
      Bugsnag.notify(e)
    end
  end

private
  def employer_contact
    @employer_contact ||= EmployerContact.find(employer_contact_id)
  end
end
