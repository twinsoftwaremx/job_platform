class SidekiqBackgroundProcessor
  def self.commit(command, options = nil)
    CommandWorker.perform_async(command.class.to_s, command.attributes)
  end
end
