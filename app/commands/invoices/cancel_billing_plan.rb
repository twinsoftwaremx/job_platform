class Invoices::CancelBillingPlan < Imperator::Command

  attribute :offer_id

  def action
    offer.invoices.delete_all
  end

private
  def offer
    @offer ||= Offer.includes(:invoices).find(offer_id)
  end
end
