class Invoices::CreateInvoice < Imperator::Command
  attribute :invoice_id

  def action
    Finance::Gateway.create_invoice(invoice)
  end

private
  def invoice
    @invoice ||= Invoice.find(invoice_id)
  end
end
