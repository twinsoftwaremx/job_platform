class Invoices::ScheduleBillingPlan < Imperator::Command

  attribute :offer_id

  def action
    schedule_upfront
    schedule_recurring
    offer.save!
  end

private
  
  def schedule_upfront
    offer.invoices << Invoice.new.tap do |invoice|
      invoice.name     = 'Upfront Fee'
      invoice.amount   = UpfrontFee.calculate(offer.salary_pay_rate)
      invoice.due_at   = Time.parse(offer.start_date)
      invoice.category = Invoice.categories[:upfront]
      invoice.status   = Invoice.statuses[:due]
    end
  end

  def schedule_recurring
    return unless offer.direct_hire?
    RecurringFee.term.times do |term|
      offer.invoices << Invoice.new.tap do |invoice|
        due_at           = Time.parse(offer.start_date) + (term + 1).months
        invoice.name     = "Recurring Fee #{due_at.strftime('%B - %Y')}"
        invoice.amount   = RecurringFee.calculate(offer.salary_pay_rate)
        invoice.due_at   = due_at
        invoice.category = Invoice.categories[:recurring]
        invoice.status   = Invoice.statuses[:due]
      end
    end
  end

  def offer
    @offer ||= Offer.find(offer_id)
  end
end
