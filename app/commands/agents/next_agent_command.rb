class Agents::NextAgentCommand < Imperator::Command
  action do
    if TalentProfile.any?
      agent = TalentProfile.last.try(:agent) || Agent.last
      index = Agent.all.index(agent)
      Agent.all[index + 1] || Agent.first
    else
      Agent.first
    end
  end
end
