class TalentProfileMatchPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def like?
    if user.talent_profile.talent_profile_match_ids.include?(record.id)
      true
    else
      false
    end
  end

  def reject?
    if user.talent_profile.talent_profile_match_ids.include?(record.id)
      true
    else
      false
    end
  end

  def interest?
    (user.employer_contact.present? && record.position.employer_contact == user.employer_contact)
  end
end
