class PositionPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      if @user.has_role? :employer_contact
        employer_contact_id = @user.employer_contact.id
        scope.where(:employer_contact_id == employer_contact_id)
      else
        scope.all
      end
    end
  end

  def show?
    true
  end

  def update?
    is_owner?
  end

  def destroy?
    is_owner?
  end

  private
  def is_owner?
    @user == @record.employer_contact.user
  end
end
