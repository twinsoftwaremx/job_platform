class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@api.fetch.com'
  layout 'mailer'

  def attach_logo
    logo_path = Rails.root.join('app', 'assets', 'images', 'fetch-logo.png')
    attachments.inline['fetch-logo.png'] = File.read(logo_path)
  end

  def attach_fetch_process
    logo_path = Rails.root.join('app', 'assets', 'images', 'fetch-process.png')
    attachments.inline['fetch-process.png'] = File.read(logo_path)
  end

  def attach_date
    @current_date = Date.today.strftime('%B %d, %Y')
  end
end
