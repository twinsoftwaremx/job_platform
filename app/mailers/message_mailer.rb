class MessageMailer < ApplicationMailer
  include MessageMailerHelper

  def send_test(email)
    attach_logo
    @email = email
    mail(to: @email, subject: 'Welcome to My Awesome Site')
  end

  def notify_users(message)
    attach_logo

    @from         = from(message)
    @message_url  = frontend_link_to(message)
    @message_body = message.body
    to            = message.to_user

    mail(to: to.email, subject: "You've received a message on Fetch!")
  end

private
  def from(message)
    user = message.from_user

    return user.agent.try(:name)          if user.is_a? AdminUser
    return user.talent_profile.try(:name) if user.has_role? :talent
    user.employer_contact.try(:name)
  end
end
