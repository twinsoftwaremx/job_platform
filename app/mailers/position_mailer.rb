class PositionMailer < ApplicationMailer
  def position_removed(position, talent)
    @position = position
    @talent = talent
    attach_logo
    mail(to: talent.user.email, subject: 'Fetch Job Deleted')
  end
end
