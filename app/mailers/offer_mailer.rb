class OfferMailer < ApplicationMailer
  helper ActionView::Helpers::NumberHelper

  def email_make_offer(offer)
    attach_logo
    populate_talent(offer)

    mail(to: @talent.user.email, subject: 'You have received a job offer!')
  end

  def email_accept_offer(offer, recipient)
    attach_logo
    populate_talent(offer)
    @recipient = recipient

    if recipient == 'employer'
      mail(to: @employer.user.email, subject: 'Talent accepted the offer!')
    else
      mail(to: @talent.user.email, subject: 'You accepted the offer!')
    end
  end

  def email_reject_offer(offer)
    attach_logo
    populate_talent(offer)

    talent_user_id = @talent.user.id
    match = offer.talent_profile_match
    user = @employer.user

    parameter_string = "user=#{user.email}&role=employer&screen=messages&position_id=#{@position.id}&match_id=#{match.id}&other_user_id=#{talent_user_id}"
    parameter = Base64.encode64(parameter_string).chomp!
    @chat_url = ENV['FRONTEND_URL'] + "#/from_email?data=#{parameter}"

    mail(to: @employer.user.email, subject: 'Talent has declined an offer!')
  end

  private

  def populate_talent(offer)
    @talent = offer.talent_profile_match.talent_profile
    @position = offer.talent_profile_match.position
    @employer = offer.employer_contact
    @current_date = DateTime.now.strftime('%^B %-d, %Y')
    @timestamp = DateTime.now.to_s

    @talent_first_name = @talent.name.present? ? @talent.name.split(' ')[0] : ''
    @talent_full_name = @talent.name.present? ? @talent.name : ''
    @talent_title = @talent.title.present? ? @talent.title : ''
    @talent_photo_url = @talent.photo.present? ? @talent.photo.url : ''
    @employer_contact_name = @employer.name
    @account_logo_url = @position.account.logo.present? ? @position.account.logo.url : ''
    @position_name = @position.job_title
    @company_name = @position.account.company_name
    @location = @position.city.full_name
    @compensation = offer.salary_pay_rate
    @start_date = DateTime.parse(offer.start_date).strftime('%m/%d/%Y')
  end
end
