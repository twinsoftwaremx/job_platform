class FetchMailer < ApplicationMailer
  def welcome_talent(talent_email)
    attach_logo
    attach_date
    attach_fetch_process

    @dashboard_url = "#{ENV['FRONTEND_URL']}/talent-dashboard.html"
    mail(to: talent_email, subject: 'Welcome to Fetch!')
  end

  def welcome_employer(employer_email)
    attach_logo
    attach_date
    attach_fetch_process

    @dashboard_url = "#{ENV['FRONTEND_URL']}/employer-dashboard.html"
    mail(to: employer_email, subject: 'Welcome to Fetch!')
  end
end
