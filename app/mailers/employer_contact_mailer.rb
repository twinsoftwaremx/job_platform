class EmployerContactMailer < ApplicationMailer
  layout false

  def invite_coworker(email, link)
    @link = link
    @timestamp = DateTime.now.to_s

    attach_logo

    mail(to: email, subject: 'Invitation to register on Fetch!')
  end
end
