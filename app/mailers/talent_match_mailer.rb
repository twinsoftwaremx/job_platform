class TalentMatchMailer < ApplicationMailer
  helper WeeklyMailerHelper

  def send_test(email)
    attach_logo
    @email = email
    mail(to: @email, subject: 'Welcome to My Awesome Site')
  end

  def email_talent_match(talent, match)
    attach_logo
    @talent = talent
    @position = match.position
    @account = match.position.account
    @primary_skill = ''
    @secondary_skill = ''

    if @account.present? and @talent.present?
      user = talent.user

      skills = match.position.required_skills
      if skills.count > 0
        @primary_skill = skills[0].name
      end
      if skills.count > 1
        @secondary_skill = skills[1].name
      end

      @current_date = DateTime.now.strftime('%^B %-d, %Y')
      @timestamp = DateTime.now.to_s
      @job_details_url = ENV['FRONTEND_URL'] + "#/from_email?user=#{user.email}&role=talent&screen=job_details&position_id=#{@position.id}&match_id=#{match.id}"

      mail \
        to:      @talent.user.email,
        cc:      'support@getfetched.com',
        subject: "You've been Fetched!"
    end
  end

  def email_talent_match_only_one(talent, match)
    attach_logo
    @talent = talent
    @position = match.position
    @account = match.position.account
    @primary_skill = ''

    if @account.present? and @talent.present?
      user = talent.user

      position_skills = match.position.required_skills
      talent_skills = talent.skills
      matched_skill = position_skills & talent_skills
      if matched_skill.count > 0
        @primary_skill = matched_skill[0].name
      end

      @current_date = DateTime.now.strftime('%^B %-d, %Y')
      @timestamp = DateTime.now.to_s
      @job_details_url = ENV['FRONTEND_URL'] + "#/from_email?user=#{user.email}&role=talent&screen=job_details&position_id=#{@position.id}&match_id=#{match.id}"

      mail \
        to:      @talent.user.email,
        cc:      'support@getfetched.com',
        subject: "You've been Fetched!"
    end
  end

  def email_talent_interest(talent, match)
    attach_logo
    @talent = talent
    @position = match.position
    @employer = @position.employer_contact unless @position.blank?

    @primary_skill = ''
    @secondary_skill = ''

    if @employer.present? and @talent.present? and @position.present?
      user = @employer.user

      skills = match.position.required_skills
      if skills.count > 0
        @primary_skill = skills[0].name
      end
      if skills.count > 1
        @secondary_skill = skills[1].name
      end

      @talent_picture = @talent.photo.present? ? @talent.photo.url : ''

      @current_date = DateTime.now.strftime('%^B %-d, %Y')
      @timestamp = DateTime.now.to_s

      parameter_string = "user=#{user.email}&role=employer&screen=talent_details&position_id=#{@position.id}&match_id=#{match.id}&talent_id=#{talent.id}"
      parameter = Base64.encode64(parameter_string).chomp!
      @profile_url = ENV['FRONTEND_URL'] + "#/from_email?data=#{parameter}"

      mail \
        to:      @employer.user.email,
        cc:      'support@getfetched.com',
        subject: "Talent is interested in a position!"
    end
  end

  def email_talent_interest_only_one(talent, match)
    attach_logo
    @talent = talent
    @position = match.position
    @employer = @position.employer_contact unless @position.blank?

    @primary_skill = ''

    if @employer.present? and @talent.present? and @position.present?
      user = @employer.user

      position_skills = match.position.required_skills
      talent_skills = talent.skills
      matched_skill = position_skills & talent_skills
      if matched_skill.count > 0
        @primary_skill = matched_skill[0].name
      end

      @talent_picture = @talent.photo.present? ? @talent.photo.url : ''

      @current_date = DateTime.now.strftime('%^B %-d, %Y')
      @timestamp = DateTime.now.to_s

      parameter_string = "user=#{user.email}&role=employer&screen=talent_details&position_id=#{@position.id}&match_id=#{match.id}&talent_id=#{talent.id}"
      parameter = Base64.encode64(parameter_string).chomp!
      @profile_url = ENV['FRONTEND_URL'] + "#/from_email?data=#{parameter}"

      mail \
        to:      @employer.user.email,
        cc:      'support@getfetched.com',
        subject: "Talent is interested in a position!"
    end
  end

  def email_weekly_matches(talent, matches)
    attach_logo
    @matches = matches
    @current_date = DateTime.now.strftime('%^B %-d, %Y')
    @timestamp = DateTime.now.to_s
    @user = talent.user

    mail(to: talent.user.email, subject: "Weekly matches")
  end
end
