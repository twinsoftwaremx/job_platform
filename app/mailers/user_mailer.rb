class UserMailer < Devise::Mailer
  # helper :application
  # include Devise::Controllers::UrlHelpers
  default template_path: 'devise/mailer'

  def send_test(email)
    @email = email
    mail(to: @email, subject: 'Welcome to My Awesome Site')
  end

  def reset_password_instructions(record, token, opts={})
    @link = ENV['FRONTEND_URL'] + "#/reset_password?token=#{token}"
    super
  end
end
