class AgentMailer < ApplicationMailer
  def talent_fetched(position, talent_profile)
    attach_logo
    @position = position
    @talent = talent_profile

    mail(to: talent_profile.agent.admin_user.email, subject: "#{talent_profile.name} has been Fetched!")
  end

  def talent_interested(position, talent_profile)
    attach_logo
    @position = position
    @talent = talent_profile

    mail \
      to:      talent_profile.agent.admin_user.email,
      cc:      'support@getfetched.com',
      subject: "#{talent_profile.name} has shown interest in a position!"
  end

  def talent_not_interested(position, talent_profile)
    attach_logo
    @position = position
    @talent = talent_profile

    mail(to: talent_profile.agent.admin_user.email, subject: "#{talent_profile.name} has shown disinterest in a position!")
  end

  def talent_messaged(position, talent, message)
    attach_logo
    @talent   = talent.talent_profile
    @message  = message
    @contact  = position.employer_contact
    @employer = position.account

    mail(to: @talent.agent.admin_user.email, subject: "#{@talent.name} has been sent a message!")
  end

  def employer_messaged(position, talent, message)
    attach_logo
    @talent = talent.talent_profile
    @message = message
    @contact = position.employer_contact
    @employer = position.account

    mail(to: @talent.agent.admin_user.email, subject: "#{@talent.name} has been sent a message!")
  end

  def talent_received_offer(offer)
    attach_logo
    @offer = offer
    @talent = offer.talent_profile_match.talent_profile
    @position = offer.talent_profile_match.position

    mail(to: @talent.agent.admin_user.email, subject: "#{@talent.name} received an offer!")
  end

  def talent_accepted_offer(offer)
    attach_logo
    @offer = offer
    @talent = offer.talent_profile_match.talent_profile
    @position = offer.talent_profile_match.position

    mail(to: @talent.agent.admin_user.email, subject: "#{@talent.name} accepted an offer!")
  end

  def talent_rejected_offer(offer, reason)
    attach_logo
    @offer = offer
    @reason = reason
    @talent = offer.talent_profile_match.talent_profile
    @position = offer.talent_profile_match.position

    mail(to: @talent.agent.admin_user.email, subject: "#{@talent.name} rejected an offer!")
  end
end
