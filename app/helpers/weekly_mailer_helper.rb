module WeeklyMailerHelper
  def job_details_link(email, position_id, match_id)
    parameter_string = "user=#{email}&role=talent&screen=job_details&position_id=#{position_id}&match_id=#{match_id}"
    parameter = Base64.encode64(parameter_string).chomp!
  
    ENV['FRONTEND_URL'] + "#/from_email?data=#{parameter}"
  end
end
