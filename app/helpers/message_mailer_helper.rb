module MessageMailerHelper

  def frontend_link_to(message)
    to    = message.to_user
    email = to.try(:email)
    role  = role(to)

    fetch_url encode \
      "user=#{email}&role=#{role}&screen=dashboard"
  end

private

  def fetch_url(parameters)
    "#{ENV['FRONTEND_URL']}#/from_email?data=#{parameters}"
  end

  def encode(value)
    Base64.encode64(value).chomp!
  end

  def role(user)
    return :agent  if user.is_a? AdminUser
    return :talent if user.has_role? :talent
    :employer
  end
end
