ActiveAdmin.register Invoice do
  scope :all
  scope :due, default: true
  scope :paid

  actions :index, :edit

  collection_action :authenticate, method: :get do
    token = $qb_oauth_consumer.get_request_token(:oauth_callback => oauth_callback_admin_invoices_url)
    session[:qb_request_token] = Marshal.dump(token)
    redirect_to("https://appcenter.intuit.com/Connect/Begin?oauth_token=#{token.token}") and return
  end

  collection_action :oauth_callback, method: :get do
    at = Marshal.load(session[:qb_request_token]).get_access_token(:oauth_verifier => params[:oauth_verifier])
    QuickbooksAuthToken.new.tap do |qb|
      qb.access_token  = at.token
      qb.access_secret = at.secret
      qb.company_id    = params['realmId']
      qb.expires_at    = Time.zone.now + 6.months
      qb.reconnect_at  = Time.zone.now + 5.months
      qb.save!
    end
  end

  index do
    selectable_column
    id_column

    column :company_name
    column :talent_profile_name
    column :category
    column :status
    column :amount
    column :due_at
    column :paid_at

    actions
  end

  action_item :view, only: :index do
    render partial: 'quickbooks'
  end

  filter :company_name
  filter :talent_profile_name
  filter :status
  filter :due_at
  filter :paid_at

  form do |f|
    f.semantic_errors
    f.inputs 'Details' do
      f.input :status
      f.input :amount
      f.input :paid_at
    end
  end
end
