ActiveAdmin.register Offer do
  permit_params :upfront_fee, :recurring_fee

  index do
    selectable_column
    id_column
    column :company_name
    column :position_title
    column :salary_pay_rate
    column :status
    column :upfront_fee
    column :recurring_fee
    actions
  end

  filter :company_name
  filter :position_title

  form do |f|
    f.inputs "Offer Details" do
      f.input :upfront_fee
      f.input :recurring_fee
    end
    f.actions
  end
end
