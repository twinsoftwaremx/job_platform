ActiveAdmin.register_page "Dashboard" do
  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  page_action :agent_new_message, method: :post do
    redirect_to new_admin_message_path(from_user_id: current_admin_user.id)
  end

  page_action :agent_load_talent_matches, method: :get do
    vertical = params[:vertical]
    role = params[:skill_category]
    skillset = params[:skillset]
    location = params[:location]
    culture = params[:culture]

    records = []
    matches_records = []

    talent_matches = current_admin_user.agent.talent_profile_matches
    if vertical.present?
      vertical = VerticalMarket.find(vertical)
      if vertical.present?
        skills_of_vertical = vertical.skills
        talent_matches.each do |match|
          profile = match.talent_profile
          skills = profile.skills
          skills.each do |skill|
            inc = skills_of_vertical.select { |s| s.id == skill.id }
            if inc.count > 0
              matches_records << match
              break
            end
          end
        end
      end
    elsif role.present?
      skills_of_category = Skill.where(skill_category_id: role)
      talent_matches.each do |match|
        profile = match.talent_profile
        skills = profile.skills
        skills.each do |skill|
          inc = skills_of_category.select { |s| s.id == skill.id }
          if inc.count > 0
            matches_records << match
            break
          end
        end
      end
    elsif skillset.present?
      talent_matches.each do |match|
        profile = match.talent_profile
        skills = profile.skills
        skills.each do |skill|
          if skillset.to_i == skill.id
            matches_records << match
            break
          end
        end
      end
    elsif location.present?
      talent_matches.each do |match|
        profile = match.talent_profile
        cities = profile.cities_of_interest
        cities.each do |city|
          if city.id == location.to_i
            matches_records << match
            break
          end
        end
      end
    elsif culture.present?
      talent_matches.each do |match|
        profile = match.talent_profile
        cultures = profile.cultures
        cultures.each do |c|
          if c.id == culture.to_i
            matches_records << match
            break
          end
        end
      end
    else
      matches_records = talent_matches
    end

    matches_records.each do |match|
      record = {
          id: match.talent_profile.id,
          talent: match.talent_profile.name,
          resume: match.talent_profile.resume.present? ? Refile.attachment_url(match.talent_profile.resume, :file, filename: match.talent_profile.resume.filename) : ''
      }
      records << record
    end
    records = records.uniq

    render json: records.to_json
  end

  page_action :agent_load_verticals, method: :get do
    verticals = VerticalMarket.all
    render json: verticals
  end

  page_action :agent_load_roles, method: :get do
    categories = SkillCategory.all
    render json: categories
  end

  page_action :agent_load_skillsets, method: :get do
    skills = Skill.all
    render json: skills
  end

  page_action :agent_load_locations, method: :get do
    locations = City.all
    render json: locations
  end

  page_action :agent_load_cultures, method: :get do
    cultures = Culture.all
    render json: cultures
  end

  action_item :new_message do
    link_to 'New Message', admin_dashboard_agent_new_message_path, method: :post
  end

  content title: proc{ I18n.t("active_admin.dashboard") } do
    # div class: "blank_slate_container", id: "dashboard_default_message" do
    #   span class: "blank_slate" do
    #     span I18n.t("active_admin.dashboard_welcome.welcome")
    #     small I18n.t("active_admin.dashboard_welcome.call_to_action")
    #   end
    # end

    # Here is an example of a simple dashboard with columns and panels.
    #
    columns do
      if current_admin_user.agent_admin?
        column do
          panel "Assigned Talent " do
            table_for current_admin_user.agent.talent_profiles do
              column :id
              column :name
              column "Email" do |talent|
                talent.user.email unless talent.user.nil?
              end
              column :summary
              column :salary_range
              column "Resume" do |talent|
                link_to "Download", talent.resume.url, target: "_blank" unless talent.resume.nil?
              end
              column "Actions" do |talent|
                link_to "View", admin_talent_profile_path(talent)
              end
            end
          end
        end

        column do
          panel "Assigned Accounts" do
            ul do
              current_admin_user.agent.accounts.limit(100).map do |account|
                li link_to(account.company_name, admin_account_path(account))
              end
            end
          end
        end
      end
    end

    columns do
      if current_admin_user.agent_admin?
        column do
          panel "Mesages" do
            table_for current_admin_user.messages_received do
              column :id
              column "From" do |message|
                # link_to message.from_user.talent_profile.present? ? message.from_user.talent_profile.name : message.from_user.employer_contact.present? ? message.from_user.employer_contact.name : message.from_user.email,
                #         ['admin', message.from_user.talent_profile.present? ? message.from_user.talent_profile : message.from_user.employer_contact.present? ? message.from_user.employer_contact : ''] unless message.from_user.nil?
                link_to message.get_user_real_name(message.from_user),
                        message.from_user_id == current_admin_user.id ? '' : message.get_user_link(message.from_user) unless message.from_user.nil?
              end
              column "Received at" do |message|
                l message.created_at.localtime, format: :long
              end
              column "Message" do |message|
                truncate(message.body, omision: "...", length: 50)
              end
              column "Actions" do |message|
                link_to "View", admin_message_path(message)
              end
            end
          end
        end

        column do
          panel "Talent Matches" do
            render partial: 'talent_matches_filters'
            render partial: 'talent_matches_table'
          end
        end
      end
    end
  end
end
