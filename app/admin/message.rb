ActiveAdmin.register Message do
  permit_params :body, :from_user_id, :to_user_id, :from_user_type

  index do
    selectable_column
    id_column
    column "From" do |message|
      if message.from_user_id == current_admin_user.id
        'Me'
      else
        link_to message.get_user_real_name(message.from_user),
                 message.from_user_id == current_admin_user.id ? '' : message.get_user_link(message.from_user) unless message.from_user.nil?
      end
    end
    column "To" do |message|
      if message.to_user_id == current_admin_user.id
        'Me'
      else
        link_to message.to_user_id == current_admin_user.id ? 'Me' : message.get_user_real_name(message.to_user),
                message.to_user_id == current_admin_user.id ? nil : message.get_user_link(message.to_user) unless message.to_user.nil?
      end
    end
    column "Received at" do |message|
      l message.created_at.localtime, format: :long
    end
    column "Message" do |message|
      truncate(message.body, omision: "...", length: 50)
    end
    column "Actions" do |message|
      link_to "View", admin_message_path(message)
    end
  end

  form do |f|
    f.inputs "Message details" do
      f.input :from_user_type, as: :hidden, :input_html => { :value => 'AdminUser' }
      f.input :from_user_id, as: :hidden, :input_html => { :value => params[:from_user_id].present? ? params[:from_user_id] : current_admin_user.id }
      f.input :to_user, :collection => (User.all.collect {|user| user.talent_profile.present? ? [user.talent_profile.name + " (Talent)", user.id] : user.employer_contact.present? ? [user.employer_contact.name + " (Employer)", user.id] : [user.email, user.id]})
      f.input :body
    end
    f.actions
  end
end
