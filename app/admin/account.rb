ActiveAdmin.register Account do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
  permit_params do
    permitted  = [:company_name, :size, :phone, :summary, :benefits]
    permitted << [:perks, :culture, :github_url, :company_site_url, :agent_id]
    permitted
  end

  show do
    tabs do
      tab 'Account Details' do
        attributes_table do
          row(:id)
          row(:company_name)
          row(:phone)
          row(:summary)
          row(:benefits)
          row(:perks)
          row(:github_url)
          row(:company_site_url)
          row(:agent)
          row(:culture_list)
          row(:company_size)
          row(:year_founded)
          row(:who_we_are)
          row(:what_we_do)
          row(:video_url)
          row(:billing_customer_id)
          row(:created_at)
          row(:updated_at)
          row(:deleted_at)
        end
      end
      tab 'Employer Contacts' do
        table_for account.employer_contacts do
          column :id
          column :name
          column :title
          column :user
          column :created_at
          column :updated_at
        end
      end
      tab 'Positions' do
        table_for account.positions do
          column :id
          column :job_title
          column :employment_type
          column :city
          column :salary_range
          column :required_skill_list
          column :created_at
          column :updated_at
        end
      end
    end
  end
end
