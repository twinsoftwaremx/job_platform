ActiveAdmin.register VerticalMarket do
  permit_params :name

  index do
    selectable_column
    id_column
    column :name
    column :created_at
    actions
  end

  form do |f|
    f.semantic_errors
    f.inputs 'Details' do
      f.input :name
    end
    f.actions
  end

  show do
    attributes_table do
      row :name
      row :created_at
      row :updated_at
    end
    panel 'Skill Categories' do
      table_for vertical_market.skill_categories do
        column :id
        column 'Name' do |category|
          link_to category.name, ['admin', category]
        end
        column :created_at
        column :updated_at
        column 'Actions' do |category|
          link_to 'View', admin_skill_category_path(category)
        end
      end
    end
    active_admin_comments
  end

end