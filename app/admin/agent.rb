ActiveAdmin.register Agent do
  permit_params :name

  filter :name
  filter :created_at
  filter :updated_at

  form do |f|
    f.semantic_errors
    f.inputs 'Details' do
      f.input :name
    end
    f.actions
  end

  show do
    attributes_table do
      row :name
      row "Email" do |agent|
        agent.admin_user.email unless agent.admin_user.nil?
      end
      row :created_at
      row :updated_at
      row :deleted_at
    end
    panel "Talents" do
      table_for agent.talent_profiles do
        column :id
        column :name
        column "Email" do |talent|
          talent.user.email unless talent.user.nil?
        end
        column :summary
        column :salary_range
        column "Resume" do |talent|
          link_to "Download", attachment_url(talent.resume, :file, filename: talent.resume.filename), target: "_blank" unless talent.resume.nil?
        end
        column "Actions" do |talent|
          link_to "View", admin_talent_profile_path(talent)
        end
      end
    end
    active_admin_comments
  end
end
