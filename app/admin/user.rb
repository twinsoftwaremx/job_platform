ActiveAdmin.register User do
  permit_params :email, :password, :password_confirmation

  index do
    selectable_column
    id_column
    column :email
    column :auth_token
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    column 'Talent Profile' do |u|
      link_to u.talent_profile.name, admin_talent_profile_path(u.talent_profile) if u.talent_profile.present?
    end
    actions
  end

  filter :email
  filter :auth_token

  form do |f|
    f.inputs "User Details" do
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end

end
