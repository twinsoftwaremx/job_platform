ActiveAdmin.register TalentProfile do
  remove_filter :talent_profile_matches
  remove_filter :matches

  action_item :match, only: :show do
    link_to "Get Matches", get_matches_admin_talent_profile_path(params[:id])
  end

  member_action :get_matches, method: :get do
    MatchFinder.call(resource)
    redirect_to admin_talent_profiles_path
  end

  permit_params do
    permitted  = [:name, :photo_id, :agent_id, :resume_id, :time_zone, :phone, :summary]
    permitted << [:area_of_interest, :years_of_experience, :usdod_clearance]
    permitted << [:reason_for_interest_in_new_job_opportunities, :job_search_status]
    permitted << [:preferred_base_salary, :usa_work_auth_type, :linked_in_url]
    permitted << [:personal_website, :dribbble_url, :stack_overflow_url]
    permitted << [:stack_overflow_url, :blog_url]

    permitted
  end

  index do
    selectable_column
    id_column
    column :name
    column "Email" do |talent|
      talent.user.email unless talent.user.nil?
    end
    column "Agent" do |talent|
      link_to talent.agent.name, admin_agent_path(talent.agent) unless talent.agent.nil?
    end
    column :summary
    column :salary_range
    column 'Skills' do |talent|
      talent.skills.map(&:name).join(', ')
    end
    column :created_at
    column :updated_at
    actions
  end

  show do
    attributes_table do
      row(:id)
      row(:agent)
      row(:name)
      row "Email" do |talent|
        talent.user.email unless talent.user.nil?
      end
      row(:title)
      row(:company)
      row(:phone)
      row(:location)
      row(:salary_range)
      row(:summary)
      row(:time_zone)
      row(:avatar_url)
      row('Cultures') { |r| r.cultures.map(&:name).join(', ') }
      row('Locations') { |r| r.cities_of_interest.map(&:name).join(', ') }
      row(:skill_category)
      row('Skills') { |r| r.skills.map(&:name).join(', ') }
      row(:area_of_interest)
      row(:years_of_experience)
      row(:usdod_clearance)
      row(:reason_for_interest_in_new_job_opportunities)
      row(:job_search_status)
      row(:usa_work_auth_type)
      row(:personal_website)
      row(:blog_url)
      row(:linked_in_url)
      row(:github_url)
      row(:dribbble_url)
      row(:stack_overflow_url)
      row(:created_at)
      row(:updated_at)
    end
  end
end
