ActiveAdmin.register SkillCategory do
  permit_params :name, :vertical_market_id

  index do
    selectable_column
    id_column
    column :name
    column 'Vertical Market' do |category|
      link_to category.vertical_market.name, ['admin', category.vertical_market] unless category.vertical_market.nil?
    end
    column :created_at
    actions
  end

  form do |f|
    f.semantic_errors
    f.inputs 'Details' do
      f.input :vertical_market
      f.input :name
    end
    f.actions
  end

  show do
    attributes_table do
      row :name
      row :vertical_market
      row :created_at
      row :updated_at
    end
    panel 'Skills' do
      table_for skill_category.skills do
        column :id
        column :name
        column :created_at
        column :updated_at
        column 'Actions' do |skill|
          link_to 'View', admin_skill_path(skill)
        end
      end
    end
    active_admin_comments
  end
end
