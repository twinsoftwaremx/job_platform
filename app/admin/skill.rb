ActiveAdmin.register Skill do
  permit_params :name, :skill_category_id

  index do
    selectable_column
    id_column
    column :name
    column 'Category' do |skill|
      link_to skill.skill_category.name, admin_skill_category_path(skill.skill_category) unless skill.skill_category.nil?
    end
    column :created_at
    column :updated_at
    column :deleted_at
    actions
  end

  form do |f|
    f.semantic_errors
    f.inputs 'Details' do
      f.input :name
      f.input :skill_category
    end
    f.actions
  end

  show do
    attributes_table do
      row :id
      row :name
      row :skill_category
      row :created_at
      row :updated_at
      row :deleted_at
    end
    active_admin_comments
  end
end
