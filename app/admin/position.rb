ActiveAdmin.register Position do
  remove_filter :talent_profile_matches
  remove_filter :talent_profiles

  collection_action :search_skills, method: :get do
    skills = Skill.all

    if params[:query].present?
      name = params[:query]
      skills = skills.where("LOWER(name) LIKE ?", "#{name.downcase}%")
    end

    render json: skills
  end

  index do
    selectable_column
    id_column
    column :job_title
    column :employment_type
    column :city
    column :salary_range
    column :required_skill_list
    column "Account" do |position|
      link_to position.employer_contact.account.company_name, admin_account_path(position.employer_contact.account) unless position.employer_contact.nil?
    end
    column :created_at
    actions
  end

  form do |f|
    f.semantic_errors # shows errors on :base
    f.inputs 'Details' do
      f.input :job_title
      f.input :description
      f.input :employment_type
      if f.object.new_record?
        f.input :employer_contact
      else
        if f.object.employer_contact.present? and f.object.employer_contact.account.present?
          f.input :employer_contact, collection: EmployerContact.where(account_id: f.object.employer_contact.account_id)
        else
          f.input :employer_contact
        end
      end
      f.input :city
      f.input :salary_range, label: 'Compensation'
      f.input :hours
      f.input :desired_years_experience, label: 'Desired years of experience'
      f.input :team_size
    end
    f.inputs 'Skills' do
      f.input :required_skill_list
      f.input :bonus_skill_list
    end
    f.actions         # adds the 'Submit' and 'Cancel' buttons
    render partial: 'scripts'
  end

  show title: :job_title do
    tabs do
      tab 'Overview' do
        attributes_table do
          row(:job_title)
          row(:description)
          row(:employment_type)
          row(:employer_contact)
          row(:city)
          row(:salary_range)
          row(:hours)
          row(:desired_years_experience)
          row(:team_size)
          row(:created_at)
          row(:updated_at)
        end
      end
      tab 'Skills' do
        attributes_table do
          row(:required_skill_list)
          row(:bonus_skill_list)
        end
      end
      tab 'Matches' do
        table_for position.talent_profile_matches, class: "index_table" do
          column :id
          column "Talent" do |talent_profile_match|
            talent_profile_match.talent_profile.name unless talent_profile_match.talent_profile.nil?
          end
          column :match_score
          column "Resume" do |talent_profile_match|
            link_to "Download", attachment_url(talent_profile_match.talent_profile.resume, :file, filename: talent_profile_match.talent_profile.resume.filename), target: "_blank" unless talent_profile_match.talent_profile.resume.nil?
          end
          column "Actions" do |talent_profile_match|
            link_to "View profile", admin_talent_profile_path(talent_profile_match.talent_profile)
          end
        end
      end
    end
  end

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
  permit_params do
    permitted =  [:employment_type_id, :employer_contact_id, :job_title]
    permitted << [:description, :hours, :salary_range_id, :desired_years_experience]
    permitted << [:team_size, :required_skill_list, :bonus_skill_list]
    permitted
  end
end
