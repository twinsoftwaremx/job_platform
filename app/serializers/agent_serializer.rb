# == Schema Information
#
# Table name: agents
#
#  id         :integer          not null, primary key
#  name       :string           default(""), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  deleted_at :datetime
#
# Indexes
#
#  index_agents_on_deleted_at  (deleted_at)
#

class AgentSerializer < ActiveModel::Serializer
  attributes :id, :name, :photo_url, :status, :messages, :user_id

  def status
    if object.admin_user.present?
      object.admin_user.online?
    end
  end

  def photo_url
    if object.photo.present?
      object.photo.url
    end
  end

  def user_id
    if object.admin_user.present?
      object.admin_user.id
    end
  end
end
