# == Schema Information
#
# Table name: vertical_markets
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  deleted_at :datetime
#
# Indexes
#
#  index_vertical_markets_on_deleted_at  (deleted_at)
#

class VerticalMarketSerializer < ActiveModel::Serializer
  attributes :id, :name
end
