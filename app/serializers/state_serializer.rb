# == Schema Information
#
# Table name: states
#
#  id           :integer          not null, primary key
#  name         :string           not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  abbreviation :string           not null
#  deleted_at   :datetime
#
# Indexes
#
#  index_states_on_deleted_at  (deleted_at)
#

class StateSerializer < ActiveModel::Serializer
  attributes :id, :name, :abbreviation
end
