# == Schema Information
#
# Table name: salary_ranges
#
#  id                :integer          not null, primary key
#  name              :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  deleted_at        :datetime
#  compensation_type :string           default("Direct Hire"), not null
#  sort_order        :integer
#
# Indexes
#
#  index_salary_ranges_on_deleted_at  (deleted_at)
#

class SalaryRangeSerializer < ActiveModel::Serializer
  attributes :id, :name, :compensation_type, :sort_order
end
