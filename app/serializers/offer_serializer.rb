# == Schema Information
#
# Table name: offers
#
#  id                      :integer          not null, primary key
#  talent_profile_match_id :integer          not null
#  employer_contact_id     :integer          not null
#  status                  :integer          default(0)
#  company_name            :string
#  position_title          :string
#  manager_name            :string
#  salary_pay_rate         :string
#  start_date              :string
#  benefits                :text
#  legal_verbiage          :text
#  hiring_managers_name    :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  details                 :text
#  job_type                :string
#  decline_reason          :text
#  deleted_at              :datetime
#  upfront_fee             :decimal(, )      default(0.0)
#  recurring_fee           :decimal(, )      default(0.0)
#
# Indexes
#
#  index_offers_on_deleted_at               (deleted_at)
#  index_offers_on_employer_contact_id      (employer_contact_id)
#  index_offers_on_talent_profile_match_id  (talent_profile_match_id)
#

class OfferSerializer < ActiveModel::Serializer
  include ActionView::Helpers::NumberHelper

  attributes :id, :company_name, :position_title, :status, :manager_name,
            :salary_pay_rate, :start_date, :benefits, :talent_profile_match_id,
            :employer_contact_id, :legal_verbiage, :hiring_managers_name, :skillset,
            :location, :company_logo_url, :compensation, :decline_reason

  def start_date
    if object.start_date.present?
      DateTime.parse(object.start_date).strftime('%m/%d/%Y')
    end
  end

  def compensation
    if object.salary_pay_rate.present?
      number_to_currency(object.salary_pay_rate)
    end
  end

  def skillset
    if object.talent_profile_match.position.present?
      object.talent_profile_match.position.required_skill_list
    end
  end

  def location
    if object.talent_profile_match.position.present?
      object.talent_profile_match.position.city.full_name
    end
  end

  def company_logo_url
    if object.employer_contact.account.logo.present?
      object.employer_contact.account.logo.url
    end
  end

  def upfront_fee
    if object.upfront_fee.present?
      number_to_currency(object.upfront_fee)
    end
  end

  def recurring_fee
    if object.recurring_fee.present?
      number_to_currency(object.recurring_fee)
    end
  end
end
