# == Schema Information
#
# Table name: cultures
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  deleted_at :datetime
#
# Indexes
#
#  index_cultures_on_deleted_at  (deleted_at)
#

class CultureSerializer < ActiveModel::Serializer
  attributes :id, :name, :photo_url

  def photo_url
    if object.photo.present?
      object.photo.url
    end
  end
end
