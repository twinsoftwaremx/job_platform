# == Schema Information
#
# Table name: messages
#
#  id             :integer          not null, primary key
#  body           :text             not null
#  to_user_id     :integer          not null
#  from_user_id   :integer          not null
#  position_id    :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  from_user_type :string           default("User")
#  to_user_type   :string           default("User")
#  deleted_at     :datetime
#
# Indexes
#
#  index_messages_on_deleted_at    (deleted_at)
#  index_messages_on_from_user_id  (from_user_id)
#  index_messages_on_position_id   (position_id)
#  index_messages_on_to_user_id    (to_user_id)
#

class MessageSerializer < ActiveModel::Serializer
  attributes :id, :body, :created_at, :to, :from

  belongs_to :to_user
  belongs_to :from_user
  belongs_to :position

  def to
    {
      id: to_user.id,
      name: get_attribute_from_association('name', 'to_user'),
      avatar: get_attribute_from_association('photo', 'to_user').present? ? get_attribute_from_association('photo', 'to_user').url : '',
      type: get_user_type('to_user')
    }
  end

  def from
    {
      id: from_user.id,
      name: get_attribute_from_association('name', 'from_user'),
      avatar: get_attribute_from_association('photo', 'from_user').present? ? get_attribute_from_association('photo', 'from_user').url : '',
      type: get_user_type('from_user')
    }
  end

  private
  def get_attribute_from_association(attribute, association)
    if object.send(association).present? and object.send(association).is_a?(User)
      if object.send(association).talent_profile.present?
        object.send(association).talent_profile.send(attribute)
      elsif object.send(association).employer_contact.present?
        object.send(association).employer_contact.send(attribute)
      else
        ''
      end
    elsif object.send(association).present? and object.send(association).is_a?(AdminUser)
      object.send(association).agent.send(attribute)
    end
  end

  def get_user_type(association)
    if object.send(association).present? and object.send(association).is_a?(User)
      if object.send(association).talent_profile.present?
        'talent_profile'
      elsif object.send(association).employer_contact.present?
        'employer_contact'
      else
        ''
      end
    elsif object.send(association).present? and object.send(association).is_a?(AdminUser)
      'agent'
    end
  end
end
