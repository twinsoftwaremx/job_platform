class V1::SessionsController < V1::BaseController
  before_action :authenticate, except: [:create]
  after_action :verify_authorized, only: [:destroy]
  after_action :user_activity, except: [:destroy]

  def create
    user_email = params[:session][:email]
    user_password = params[:session][:password]
    user = user_email.present? && User.find_by(email: user_email)

    if not user.present?
      user = user_email.present? && AdminUser.find_by(email: user_email)
    end

    if user.present? && user.valid_password?(user_password)
      sign_in user, store: false
      user.generate_authentication_token!
      user.save
      render json: user, status: 200
    else
      render json: { errors: "Invalid email or password" }, status: 401
    end
  end

  def destroy
    user = User.find_by(auth_token: params[:id])

    if not user.present?
      user = AdminUser.find_by(auth_token: params[:id])
    end

    authorize user, :sign_out?

    user.last_activity_at = nil
    user.generate_authentication_token!
    user.save

    head 204
  end
end
