class V1::InterestAssociationsController < V1::BaseController

  def index
    talent_profile = TalentProfile.find(params[:talent_profile_id])

    authorize talent_profile, :view_object_list?

    interest_associations = InterestAssociation.find_by(talent_profile: params[:talent_profile_id])
    render json: interest_associations, status: 200
  end

  def show
    interest_association = InterestAssociation.find(params[:id])

    authorize interest_association

    render json: interest_association, status: 200
  end

  def create
    talent_profile = TalentProfile.find(params[:talent_profile_id])

    authorize talent_profile, :add_object?

    interest_association = InterestAssociation.new(interest_association_params)
    interest_association.talent_profile = talent_profile
    if interest_association.save
      render json: interest_association, status: 201
    else
      render json: { errors: interest_association.errors }, status: 422
    end
  end

  def destroy
    interest_association = InterestAssociation.find(params[:id])

    authorize interest_association
    interest_association.destroy
    head 204
  end

  private
  def interest_association_params
    params.require(:interest_association).permit(:interest_id)
  end

end