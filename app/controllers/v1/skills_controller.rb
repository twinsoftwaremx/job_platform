class V1::SkillsController < V1::BaseController
  before_action :authenticate, except: [:index]
  after_action :verify_authorized, except: [:index, :show]

  def index
    skills = policy_scope(Skill)

    if params[:category_id].present? 
      skills = skills.where(skill_category_id: params[:category_id])
    end
    if params[:categories_list].present?
      skills = Skill.by_category_list(skills, params[:categories_list])
    end

    render json: skills, status: 200
  end

  def show
    skill = Skill.find(params[:id])

    render json: skill, status: 200
  end

  def create
    skill = Skill.create(skill_params)

    authorize skill

    if skill.save
      render json: skill, status: 201
    else
      render json: { errors: skill.errors }, status: 422
    end
  end

  def update
    skill = Skill.find(params[:id])

    authorize skill

    if skill.update(skill_params)
      render json: skill, status: 204
    else
      render json: { errors: skill.errors }, status: 422
    end
  end

  def destroy
    skill = Skill.find(params[:id])

    authorize skill

    skill.destroy
    head 204
  end

  private
  def skill_params
    params.require(:skill).permit(:name)
  end
end