class V1::EmploymentTypesController < V1::BaseController
  before_action :authenticate, except: [:index]
  after_action :verify_authorized, except: [:index, :show]

  def index
    employment_types = policy_scope(EmploymentType)

    render json: employment_types, status: 200
  end

  def show
    employment_type = EmploymentType.find(params[:id])

    render json: employment_type, status: 200
  end

  def create
    employment_type = EmploymentType.new(employment_type_params)

    authorize employment_type

    if employment_type.save
      render json: employment_type, status: 201
    else
      render json: { errors: employment_type.errors }, status: 422
    end
  end

  def update
    employment_type = EmploymentType.find(params[:id])

    authorize employment_type

    if employment_type.update(employment_type_params)
      render json: employment_type, status: 204
    else
      render json: { errors: employment_type.errors }, status: 422
    end
  end

  def destroy
    employment_type = EmploymentType.find(params[:id])

    authorize employment_type

    employment_type.destroy
    head 204
  end

  private
  def employment_type_params
    params.require(:employment_type).permit(:name)
  end
end