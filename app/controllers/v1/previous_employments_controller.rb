class V1::PreviousEmploymentsController < V1::BaseController

  def index
    talent_profile = TalentProfile.find(params[:talent_profile_id])

    authorize talent_profile, :view_object_list?

    previous_employments = PreviousEmployment.find_by(talent_profile: talent_profile.id)
    render json: previous_employments, status: 200
  end

  def show
    previous_employment = PreviousEmployment.find(params[:id])

    authorize previous_employment

    render json: previous_employment, status: 200
  end

  def create
    talent_profile = TalentProfile.find(params[:talent_profile_id])

    authorize talent_profile, :add_object?

    previous_employment = PreviousEmployment.new(previous_employment_params)
    previous_employment.talent_profile = talent_profile
    if previous_employment.save
      render json: previous_employment, status: 201
    else
      render json: { errors: previous_employment.errors }, status: 422
    end
  end

  def update
    previous_employment = PreviousEmployment.find(params[:id])

    authorize previous_employment

    if previous_employment.update(previous_employment_params)
      render json: previous_employment, status: 204
    else
      render json: { errors: previous_employment.errors }, status: 422
    end
  end

  def destroy
    previous_employment = PreviousEmployment.find(params[:id])

    authorize previous_employment

    previous_employment.destroy
    head 204
  end

  private
  def previous_employment_params
    params.require(:previous_employment).permit(:company, :position, :start_date, :end_date, :description)
  end
end
