class V1::VerticalMarketsController < V1::BaseController
  before_action :authenticate, except: [:index]
  after_action :verify_authorized, except: [:index, :show]

  def index
    vertical_markets = policy_scope(VerticalMarket)
    render json: vertical_markets, status: 200
  end

  def show
    vertical_market = VerticalMarket.find(params[:id])
    render json: vertical_market, status: 200
  end

  def create
    vertical_market = VerticalMarket.create(vertical_market_params)

    authorize vertical_market

    if vertical_market.save
      render json: vertical_market, status: 201
    else
      render json: { errors: vertical_market.errors }, status: 422
    end
  end

  def update
    vertical_market = VerticalMarket.find(params[:id])

    authorize vertical_market

    if vertical_market.update(vertical_market_params)
      render json: vertical_market, status: 204
    else
      render json: { errors: vertical_market.errors }, status: 422
    end
  end

  def destroy
    vertical_market = VerticalMarket.find(params[:id])

    authorize vertical_market

    vertical_market.destroy
    head 204
  end

  private
  def vertical_market_params
    params.require(:vertical_market).permit(:name)
  end
end