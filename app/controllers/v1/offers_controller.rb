class V1::OffersController < V1::BaseController
  after_action  :verify_authorized, except: :index
  before_action :set_offer, only: [:show, :update, :destroy, :accept, :reject]

  def index
    offers = policy_scope(Offer)
    render json: offers, status: 200
  end

  def show
    authorize @offer
    render json: @offer, status: 200
  end

  def create
    offer = Offer.new(offer_params)
    offer.upfront_fee   = UpfrontFee.calculate(offer.salary)
    offer.recurring_fee = RecurringFee.calculate(offer.salary)

    authorize offer

    if offer.save

      OfferMailer.email_make_offer(offer).deliver_now
      AgentMailer.talent_received_offer(offer).deliver_now

      render json: offer, status: 201
    else
      puts offer.errors.inspect
      render json: { errors: offer.errors }
    end
  end

  def destroy
    authorize @offer
    @offer.destroy
    render json: @offer, status: 204
  end

  def accept
    authorize @offer

    status = @offer.accepted? ? 'unanswered' : 'accepted'

    if @offer.update(status: status)

      %w(employer talent).map { |role| OfferMailer.email_accept_offer(@offer, role).deliver_now }
      AgentMailer.talent_accepted_offer(@offer).deliver_now
      BillingPlanWorker.perform_async(@offer.id)

      render json: @offer, status: 201
    else
      puts @offer.errors.inspect
      render json: { errors: @offer.errors }
    end
  end

  def reject
    authorize @offer

    status = @offer.rejected? ? 'unanswered' : 'rejected'
    reason = params[:reason]

    if @offer.update(status: status, deline_reason: decline_reason)

      OfferMailer.email_reject_offer(offer).deliver_now
      AgentMailer.talent_rejected_offer(offer, reason).deliver_now
      BillingPlanWorker.perform_async(offer.id, strategy: :cancel)

      render json: offer, status: 201
    else
      puts @offer.errors.inspect
      render json: { errors: @offer.errors }
    end
  end

private
  def set_offer
    @offer = Offer.find(params[:id])
  end

  def offer_params
    params.require(:offer).permit \
      :benefits,
      :company_name,
      :details,
      :employer_contact_id,
      :hiring_managers_name,
      :job_type,
      :legal_verbiage,
      :manager_name,
      :position_title,
      :salary_pay_rate,
      :start_date,
      :talent_profile_match_id
  end
end
