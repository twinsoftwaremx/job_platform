class V1::CulturesController < V1::BaseController
  before_action :authenticate, except: [:index]
  after_action :verify_authorized, except: [:index, :show]

  def index
    cultures = policy_scope(Culture)

    render json: cultures, status: 200
  end

  def show
    culture = Culture.find(params[:id])

    render json: culture, status: 200
  end

  def create
    culture = Culture.create(culture_params)

    authorize culture

    if culture.save
      render json: culture, status: 201
    else
      render json: { errors: culture.errors }, status: 422
    end
  end

  def update
    culture = Culture.find(params[:id])

    authorize culture

    if culture.update(culture_params)
      render json: culture, status: 204
    else
      render json: { errors: culture.errors }, status: 422
    end
  end

  def destroy
    culture = Culture.find(params[:id])

    authorize culture

    culture.destroy
    head 204
  end

  private
  def culture_params
    params.require(:culture).permit(:name, :photo_id)
  end
end