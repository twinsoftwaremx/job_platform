class V1::LocationOfInterestAssociationsController < V1::BaseController

  def index
    talent_profile = TalentProfile.includes(:location_of_interest_associations).find(params[:talent_profile_id])

    authorize talent_profile, :view_object_list?
    location_of_interest_associations = LocationOfInterestAssociation.find_by(talent_profile: params[:talent_profile_id])
    render json: location_of_interest_associations, status: 200
  end

  def show
    location_of_interest_association = LocationOfInterestAssociation.find(params[:id])

    authorize location_of_interest_association
    render json: location_of_interest_association, status: 200
  end

  def create
    talent_profile = TalentProfile.find(params[:talent_profile_id])

    authorize talent_profile, :add_object?
    location_of_interest_association = LocationOfInterestAssociation.new(location_of_interest_association_params)
    location_of_interest_association.talent_profile = talent_profile
    if location_of_interest_association.save
      render json: location_of_interest_association, status: 201
    else
      render json: { errors: location_of_interest_association.errors }, status: 422
    end
  end

  def destroy
    location_of_interest_association = LocationOfInterestAssociation.find(params[:id])

    authorize location_of_interest_association
    location_of_interest_association.destroy
    head 204
  end

  private
  def location_of_interest_association_params
    params.require(:location_of_interest_association).permit(:location_id)
  end

end
