class V1::AssetsController < V1::BaseController
  skip_before_action :verify_authenticity_token
  after_action :verify_authorized, only: []

  def show
    @asset = Asset.find(params[:id])
    render json: @asset
  end

  def create
    @asset = Asset.create(asset_params)

    if @asset.save
      render json: @asset, status: 201
    else
      render json: {errors: @asset.errors}
    end
  end

  private
   def asset_params
    params.require(:asset).permit(:file, :type)
  end
end
