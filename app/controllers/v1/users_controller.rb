class V1::UsersController < V1::BaseController
  before_action :authenticate, except: [:create, :reset_password, :forgot_password]
  after_action :verify_authorized, only: [:show]

  def show
    user = User.find(params[:id])

    authorize user

    render json: user
  end

  def create
    user = User.new(user_params)
    if user.save
      render json: user, status: 201
    else
      render json: { errors: user.errors }, status: 422
    end
  end

  def from_token
    user = current_user
    puts user.inspect
    render json: user, status: 200
  end

  def forgot_password
    email = params[:email]

    user = User.where(email: email).first
    if user.present?
      user.send_reset_password_instructions
      render json: user, status: 200
    else
      render json: user, status: 422
    end
  end

  def reset_password
    token = params[:token]
    password = params[:password]
    decoded_password = Base64.decode64(password)

    attributes = {
      password: decoded_password,
      confirmation: decoded_password,
      reset_password_token: token
    }
    user = User.reset_password_by_token(attributes)
    if user.errors.count > 0
      error_message = user.errors.messages
      render json: error_message, status: 422
    else
      render json: user, status: 200
    end
  end

  private
  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :role)
  end

end
