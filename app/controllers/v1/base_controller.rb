class V1::BaseController < ActionController::API
  include ActionController::HttpAuthentication::Token::ControllerMethods
  include Pundit
  before_action :authenticate
  after_action :verify_authorized
  after_action :user_activity

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  # Overwrite Devise method
  def current_user
    authenticate_with_http_token do |token, options|
      @current_user ||= User.find_by(auth_token: token)
      @current_user ||= AdminUser.find_by(auth_token: token)
    end
  end

  protected
  def authenticate
    authenticate_token || render_unauthorized
  end

  def authenticate_token
    authenticate_with_http_token do |token, options|
      user = User.find_by(auth_token: token)
      if !user.present?
        user = AdminUser.find_by(auth_token: token)
      end
      user
    end
  end

  def render_unauthorized
    render json: {errors: 'Bad credentials'}, status: 401
  end

  def user_not_authorized
    render json: {errors: 'User not authorized for this action'}, status: 403
  end

  private
  def user_activity
    current_user.try(:touch, :last_activity_at)
  end
end
