class V1::PreferredCompanySizeAssociationsController < V1::BaseController

  def index
    talent_profile = TalentProfile.find(params[:talent_profile_id])

    authorize talent_profile, :view_object_list?
    
    pref_company_sizes = PreferredCompanySizeAssociation.find_by(talent_profile: params[:talent_profile_id])
    render json: pref_company_sizes, status: 200
  end

  def show
    pref_company_size = PreferredCompanySizeAssociation.find(params[:id])

    authorize pref_company_size

    render json: pref_company_size, status: 200
  end

  def create
    talent_profile = TalentProfile.find(params[:talent_profile_id])

    authorize talent_profile, :add_object?

    pref_company_size = PreferredCompanySizeAssociation.new(preferred_company_size_association_params)
    pref_company_size.talent_profile = talent_profile
    if pref_company_size.save
      render json: pref_company_size, status: 201
    else
      render json: { errors: pref_company_size.errors }, status: 422
    end
  end

  def destroy
    preferred_company_size_association = PreferredCompanySizeAssociation.find(params[:id])

    authorize preferred_company_size_association

    preferred_company_size_association.destroy
    head 204
  end

  private
  def preferred_company_size_association_params
    params.require(:preferred_company_size_association).permit(:company_size_id)
  end

end