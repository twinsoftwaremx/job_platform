class V1::SalaryRangesController < V1::BaseController
  before_action :authenticate, except: [:index]
  after_action :verify_authorized, except: [:index, :show]

  def index
    salary_ranges = policy_scope(SalaryRange)

    render json: salary_ranges, status: 200
  end

  def show
    salary_range = SalaryRange.find(params[:id])

    render json: salary_range, status: 200
  end

  def create
    salary_range = SalaryRange.new(salary_range_params)

    authorize salary_range

    if salary_range.save
      render json: salary_range, status: 201
    else
      render json: { errors: salary_range.errors }, status: 422
    end
  end

  def update
    salary_range = SalaryRange.find(params[:id])

    authorize salary_range

    if salary_range.update(salary_range_params)
      render json: salary_range, status: 204
    else
      render json: { errors: salary_range.errors }, status: 422
    end
  end

  def destroy
    salary_range = SalaryRange.find(params[:id])

    authorize salary_range

    salary_range.destroy
    head 204
  end

  private
  def salary_range_params
    params.require(:salary_range).permit(:name)
  end
end