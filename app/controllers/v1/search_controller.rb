class V1::SearchController < V1::BaseController
  skip_after_action :verify_authorized
  after_action :verify_custom_authorized

  def talent_profile
    options = {
      skills: params[:skills]
    }
    @talent_profiles = TalentProfile.search(params[:q], options)
    render json: @talent_profiles.records, status: 200
  end

  def talent_by_position
    @position = Position.find(params[:id])
    @talent_profiles = TalentFinder.for_position(@position)
    render json: @talent_profiles.to_json, status: 200
  end

  def verify_custom_authorized
    authorize :search, "#{action_name}?".to_sym
  end
end
