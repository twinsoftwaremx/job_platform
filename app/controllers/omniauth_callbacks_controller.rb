class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  TYPES = {
    github: Auth::GithubAuthenticator.new,
    linkedin: Auth::LinkedinAuthenticator.new
  }

  [:github, :linkedin].each do |provider|
    define_method(provider.to_s) do
      authenticator = self.class.find_auth_for(provider)
      result = authenticator.authenticate(auth_hash, auth_params, current_user)
      if result.persisted?
        sign_in result, store: false
        handle_information_retrieval(provider, result.id)
        redirect_to build_frontend_callback(result)
      else
        session["devise.#{provider.to_s}_data"] = env["omniauth.auth"]
        render json: {errors: result.errors.full_messages}, status: 401
      end
    end
  end

  def handle_information_retrieval(provider, user_id)
    case provider
    when :github
      token = env["omniauth.auth"].credentials.token
      ::GithubWorker.perform_async(token, user_id)
    when :linkedin
      access_token = auth_hash.extra.access_token.token
      access_secret = auth_hash.extra.access_token.secret
      ::LinkedinWorker.perform_async(access_token, access_secret, user_id)
    end
  end

  def self.find_auth_for(provider)
    TYPES[provider]
  end

  private
  def auth_hash
    @auth_hash ||= request.env["omniauth.auth"]
  end

  def auth_params
    @auth_params ||= request.env["omniauth.params"]
  end

  def build_frontend_callback(result)
    cipher = Gibberish::AES.new('p4ssw0rd')
    token = cipher.encrypt(result.auth_token)
    if ENV['FRONTEND_URL'].present?
      ENV['FRONTEND_URL'] + "/#/omniauth" + "?token=#{CGI.escape(token)}"
    end
  end
end
