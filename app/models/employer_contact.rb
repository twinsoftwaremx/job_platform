# == Schema Information
#
# Table name: employer_contacts
#
#  id            :integer          not null, primary key
#  name          :string           not null
#  title         :string
#  linked_in_url :string
#  summary       :text
#  phone         :string
#  time_zone     :string
#  account_id    :integer          not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  user_id       :integer
#  deleted_at    :datetime
#
# Indexes
#
#  index_employer_contacts_on_account_id  (account_id)
#  index_employer_contacts_on_deleted_at  (deleted_at)
#  index_employer_contacts_on_user_id     (user_id)
#

class EmployerContact < ActiveRecord::Base
  DEFAULT_RECENT_CONVERSATIONS = 4

  acts_as_paranoid

  has_many :positions, dependent: :destroy
  belongs_to :account, required: true
  belongs_to :user, required: true
  has_one :photo, as: :assetable, class_name: "Photo", dependent: :destroy
  has_many :offers, dependent: :destroy
  has_many :messages, through: :positions, dependent: :destroy

  validates :name, presence: true

  after_create :send_welcome_email
  after_create :create_customer_in_quickbooks

  delegate :billing_customer_id, to: :account

  def photo_id=(id)
    if @asset = Photo.find(id)
      self.photo = @asset
    else
      self.errors.add(:photo, "Asset not found")
    end
  end

  def messages_received
    messages.where(to_user_id: user.id)
  end

  def self.get_recent_conversations(employer_contact_id, count)
    count = (count == 0) ? DEFAULT_RECENT_CONVERSATIONS : count

    sql = %{select max(from_user_id) as from_user_id, position_id, tp.id as talent_profile_id, max(m.created_at) as date from positions p
            inner join messages m on m.position_id=p.id and m.deleted_at is null
            inner join employer_contacts ec on ec.id=p.employer_contact_id and ec.deleted_at is null
            inner join users u on u.id=ec.user_id and u.deleted_at is null
            inner join talents t on (t.user_id=from_user_id or t.user_id=to_user_id) and t.deleted_at is null
            inner join talent_profiles tp on tp.id=t.talent_profile_id and tp.deleted_at is null
            where p.employer_contact_id=#{employer_contact_id}
            and (to_user_id=u.id or from_user_id=u.id) and p.deleted_at is null
            group by position_id, tp.id
            order by date desc
            limit #{count}}
    array = ActiveRecord::Base.connection.execute(sql)
    results = array.map do |row|
      talent_profile = TalentProfile.find(row['talent_profile_id'])
      {
        job_id: row['position_id'],
        talent_profile_name: talent_profile.name,
        photo_url: talent_profile.photo != nil ? talent_profile.photo.url : '',
        last_message_date: row['date'],
        user_id: talent_profile.user.id
      }
    end
    results
  end

private

  def send_welcome_email
    FetchMailer.welcome_employer(self.user.email).deliver_now if self.account.employer_contacts.count == 1
  end

  def create_customer_in_quickbooks
    if self.account.employer_contacts.count == 1 && QuickbooksAuthToken.any?
      Finance::CreateCustomer.new(employer_contact_id: self.id).commit
    end
  end
end
