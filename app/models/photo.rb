# == Schema Information
#
# Table name: assets
#
#  id             :integer          not null, primary key
#  file_id        :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  assetable_id   :integer
#  assetable_type :string
#  type           :string
#  content_type   :string
#  filename       :string
#

class Photo < Asset
end
