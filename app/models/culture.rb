# == Schema Information
#
# Table name: cultures
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  deleted_at :datetime
#
# Indexes
#
#  index_cultures_on_deleted_at  (deleted_at)
#

class Culture < ActiveRecord::Base
  acts_as_paranoid

  has_many :account_culture_associations, dependent: :destroy
  has_many :accounts, through: :account_culture_associations
  has_many :talent_culture_associations, dependent: :destroy
  has_many :talent_profiles, through: :talent_culture_associations, source: :talent_profile
  has_one :photo, as: :assetable, class_name: "Photo", dependent: :destroy

  validates :name, presence: true

  def photo_id=(id)
    if @asset = Photo.find(id)
      self.photo = @asset
    else
      self.errors.add(:photo, "Asset not found")
    end
  end
end
