# == Schema Information
#
# Table name: addresses
#
#  id         :integer          not null, primary key
#  line1      :string
#  line2      :string
#  city       :string
#  state      :string
#  zip        :string
#  type       :string
#  latitude   :float
#  longitude  :float
#  created_at :datetime
#  updated_at :datetime
#  deleted_at :datetime
#
# Indexes
#
#  index_addresses_on_deleted_at  (deleted_at)
#  index_addresses_on_type        (type)
#

class Address < ActiveRecord::Base
  ZIP_REGEX   = /\A\d{5}([\-]\d{4})?\z/

  acts_as_paranoid

  validates_format_of :zip, :with => ZIP_REGEX, :message => 'is invalid'

  # For elastic search
  def coordinates
    "#{latitude},#{longitude}"
  end
end
