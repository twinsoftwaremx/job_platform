# == Schema Information
#
# Table name: interested_locations
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class InterestedLocation < ActiveRecord::Base
  has_many :location_of_interest_association
  has_many :talent_profiles, through: :location_of_interest_association

  validates :name, presence: true
end
