# == Schema Information
#
# Table name: company_sizes
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  deleted_at :datetime
#
# Indexes
#
#  index_company_sizes_on_deleted_at  (deleted_at)
#

class CompanySize < ActiveRecord::Base
  acts_as_paranoid

  has_many :preferred_company_size_associations, dependent: :destroy
  has_many :talent_profiles, through: :preferred_company_size_associations

  validates :name, presence: true
end
