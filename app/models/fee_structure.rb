class FeeStructure
  FEE_PERCENTAGE = 0.15

  attr_reader :id, :standard_fee, :monthly_subscription, :upfront_fee

  def calculate_fees(salary, upfront: UpfrontFee, recurring: RecurringFee)
    @upfront_fee          = upfront.calculate(salary)
    @monthly_subscription = recurring.calculate(salary)
    @standard_fee         = (salary.to_f * FEE_PERCENTAGE).round(2)
    @standard_fee         = 0 unless @standard_fee > 0
  end
end
