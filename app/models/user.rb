# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  auth_token             :string           default("")
#  last_activity_at       :datetime
#  deleted_at             :datetime
#
# Indexes
#
#  index_users_on_auth_token            (auth_token) UNIQUE
#  index_users_on_deleted_at            (deleted_at)
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_last_activity_at      (last_activity_at)
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#

class User < ActiveRecord::Base
  USER_SELECTABLE_ROLES = [:talent, :employer_contact]
  DEFAULT_USER_ROLE = :talent
  rolify
  acts_as_paranoid

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, omniauth_providers: [:github, :linkedin]

  validates :auth_token, uniqueness: true

  before_create :generate_authentication_token!

  has_one :talent, dependent: :destroy
  has_one :talent_profile, through: :talent
  has_one :github_asset, dependent: :destroy
  has_one :linkedin_asset, dependent: :destroy
  has_many :identities, dependent: :destroy

  has_one :employer_contact, dependent: :destroy
  has_one :account, through: :employer_contact

  has_many :messages_sent, class_name: 'Message', foreign_key: 'from_user_id', as: :from_user,
           dependent: :destroy
  has_many :messages_received, class_name: 'Message', foreign_key: 'to_user_id', as: :to_user,
           dependent: :destroy

  def role=(user_role)
    case user_role.to_sym
    when :admin
      self.add_role DEFAULT_USER_ROLE
    when :account_admin
      self.add_role :account_admin
      self.add_role :employer_contact
    else
      self.add_role user_role
    end
  end

  def role
    roles_names = ''
    self.roles.each do |role|
      roles_names += "#{role.name} "
    end
    roles_names.strip()
  end

  def generate_authentication_token!
    begin
      self.auth_token = Devise.friendly_token
    end while self.class.exists?(auth_token: auth_token)
  end

  def online?
    (last_activity_at.present? && last_activity_at > 5.minutes.ago)
  end
end
