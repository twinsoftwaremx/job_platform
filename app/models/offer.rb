# == Schema Information
#
# Table name: offers
#
#  id                      :integer          not null, primary key
#  talent_profile_match_id :integer          not null
#  employer_contact_id     :integer          not null
#  status                  :integer          default(0)
#  company_name            :string
#  position_title          :string
#  manager_name            :string
#  salary_pay_rate         :string
#  start_date              :string
#  benefits                :text
#  legal_verbiage          :text
#  hiring_managers_name    :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  details                 :text
#  job_type                :string
#  decline_reason          :text
#  deleted_at              :datetime
#  upfront_fee             :decimal(, )      default(0.0)
#  recurring_fee           :decimal(, )      default(0.0)
#
# Indexes
#
#  index_offers_on_deleted_at               (deleted_at)
#  index_offers_on_employer_contact_id      (employer_contact_id)
#  index_offers_on_talent_profile_match_id  (talent_profile_match_id)
#

class Offer < ActiveRecord::Base
  after_create :change_match_status

  acts_as_paranoid

  belongs_to :talent_profile_match, required: true
  belongs_to :employer_contact, required: true
  has_many   :invoices

  validates :company_name, :position_title, presence: true

  enum status: [:unanswered, :rejected, :accepted]

  delegate :talent_profile_name, to: :talent_profile_match
  delegate :billing_customer_id, to: :employer_contact

  def salary
    self.salary_pay_rate
  end

  def direct_hire?
    self.job_type == 'Direct Hire'
  end

  private
  def change_match_status
    talent_profile_match.status = TalentProfileMatch.statuses[:offer]
    talent_profile_match.save
  end
end
