# == Schema Information
#
# Table name: skills
#
#  id                :integer          not null, primary key
#  name              :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  skill_category_id :integer
#  deleted_at        :datetime
#
# Indexes
#
#  index_skills_on_deleted_at  (deleted_at)
#

class Skill < ActiveRecord::Base
  acts_as_paranoid

  has_many :skill_associations, dependent: :destroy
  has_many :talent_profiles, through: :skill_associations
  belongs_to :skill_category

  validates :name, presence: true

  def self.by_category_list(skills, category_list)
    ids = category_list.split(',')
    skills = skills.includes(:skill_category).where(skill_category_id: ids).order(:skill_category_id)

    result = []
    current = []
    prev_cat_id = 0
    skills.each do |s|
      cat_id = s.skill_category_id
      if prev_cat_id == 0
        current = []
        prev_cat_id = cat_id
      end

      if prev_cat_id == cat_id
        current << s
      else
        result << current
        current = []
        current << s
        prev_cat_id = cat_id
      end
    end
    result << current
    result
  end
end
