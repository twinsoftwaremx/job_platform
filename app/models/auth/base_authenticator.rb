class Auth::BaseAuthenticator
  def name
    raise NotImplementedError
  end

  def authenticate(hash, params, user)
    raise NotImplementedError
  end

  private
  def process(auth_hash, current_user)
    identity = Identity.find_or_create_by(uid: auth_hash.uid, provider: auth_hash.provider)
    user = current_user ? current_user : identity.user

    if user.nil?
      email = auth_hash.info.email
      user = User.where(email: email).first if email

      if user.nil?
        user = User.new(
          email: email ? email : "#{TEMP_EMAIL_PREFIX}-#{auth.uid}-#{auth.provider}.com",
          password: Devise.friendly_token[0,20]
        )
        user.save!
      end
    end

    if identity.user != user
      identity.user = user
      identity.save!
    end
    user
  end

  def after_process(auth_hash, auth_params, user)
    case auth_params["type"]
    when 'employer'
      # To Be Implemented
    when 'talent'
      unless user.talent_profile.present?
        profile = create_profile_from_hash(auth_hash)
        user.talent_profile = profile if profile.valid?
      end
    else
      # Should never get here but better to be explicit.
    end
    user
  end
end
