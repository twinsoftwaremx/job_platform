class Auth::GithubAuthenticator < Auth::BaseAuthenticator
  def name
    "github"
  end

  def authenticate(auth_hash, auth_params={}, current_user = nil)
    user = process(auth_hash, current_user)
    user = after_process(auth_hash, auth_params, user)
    user
  end

  def create_profile_from_hash(hash)
    avatar = hash.info.image
    name   = hash.info.name
    url    = hash.info.urls["Github"]

    TalentProfile.new.tap do |profile|
      profile.agent      = Agents::NextAgentCommand.new.perform
      profile.time_zone  = "EST"
      profile.name       = name   if name.present?
      profile.github_url = url    if url.present?
      profile.avatar_url = avatar if avatar.present?
      profile.save
    end
  end
end
