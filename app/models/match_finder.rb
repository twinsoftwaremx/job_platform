class MatchFinder
  def self.call(talent)
    new.call(talent)
  end

  def call(talent)
    matches = MatchFinder.for_talent(talent)
    existing_matches = talent.matches

    x = existing_matches.collect(&:id)
    y = matches.collect { |match| match[:id] }

    # This logic checks for differences on new matches vs existing. If new matches
    # returns empty (no matches) we should ignore them, otherwise find the difference
    # in id's.
    new_matches = y.empty? ? [] : (x-y)+(y-x)

    new_matches.each do |match|
      pos = Position.where(id: match).first

      if pos.present?
        match_found = matches.find { |m| m[:id]==match }
        score       = match_found.present? ? match_found[:score]*1000 : 0
        multiple    = match_found.present? ? match_found[:multiple] : true
        new_match   = TalentProfileMatch.create!(talent_profile_id: talent.id, position_id: match, match_score: score, multiple_skills: multiple)

        if match_found[:multiple]
          TalentMatchMailer.email_talent_match(talent, new_match).deliver_later
        else
          TalentMatchMailer.email_talent_match_only_one(talent, new_match).deliver_later
        end

        AgentMailer.talent_fetched(pos, talent).deliver_later
      end
    end
  end

  def self.for_talent(talent)
    options = {}
    options[:skills] = talent.skills.collect(&:name)
    options[:locations] = talent.cities_of_interest.collect(&:name)
    options[:cultures] = talent.cultures.collect(&:name)
    options[:salary_range] =
      talent.salary_range.present? ?
      talent.salary_range.name :
      ""
    [:skills, :salary_range, :locations, :cultures].map {|x| return [] if !options[x].present? }

    results = Position.match(options)
    res = results.map do |r|
      {
        id: r.id.to_i,
        score: r._score,
        multiple: true
      }
    end
    if res.count == 0
      puts '*** trying the match_only_one method'
      results = Position.match_only_one(options)
      res = results.map do |r|
        {
          id: r.id.to_i,
          score: r._score,
          multiple: false
        }
      end
    end
    res
  end
end
