# == Schema Information
#
# Table name: identities
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  provider   :string
#  uid        :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  deleted_at :datetime
#
# Indexes
#
#  index_identities_on_deleted_at  (deleted_at)
#  index_identities_on_user_id     (user_id)
#

class Identity < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :user
  validates :uid, :provider, presence: true
  validates :uid, uniqueness: { scope: :provider}
end
