# == Schema Information
#
# Table name: positions
#
#  id                       :integer          not null, primary key
#  job_title                :string           not null
#  description              :text
#  hours                    :integer          default(0)
#  desired_years_experience :integer          default(0)
#  team_size                :string
#  employment_type_id       :integer          not null
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  employer_contact_id      :integer
#  remote                   :boolean          default(FALSE)
#  location_id              :integer
#  salary_range_id          :integer
#  city_id                  :integer
#  hiring_manager           :string
#  is_deleted               :boolean          default(FALSE)
#  deleted_at               :datetime
#
# Indexes
#
#  index_positions_on_city_id              (city_id)
#  index_positions_on_deleted_at           (deleted_at)
#  index_positions_on_employer_contact_id  (employer_contact_id)
#  index_positions_on_employment_type_id   (employment_type_id)
#

class Position < ActiveRecord::Base
  include Searchable
  include PositionSearchable

  acts_as_paranoid

  after_create :send_slack_new_position_message
  after_save :get_matches
  before_destroy :notify_interested, prepend: true

  has_many :bonus_skill_associations, dependent: :destroy
  has_many :bonus_skills, through: :bonus_skill_associations, source: :skill
  has_many :required_skill_associations, dependent: :destroy
  has_many :required_skills, through: :required_skill_associations, source: :skill
  belongs_to :employment_type, required: true
  belongs_to :employer_contact, required: true
  has_one :account, through: :employer_contact
  belongs_to :location, class_name: 'Location'
  has_many :talent_profile_matches, dependent: :destroy
  has_many :talent_profiles, through: :talent_profile_matches
  has_many :offers, through: :talent_profile_matches
  belongs_to :salary_range
  belongs_to :city
  has_many :messages, dependent: :destroy

  validates :job_title, presence: true

  def bonus_skill_list
    bonus_skills.map(&:name).join(", ")
  end

  def bonus_skill_list=(names)
    self.bonus_skills = names.split(",").map do |m|
      Skill.where(name: m.strip).first_or_create!
    end
  end

  def required_skill_list
    required_skills.map(&:name).join(", ")
  end

  def required_skill_list=(names)
    self.required_skills = names.split(",").map do |m|
      Skill.where(name: m.strip).first_or_create!
    end
  end

  private

  def get_matches
    TalentFinder.call(self)
  end

  def notify_interested
    status = TalentProfileMatch.statuses
    talent = talent_profile_matches.where(status: [
      status[:liked],
      status[:interested],
      status[:offer]
    ]).map(&:talent_profile)

    talent.each do |t|
      PositionMailer.position_removed(self, t).deliver_now
    end
  end

  def send_slack_new_position_message
    message = "#{account.company_name} is now hiring for #{job_title}"
    SlackWorker.new.perform(message,ENV['SLACK_NEW_POSITION_SIGNUP_CHANNEL'],ENV['SLACK_USERNAME'],':thumbsup:')
  end
end
