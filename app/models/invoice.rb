# == Schema Information
#
# Table name: invoices
#
#  id         :integer          not null, primary key
#  offer_id   :integer
#  name       :string           not null
#  category   :integer          not null
#  status     :integer          default(0), not null
#  amount     :decimal(, )      default(0.0), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  due_at     :datetime         not null
#  paid_at    :datetime
#  deleted_at :datetime
#
# Indexes
#
#  index_invoices_on_offer_id  (offer_id)
#

class Invoice < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :offer

  delegate :company_name,        to: :offer, prefix: true
  delegate :billing_customer_id, to: :offer, prefix: true
  delegate :salary,              to: :offer, prefix: true
  delegate :talent_profile_name, to: :offer, prefix: true

  enum category: %i(upfront recurring)
  enum status:   %i(pending due paid canceled)

  scope :due,  ->(date=CURRENT) { where(status: statuses[:due]).where('due_at < ?', date) }
  scope :paid, ->               { where(status: statuses[:paid]).where.not(paid_at: nil) }

  validates :amount, numericality: { greater_than: 0 }
  validates :name,   presence:     true

  CURRENT = Time.zone.now.end_of_month

  def customer_id
    @customer_id ||= offer_billing_customer_id || Finance::Gateway.find_customer(offer_company_name).id
  end

  private_constant :CURRENT
end
