module PositionSearchable
  extend ActiveSupport::Concern

  included do
    # Setup our Elastic Search indexes
    settings index: { number_of_shards: 1, number_of_replicas: 0 } do
      mapping dynamic: 'false' do
        indexes :id, type: 'integer', index: :not_analyzed
        indexes :salary_range, analyzer: 'keyword'
        indexes :required_skills, analyzer: 'keyword'
        indexes :bonus_skills, analyzer: 'keyword'
        indexes :cultures, analyzer: 'keyword'
        indexes :city, analyzer: 'keyword'
      end
    end

    # Customize the JSON serialization for Elasticsearch
    def as_indexed_json(options={})
      hash = self.as_json()
      hash['cultures'] = self.account.cultures.collect(&:name)
      hash[:required_skills] = self.required_skills.collect(&:name)
      hash[:bonus_skills] = self.bonus_skills.collect(&:name)
      hash[:city] = self.city.name
      hash[:salary_range] = self.salary_range.name
      hash
    end

    def self.match(options={})
      @search_definition = Elasticsearch::DSL::Search.search do
        query do
          bool do
            must do
              terms required_skills: options[:skills], boost: 2.0, minimum_should_match: 3
            end
            must do
              terms city: options[:locations]
            end
            must do
              terms cultures: options[:cultures]
            end
            must do
              term salary_range: options[:salary_range]
            end
          end
        end
      end
      self.__elasticsearch__.search(@search_definition)
    end

    def self.match_only_one(options={})
      @search_definition = Elasticsearch::DSL::Search.search do
        query do
          bool do
            must do
              terms required_skills: options[:skills], boost: 2.0, minimum_should_match: 1
            end
            must do
              terms city: options[:locations]
            end
            must do
              terms cultures: options[:cultures], minimum_should_match: 1
            end
            must do
              term salary_range: options[:salary_range]
            end
          end
        end
      end
      self.__elasticsearch__.search(@search_definition)
    end
  end
end
