# == Schema Information
#
# Table name: github_assets
#
#  id                :integer          not null, primary key
#  follower_count    :integer
#  public_repo_count :integer
#  user_id           :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  deleted_at        :datetime
#
# Indexes
#
#  index_github_assets_on_deleted_at  (deleted_at)
#  index_github_assets_on_user_id     (user_id)
#

class GithubAsset < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :user
end
