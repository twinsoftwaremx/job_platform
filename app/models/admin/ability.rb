class Admin::Ability
  include CanCan::Ability

  def initialize(user)
    if user.admin? && !user.agent_admin?
      can :manage, :all
    else
      can :manage, ActiveAdmin::Page, name: "Dashboard"
      can :read, TalentProfile
      can :read, Account
      can :create, Message
      can :read, Message, :from_user_id => user.id
      can :read, Message, :to_user_id => user.id
    end
  end
end
