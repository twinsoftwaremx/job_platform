# == Schema Information
#
# Table name: states
#
#  id           :integer          not null, primary key
#  name         :string           not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  abbreviation :string           not null
#  deleted_at   :datetime
#
# Indexes
#
#  index_states_on_deleted_at  (deleted_at)
#

class State < ActiveRecord::Base
  acts_as_paranoid

  has_many :cities, dependent: :destroy

  validates :name, presence: true
  validates :abbreviation, presence: true
end
