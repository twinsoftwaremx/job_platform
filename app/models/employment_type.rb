# == Schema Information
#
# Table name: employment_types
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  deleted_at :datetime
#
# Indexes
#
#  index_employment_types_on_deleted_at  (deleted_at)
#

class EmploymentType < ActiveRecord::Base
  acts_as_paranoid
end
