class TalentFinder

  def self.call(position)
    matches = TalentFinder.for_position(position)

    matches.each do |match|
      record = TalentProfileMatch.where(talent_profile_id: match[:id], position_id: position.id)
      if record.blank?
        new_match = TalentProfileMatch.create!(talent_profile_id: match[:id], position_id: position.id,
                                               match_score: match[:score] * 1000,
                                               multiple_skills: match[:multiple])
        talent = TalentProfile.find(match[:id])
        if match[:multiple]
          TalentMatchMailer.email_talent_match(talent, new_match).deliver_later
        else
          TalentMatchMailer.email_talent_match_only_one(talent, new_match).deliver_later
        end

        AgentMailer.talent_fetched(position, talent).deliver_later
      end
    end
  end

  def self.for_position(position)
    return [] if position.nil?
    options = {}
    options[:for_position] = true
    options[:required_skills] = position.required_skills.collect(&:name)
    options[:bonus_skills] = position.bonus_skills.collect(&:name)
    options[:salary_range] = position.salary_range.name
    options[:city] = position.city.name
    options[:cultures] = position.account.present? ? position.account.cultures.collect(&:name) : []

    results = TalentProfile.match(options)
    res = results.map do |r|
      {
        id: r.id.to_i,
        score: r._score,
        multiple: true
      }
    end
    if res.count == 0
      res = results.map do |r|
        {
          id: r.id.to_i,
          score: r._score,
          multiple: false
        }
      end
    end
    res
  end
end
