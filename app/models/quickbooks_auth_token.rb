# == Schema Information
#
# Table name: quickbooks_auth_tokens
#
#  id            :integer          not null, primary key
#  access_token  :string           not null
#  access_secret :string           not null
#  company_id    :string           not null
#  expires_at    :datetime         default(Sun, 11 Sep 2016 02:19:11 UTC +00:00), not null
#  reconnect_at  :datetime         default(Thu, 11 Aug 2016 02:19:11 UTC +00:00), not null
#  created_at    :datetime
#  updated_at    :datetime
#

class QuickbooksAuthToken < ActiveRecord::Base
  scope :reconnect_today, -> { where(reconnect_at: Time.zone.now.beginning_of_day..Time.zone.now.end_of_day) }
end
