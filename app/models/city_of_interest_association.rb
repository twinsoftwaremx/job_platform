# == Schema Information
#
# Table name: city_of_interest_associations
#
#  id                :integer          not null, primary key
#  talent_profile_id :integer
#  city_id           :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  deleted_at        :datetime
#
# Indexes
#
#  index_city_of_interest_associations_on_city_id            (city_id)
#  index_city_of_interest_associations_on_deleted_at         (deleted_at)
#  index_city_of_interest_associations_on_talent_profile_id  (talent_profile_id)
#

class CityOfInterestAssociation < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :city
  belongs_to :talent_profile
end
