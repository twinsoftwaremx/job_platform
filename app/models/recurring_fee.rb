class RecurringFee
  LOW    = 1_000
  HIGH   = 2_000
  MAX    = 76_000
  MONTHS = 36
  PERCENTAGE = 0.15

  module ClassMethods
    def calculate(salary)
      new(salary).calculate
    end

    def term
      MONTHS
    end
  end

  def initialize(salary, upfront: UpfrontFee)
    @salary  = salary.to_s.gsub(/[^\d\.]/, '').to_f
    @upfront = upfront
  end

  def calculate
    ((standard_fee/MONTHS)+4).round(-1).tap do |fee|
      fee = 0 unless fee > 0
    end
  end

private
  attr_reader      :salary
  attr_reader      :upfront
  private_constant :LOW
  private_constant :HIGH
  private_constant :MAX
  private_constant :MONTHS

  def standard_fee
    @standard_fee ||= (salary * PERCENTAGE).round(2)
  end

  def upfront_fee
    @upfront_fee ||= begin
      upfront.calculate(offer)
    end
  end

  extend ClassMethods
end
