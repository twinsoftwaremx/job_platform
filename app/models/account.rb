# == Schema Information
#
# Table name: accounts
#
#  id                  :integer          not null, primary key
#  company_name        :string
#  phone               :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  summary             :text
#  benefits            :text
#  perks               :text
#  github_url          :string
#  company_site_url    :string
#  address_id          :integer
#  culture             :integer          default(0)
#  agent_id            :integer
#  company_size_id     :integer
#  year_founded        :integer
#  revenue             :string
#  who_we_are          :text
#  what_we_do          :text
#  video_url           :string
#  deleted_at          :datetime
#  billing_customer_id :string
#
# Indexes
#
#  index_accounts_on_company_size_id  (company_size_id)
#  index_accounts_on_deleted_at       (deleted_at)
#

class Account < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :address, class_name: 'FullAddress'
  has_many :employer_contacts, dependent: :destroy
  has_many :positions, through: :employer_contacts
  belongs_to :agent
  has_many :account_culture_associations, dependent: :destroy
  has_many :cultures, through: :account_culture_associations
  has_one :logo, as: :assetable, class_name: "Photo", dependent: :destroy
  belongs_to :company_size

  validates :company_name, presence: true

  after_create :send_slack_signup_message
  after_destroy :send_slack_leaving_message

  def culture_list
    cultures.map(&:name).join(", ")
  end

  def culture_list=(list)
    self.cultures = list.split(",").map do |m|
      Culture.where(name: m.strip).first_or_create!
    end
  end

  def logo_id=(id)
    if @asset = Photo.find(id)
      self.logo = @asset
    else
      self.errors.add(:logo, "Asset not found")
    end
  end

  def create_logo(base64_string)
    self.logo = Asset.new(type:'Photo')
    header, data = base64_string.split(',')
    file = Tempfile.new('logo')
    file.binmode
    tmp_file_name = file.path
    file.write(Base64.decode64(data))
    file.close
    file = File.open(tmp_file_name)
    self.logo.file = file
    file.close
  end

  private

  def send_slack_signup_message
    message = "Good news, #{company_name} is ready to fetch some talent. There are now #{Account.count} employers registered"
    SlackWorker.new.perform(message,ENV['SLACK_EMPLOYER_SIGNUP_CHANNEL'],ENV['SLACK_USERNAME'],':thumbsup:')
  end

  def send_slack_leaving_message
    message = "Bummer, #{company_name} decided to leave fetch. There are now #{Account.count} employers registered"
    SlackWorker.new.perform(message,ENV['SLACK_EMPLOYER_LEAVING_CHANNEL'],ENV['SLACK_USERNAME'],':thumbsdown:')
  end
end
