class UpfrontFee
  LOW  = 1_000
  HIGH = 2_000
  MAX  = 76_000
  PERCENTAGE = 0.5

  module ClassMethods
    def calculate(salary)
      new(salary).calculate
    end
  end

  def initialize(salary)
    @salary = salary.to_s.gsub(/[^\d\.]/, '').to_f
  end

  def calculate
    return LOW unless salary > MAX
    HIGH
  end

private
  attr_reader      :salary
  private_constant :LOW
  private_constant :HIGH
  private_constant :MAX

  extend ClassMethods
end
