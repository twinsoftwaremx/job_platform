# == Schema Information
#
# Table name: skill_categories
#
#  id                 :integer          not null, primary key
#  name               :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  deleted_at         :datetime
#  vertical_market_id :integer
#
# Indexes
#
#  index_skill_categories_on_deleted_at          (deleted_at)
#  index_skill_categories_on_vertical_market_id  (vertical_market_id)
#

class SkillCategory < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :vertical_market
  has_many :skills, dependent: :destroy

  validates :name, presence: true
end
