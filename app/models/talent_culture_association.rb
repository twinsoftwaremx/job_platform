# == Schema Information
#
# Table name: talent_culture_associations
#
#  id                :integer          not null, primary key
#  talent_profile_id :integer
#  culture_id        :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  deleted_at        :datetime
#
# Indexes
#
#  index_talent_culture_associations_on_culture_id         (culture_id)
#  index_talent_culture_associations_on_deleted_at         (deleted_at)
#  index_talent_culture_associations_on_talent_profile_id  (talent_profile_id)
#

class TalentCultureAssociation < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :talent_profile, required: true
  belongs_to :culture, required: true
end
