# == Schema Information
#
# Table name: salary_ranges
#
#  id                :integer          not null, primary key
#  name              :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  deleted_at        :datetime
#  compensation_type :string           default("Direct Hire"), not null
#  sort_order        :integer
#
# Indexes
#
#  index_salary_ranges_on_deleted_at  (deleted_at)
#

class SalaryRange < ActiveRecord::Base
  acts_as_paranoid

  has_many :talent_profiles
  has_many :positions

  validates :name, presence: true
end
