# == Schema Information
#
# Table name: admin_users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  agent_id               :integer
#  auth_token             :string           default("")
#  last_activity_at       :datetime
#  deleted_at             :datetime
#
# Indexes
#
#  index_admin_users_on_auth_token            (auth_token) UNIQUE
#  index_admin_users_on_deleted_at            (deleted_at)
#  index_admin_users_on_email                 (email) UNIQUE
#  index_admin_users_on_last_activity_at      (last_activity_at)
#  index_admin_users_on_reset_password_token  (reset_password_token) UNIQUE
#

class AdminUser < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  acts_as_paranoid
  belongs_to :agent

  validates :auth_token, uniqueness: true

  before_create :generate_authentication_token!

  has_many :messages_sent, class_name: 'Message', foreign_key: 'from_user_id', as: :from_user, dependent: :destroy
  has_many :messages_received, class_name: 'Message', foreign_key: 'to_user_id', as: :to_user, dependent: :destroy

  def admin?
    true
  end

  def agent_admin?
    agent_id.present?
  end

  def generate_authentication_token!
    begin
      self.auth_token = Devise.friendly_token
    end while self.class.exists?(auth_token: auth_token)
  end

  def online?
    (last_activity_at.present? && last_activity_at > 5.minutes.ago)
  end
end
