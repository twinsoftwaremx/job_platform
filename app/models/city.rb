# == Schema Information
#
# Table name: cities
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  state_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  deleted_at :datetime
#
# Indexes
#
#  index_cities_on_deleted_at  (deleted_at)
#  index_cities_on_state_id    (state_id)
#

class City < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :state

  validates :name, presence: true

  def full_name
    "#{name}, #{state.abbreviation}"
  end
end
