# == Schema Information
#
# Table name: talent_feedbacks
#
#  id                :integer          not null, primary key
#  text              :text
#  talent_profile_id :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_talent_feedbacks_on_talent_profile_id  (talent_profile_id)
#

class TalentFeedback < ActiveRecord::Base
  belongs_to :talent_profile

  validates :text, presence: true
end
