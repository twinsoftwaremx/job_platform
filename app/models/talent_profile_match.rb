# == Schema Information
#
# Table name: talent_profile_matches
#
#  id                :integer          not null, primary key
#  position_id       :integer
#  talent_profile_id :integer
#  match_score       :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  status            :integer          default(0)
#  deleted_at        :datetime
#  multiple_skills   :boolean          default(TRUE)
#
# Indexes
#
#  index_talent_profile_matches_on_deleted_at         (deleted_at)
#  index_talent_profile_matches_on_position_id        (position_id)
#  index_talent_profile_matches_on_talent_profile_id  (talent_profile_id)
#

class TalentProfileMatch < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :position, required: true
  belongs_to :talent_profile, required: true
  has_one :offer, dependent: :destroy

  enum status: [:new_match, :liked, :rejected, :interested, :offer]

  scope :ranked, -> { not_rejected.order('match_score ASC') }
  scope :not_rejected, -> { where(status: [statuses[:new_match], statuses[:liked], statuses[:interested], statuses[:offer]]) }

  delegate :name, to: :talent_profile, prefix: true

  def matching_skills
    if multiple_skills
      talent_profile.skills
    else
      talent_profile.skills & position.required_skills
    end
  end
end
