# == Schema Information
#
# Table name: messages
#
#  id             :integer          not null, primary key
#  body           :text             not null
#  to_user_id     :integer          not null
#  from_user_id   :integer          not null
#  position_id    :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  from_user_type :string           default("User")
#  to_user_type   :string           default("User")
#  deleted_at     :datetime
#
# Indexes
#
#  index_messages_on_deleted_at    (deleted_at)
#  index_messages_on_from_user_id  (from_user_id)
#  index_messages_on_position_id   (position_id)
#  index_messages_on_to_user_id    (to_user_id)
#

class Message < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :to_user, required: true, polymorphic: true
  belongs_to :from_user, required: true, polymorphic: true
  belongs_to :position

  validates :body, presence: true

  after_create :notify_users

  def notify_users
    ::MessageMailer.notify_users(self).deliver_now!

    # No need to notify agent of messages sent to or from Agent.
    unless from_user.is_a?(AdminUser) || to_user.is_a?(AdminUser)
      if from_user.has_role? :talent
        AgentMailer.talent_messaged(position, from_user, body).deliver_now
      else
        AgentMailer.employer_messaged(position, to_user, body).deliver_now
      end
    end
  end

  def get_user_real_name(user)
    if user.is_a? User
      if user.talent_profile.present?
        name = user.talent_profile.name
      else
        name = user.employer_contact.present? ? user.employer_contact.name : user.email
      end
    else
      name = user.agent.present? ? user.agent.name : user.email
    end
    name
  end

  def get_user_link(user)
    if user.is_a? User
      if user.talent_profile.present?
        ['admin', user.talent_profile]
      elsif user.employer_contact.present?
        ['admin', user.employer_contact]
      else
        ''
      end
    else
      ''
    end
  end
end
