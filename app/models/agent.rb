# == Schema Information
#
# Table name: agents
#
#  id         :integer          not null, primary key
#  name       :string           default(""), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  deleted_at :datetime
#
# Indexes
#
#  index_agents_on_deleted_at  (deleted_at)
#

class Agent < ActiveRecord::Base
  acts_as_paranoid

  has_one :admin_user, dependent: :destroy
  has_many :talent_profiles
  has_many :talent_profile_matches, through: :talent_profiles
  has_many :accounts
  has_one :photo, as: :assetable, class_name: 'Photo', dependent: :destroy

  validates :name, presence: true

  def admin?
    true
  end

  def photo_id=(id)
    if @asset = Photo.find(id)
      self.photo = @asset
    else
      self.errors.add(:photo, "Asset not found")
    end
  end

  def messages
    admin_user.messages_sent+admin_user.messages_received
  end
end
