class BillingPlanWorker
  include Sidekiq::Worker

  def perform(offer_id, strategy: :schedule)
    command(strategy).new(offer_id: offer_id).perform
  end

  def command(strategy)
    registry.fetch(strategy) { raise StandardError "no #{strategy} strategy found" }
  end

private

  def registry
    @registry ||=
    {
      schedule: Invoices::ScheduleBillingPlan,
      cancel:   Invoices::CancelBillingPlan
    }
  end

end
