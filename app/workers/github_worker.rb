class GithubWorker
  include Sidekiq::Worker

  def perform(oauth_token, user_id)
    g = ::Github.new(oauth_token: oauth_token)
    public_repo_count = g.users.get.public_repos
    follower_count = g.users.get.followers
    g_asset = ::GithubAsset.first_or_create(user_id: user_id)
    g_asset.public_repo_count = public_repo_count
    g_asset.follower_count = follower_count
    g_asset.save
  end
end
