class WeeklyMatchesWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  recurrence { weekly }

  def perform
    begin
      TalentProfile.all.each do |talent|
        matches = talent.talent_profile_matches.where('created_at > ?', 1.week.ago)
        if matches.count > 0
          TalentMatchMailer.email_weekly_matches(talent, matches).deliver_now
        end
      end
    rescue Exception => e
      Bugsnag.notify(e)
      logger.info(e.inspect)
    end
  end
end
