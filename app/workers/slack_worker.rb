require 'slack-notify'

#Emoji Cheat Sheet: http://www.emoji-cheat-sheet.com/
class SlackWorker
  include Sidekiq::Worker

  def perform(message, channel, username, emoji)
    begin
      if should_notify?
        webhook_url = ENV['SLACK_WEBHOOK_URL']
        user_name = username ||= ENV['SLACK_USERNAME']

        if webhook_url.present? && user_name.present? && message.present?
          client = SlackNotify::Client.new(
            webhook_url: webhook_url,
            channel: channel,
            username: user_name,
            icon_emoji: emoji,
          )
          client.notify(message)
        end
      end
      true
    rescue => exception
      Bugsnag.notify(exception)
      false
    end
  end

private
  def should_notify?
    Rails.env.production? || ENV['FETCH_ENV'] == 'production'.freeze
  end
end
