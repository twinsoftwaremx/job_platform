class CommandWorker
  include Sidekiq::Worker

  def perform(command_type, attributes)
    command_klass = command_type.constantize
    command       = command_klass.new(attributes)
    command.perform
  end
end
