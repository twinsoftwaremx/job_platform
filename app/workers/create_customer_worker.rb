class CreateCustomerWorker
  include Sidekiq::Worker

  def perform(employer_contact_id)
    Finance::CreateCustomer.new(employer_contact_id: employer_contact_id).perform
  end
end
