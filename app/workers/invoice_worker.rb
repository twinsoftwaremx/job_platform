class InvoiceWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  recurrence do
    central_time = Time.current.in_time_zone('Central Time (US & Canada)').change(hour: 10)
    daily.hour_of_day central_time.utc.hour
  end

  def perform
    begin
      Invoice
        .due(Time.zone.now.end_of_day)
        .map { |invoice| Invoices::CreateInvoice.new(invoice_id: invoice.id).perform }
    rescue => e
      Bugsnag.notify(e)      
      logger.info(e.inspect)
    end
  end
end
