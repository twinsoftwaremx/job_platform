class TokenRenewerWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  recurrence do
    central_time = Time.current.in_time_zone('Central Time (US & Canada)').change(hour: 7)
    daily.hour_of_day central_time.utc.hour
  end

  def perform
    QuickbooksAuthToken.reconnect_today.each do |record|
      service      = Quickbooks::Service::AccessToken.new
      access_token = OAuth::AccessToken.new(qb_oauth_consumer, record.access_token, record.access_secret)

      service.access_token = access_token
      service.company_id   = record.company_id
      result               = service.renew

      record.access_token  = result.token
      record.access_secret = result.secret
      record.expires_at    = 6.months.from_now.utc
      record.reconnect_at  = 5.months.from_now.utc
      record.save!
    end
  end

  def qb_oauth_consumer
    @qb_oauth_consumer ||= OAuth::Consumer.new \
      ENV['QUICKBOOKS_OAUTH_CONSUMER_KEY'],
      ENV['QUICKBOOKS_OAUTH_CONSUMER_SECRET'],
      site:               'https://oauth.intuit.com',
      request_token_path: '/oauth/v1/get_request_token',
      authorize_url:      'https://appcenter.intuit.com/Connect/Begin',
      access_token_path:  '/oauth/v1/get_access_token'
  end
end
