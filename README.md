# api.fetch.com

### ENV Variables

In `.env`, please refer to the Isotope11 Clients' datavault for the relevant keys.

    SECRET_KEY_BASE="393jfkdhf83hszgej347Hk383hK38fhkl2l3"
    AWS_ACCESS_KEY="xxxx"
    AWS_SECRET_KEY="xxxxx"
    AWS_S3_REGION="us-east-1"
    AWS_S3_BUCKET="fetch-api-development"
    MAILGUN_USERNAME="xxx"
    MAILGUN_PASSWORD="xxx"
    MAILGUN_DOMAIN="xxx"
    HOSTNAME="http://localhost:3000/"
    GITHUB_KEY="xxxx"
    GITHUB_SECRET="xxxx"
    LINKEDIN_KEY="<YOUR CONSUMER KEY>"
    LINKEDIN_SECRET="<YOUR CONSUMER SECRET KEY>"
    REDIS_URL="redis://127.0.0.1:6379"
    ELASTICSEARCH_URL="http://localhost:9200"
    FRONTEND_URL="http://localhost:3000"
    DATABASE_TEST_USERNAME="fetch"
    DATABASE_URL="postgresql://localhost:5432/fetch_dev"
    SLACK_WEBHOOK_URL='https://hooks.slack.com/services/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'
    SLACK_USERNAME='fetch'
    SLACK_EMPLOYER_SIGNUP_CHANNEL='#signups_employer'
    SLACK_EMPLOYER_LEAVING_CHANNEL='#leaving_employer'
    SLACK_TALENT_SIGNUP_CHANNEL='#signups_talent'
    SLACK_TALENT_LEAVING_CHANNEL='#leaving_talent'
    SLACK_NEW_POSITION_SIGNUP_CHANNEL='#new_positions'

### Database

To create a database user use

    `createuser -s -r fetch` where fetch is the user name you setup in the `DATABASE_TEST_USERNAME` variable.

To setup the database for the first time

    rake db:create db:migrate db:seed

## Redis

You will need to setup redis and configure the url in the `REDIS_URL` variable.
On OSX you can use this guide https://medium.com/@petehouston/install-and-config-redis-on-mac-os-x-via-homebrew-eb8df9a4f298#.2iglsris3

### Elastic Search in Development

    brew install elasticsearch

To Start elasticsearch now

    launchctl load ~/Library/LaunchAgents/homebrew.mxcl.elasticsearch.plist

To have Elastic Search run at login you can use a Launch Agent

    ln -sfv /usr/local/opt/elasticsearch/*.plist ~/Library/LaunchAgents

ES should be running on port 9200

Run `rake environment elasticsearch:import:all DIR=app/models FORCE=y`
to force reindexing.

Tests use a mock elastic-search cluster but still requires the ES libraries. If tests are failing
sporadically for you and spitting out "Address in use errors", check the default 9200 port if in use
with `sudo lsof -i :9200` and kill it.

### Documentation

To generate docs:

	$ rake docs:generate

Docs are reachable at:
`/docs/api`

### TODO

#### Papertrail

Once we have servers, these tasks should be done to enable papertrail logging


**Find Out Which Logger System is Running**

```
ls -d /etc/*syslog*
```

If it's Ubuntu it'll probably be `rsyslog`

edit `/etc/rsyslog.conf` as root and add the following line to the bottom:

```
*.*          @<SUBDOMAIN>.papertrailapp.com:<PORT>
```

then `sudo service rsyslog restart`
