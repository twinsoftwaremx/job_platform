# == Schema Information
#
# Table name: interested_locations
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :interested_location do
    name { Faker::Address.city }
  end
end
