# == Schema Information
#
# Table name: linkedin_assets
#
#  id              :integer          not null, primary key
#  recommendations :json
#  skills          :json
#  positions       :json
#  user_id         :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  profile         :json
#  deleted_at      :datetime
#
# Indexes
#
#  index_linkedin_assets_on_deleted_at  (deleted_at)
#  index_linkedin_assets_on_user_id     (user_id)
#

FactoryGirl.define do
  factory :linkedin_asset do
    recommendations ""
skills ""
positions ""
user nil
  end

end
