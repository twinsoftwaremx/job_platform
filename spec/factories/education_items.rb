# == Schema Information
#
# Table name: education_items
#
#  id                :integer          not null, primary key
#  college           :string
#  year              :integer
#  degree            :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  talent_profile_id :integer
#

FactoryGirl.define do
  factory :education_item do
    association :talent_profile
    college "MyString"
    year 2006
    degree "BA"
  end
end
