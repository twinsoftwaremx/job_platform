# == Schema Information
#
# Table name: identities
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  provider   :string
#  uid        :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  deleted_at :datetime
#
# Indexes
#
#  index_identities_on_deleted_at  (deleted_at)
#  index_identities_on_user_id     (user_id)
#

FactoryGirl.define do
  factory :identity do
    association :user
    provider "github"
    uid { Random.rand(9999) }
  end
end
