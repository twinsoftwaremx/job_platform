# == Schema Information
#
# Table name: messages
#
#  id             :integer          not null, primary key
#  body           :text             not null
#  to_user_id     :integer          not null
#  from_user_id   :integer          not null
#  position_id    :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  from_user_type :string           default("User")
#  to_user_type   :string           default("User")
#  deleted_at     :datetime
#
# Indexes
#
#  index_messages_on_deleted_at    (deleted_at)
#  index_messages_on_from_user_id  (from_user_id)
#  index_messages_on_position_id   (position_id)
#  index_messages_on_to_user_id    (to_user_id)
#

FactoryGirl.define do
  factory :message, class: "Message" do
    body "MyText"
    association :from_user, factory: :user
    position

    after(:build) do |message, evaluator|
      message.to_user = create(:talent_profile).user unless message.to_user.present?
    end
  end

  factory :admin_user_message, class: "Message" do
    body "MyText"
    association :to_user, factory: :admin_user
    association :from_user, factory: :user
    position
  end
end
