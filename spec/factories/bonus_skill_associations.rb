# == Schema Information
#
# Table name: bonus_skill_associations
#
#  id          :integer          not null, primary key
#  position_id :integer
#  skill_id    :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_bonus_skill_associations_on_position_id  (position_id)
#  index_bonus_skill_associations_on_skill_id     (skill_id)
#

FactoryGirl.define do
  factory :bonus_skill_association do
    association :position
    association :skill
  end

end
