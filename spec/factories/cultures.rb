# == Schema Information
#
# Table name: cultures
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  deleted_at :datetime
#
# Indexes
#
#  index_cultures_on_deleted_at  (deleted_at)
#

FactoryGirl.define do
  factory :culture do
    name { Faker::Lorem.word }
  end

end
