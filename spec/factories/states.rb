# == Schema Information
#
# Table name: states
#
#  id           :integer          not null, primary key
#  name         :string           not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  abbreviation :string           not null
#  deleted_at   :datetime
#
# Indexes
#
#  index_states_on_deleted_at  (deleted_at)
#

FactoryGirl.define do
  factory :state do
    name "MyString"
    abbreviation "MS"
  end

end
