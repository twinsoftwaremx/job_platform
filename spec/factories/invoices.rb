# == Schema Information
#
# Table name: invoices
#
#  id         :integer          not null, primary key
#  offer_id   :integer
#  name       :string           not null
#  category   :integer          not null
#  status     :integer          default(0), not null
#  amount     :decimal(, )      default(0.0), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  due_at     :datetime         not null
#  paid_at    :datetime
#  deleted_at :datetime
#
# Indexes
#
#  index_invoices_on_offer_id  (offer_id)
#

FactoryGirl.define do
  factory :invoice do
    offer
    name     { Faker::Name.title }
    category 0
    status   0
    amount   1.0
    due_at   Time.zone.now
  end
end
