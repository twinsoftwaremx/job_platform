# == Schema Information
#
# Table name: skill_categories
#
#  id                 :integer          not null, primary key
#  name               :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  deleted_at         :datetime
#  vertical_market_id :integer
#
# Indexes
#
#  index_skill_categories_on_deleted_at          (deleted_at)
#  index_skill_categories_on_vertical_market_id  (vertical_market_id)
#

FactoryGirl.define do
  factory :skill_category do
    name "MyString"
    vertical_market

    trait :with_skill_list do
      transient do
        number_of_skills 1
      end

      after(:create) do |category, evaluator|
        create_list(:skill, evaluator.number_of_skills, skill_category: category)
      end
    end
  end
end
