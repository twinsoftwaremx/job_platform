# == Schema Information
#
# Table name: vertical_markets
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  deleted_at :datetime
#
# Indexes
#
#  index_vertical_markets_on_deleted_at  (deleted_at)
#

FactoryGirl.define do
  factory :vertical_market do
    name "MyString"

    trait :with_skill_categories_list do
      transient do
        number_of_skill_categories 1
      end

      after(:create) do |vertical, evaluator|
        create_list(:skill_category, evaluator.number_of_skill_categories, vertical_market: vertical)
      end
    end
  end

end
