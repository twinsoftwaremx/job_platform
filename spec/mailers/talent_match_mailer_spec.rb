require "rails_helper"

RSpec.describe TalentMatchMailer, type: :mailer do
  before do
    TalentProfile.any_instance.stub(:get_matches)
    Position.any_instance.stub(:get_matches)
  end

  describe "send test" do
    let(:email) { Faker::Internet.email }
    let(:mail) { TalentMatchMailer.send_test(email) }

    it "renders the subject" do
      expect(mail.subject).to eql('Welcome to My Awesome Site')
    end

    it "renders the sender email" do
      expect(mail.from).to include("noreply@api.fetch.com")
    end

    it "assigns @email" do
      expect(mail.body.encoded).to match(email)
    end
  end

  describe "sent talent match summary" do
    let(:talent) { FactoryGirl.create(:talent_profile) }
    let(:photo) { create(:photo) }
    let(:account) { create(:account, logo: photo) }
    let(:employer_contact) { create(:employer_contact, account: account) }
    let(:position) { FactoryGirl.create(:position, :with_required_skill_list, number_of_skills: 3, employer_contact: employer_contact) }
    let(:talent_profile_match) { create(:talent_profile_match, talent_profile: talent, position: position) }
    let(:mail) { TalentMatchMailer.email_talent_match(talent, talent_profile_match) }

    it "renders the subject" do
      expect(mail.subject).to eql("You've been Fetched!")
    end

    it "renders the sender email" do
      expect(mail.from).to include("noreply@api.fetch.com")
    end

    it "sends to the talent email" do
      expect(mail.to).to include(talent.user.email)
    end
  end

  describe "issues when sending the talent match email" do
    let(:talent) { FactoryGirl.create(:talent_profile) }
    let(:photo) { create(:photo) }
    let(:account) { create(:account, logo: photo) }
    let(:employer_contact) { create(:employer_contact, account: account) }
    let(:position) { FactoryGirl.create(:position, :with_required_skill_list, number_of_skills: 3, employer_contact: employer_contact) }
    let(:talent_profile_match) { create(:talent_profile_match, talent_profile: talent, position: position) }

    it "should not send the email if the employer_contact information is missing" do
      talent_profile_match.position.employer_contact = nil
      mail = TalentMatchMailer.email_talent_match(talent, talent_profile_match)
      expect(mail.to).to be nil
    end

    it "should not send the email if the account information is missing" do
      talent_profile_match.position.account = nil
      mail = TalentMatchMailer.email_talent_match(talent, talent_profile_match)
      expect(mail.to).to be nil
    end

    it "should not send the email if the talent information is missing" do
      talent = nil
      mail = TalentMatchMailer.email_talent_match(talent, talent_profile_match)
      expect(mail.to).to be nil
    end

    it "should send the email even if the account does not have a logo" do
      talent_profile_match.position.employer_contact.account.logo = nil
      mail = TalentMatchMailer.email_talent_match(talent, talent_profile_match)
      expect(mail.to).to include(talent.user.email)
    end
  end

  describe "sent talent interest" do
    let(:talent) { FactoryGirl.create(:talent_profile) }
    let(:match) { create(:talent_profile_match, talent_profile: talent)}
    let(:mail) { TalentMatchMailer.email_talent_interest(talent, match) }

    it "renders the subject" do
      expect(mail.subject).to eql('Talent is interested in a position!')
    end

    it "renders the sender email" do
      expect(mail.from).to include("noreply@api.fetch.com")
    end

    it "sends to the employer email" do
      expect(mail.to).to include(match.position.employer_contact.user.email)
    end
  end

  describe "issues when sending the talent interest email" do
    let(:talent) { FactoryGirl.create(:talent_profile) }
    let(:match) { create(:talent_profile_match, talent_profile: talent)}

    it "should not send the message if the employer_contact information is missing" do
      match.position.employer_contact = nil
      email = TalentMatchMailer.email_talent_interest(talent, match)
      expect(email.to).to be nil
    end

    it "should not send the message if the position information is missing" do
      match.position = nil
      email = TalentMatchMailer.email_talent_interest(talent, match)
      expect(email.to).to be nil
    end

    it "should not send the message if the talent information is missing" do
      talent = nil
      email = TalentMatchMailer.email_talent_interest(talent, match)
      expect(email.to).to be nil
    end
  end
end
