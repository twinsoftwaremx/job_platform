# Preview all emails at http://localhost:3000/rails/mailers/talent_match_mailer
class TalentMatchMailerPreview < ActionMailer::Preview
  def email_talent_interest
    TalentMatchMailer.email_talent_interest(TalentProfile.first, TalentProfileMatch.first)
  end
end
