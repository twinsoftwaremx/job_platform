# Preview all emails at http://localhost:3000/rails/mailers/message_mailer
class MessageMailerPreview < ActionMailer::Preview
  def notify_users
    message = Message.last || Message.new(to_user: User.first, from_user: User.last, body: 'Test Message')
    MessageMailer.notify_users(message)
  end
end
