Geocoder.configure(:lookup => :test)
Geocoder::Lookup::Test.set_default_stub(
  [
    {
      'latitude'     => 40.7143528,
      'longitude'    => -74.0059731,
      'address'      => 'New York, NY, USA',
      'city'         => 'New York',
      'state'        => 'New York',
      'state_code'   => 'NY',
      'postal_code'  => '10001',
      'country'      => 'United States',
      'country_code' => 'US'
    }
  ]
)
