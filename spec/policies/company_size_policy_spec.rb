require 'rails_helper'

describe CompanySizePolicy do

  let(:user) { User.new }

  subject { described_class }

  permissions ".scope" do
  end

  permissions :show? do
  end

  permissions :create? do
  end

  permissions :update? do
  end

  permissions :destroy? do
  end

  describe "user_has_allowed_role?" do
    it "should return true if the user has one of the allowed roles" do
      user.add_role :admin
      policy = CompanySizePolicy.new(user, :create)
      expect(policy.user_has_allowed_role?).to be true
    end
  end
end
