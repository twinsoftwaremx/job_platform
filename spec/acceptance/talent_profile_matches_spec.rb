require 'acceptance_helper'

resource "Talent Profile Matches" do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user) }

  before do
    TalentProfile.any_instance.stub(:get_matches)
    Position.any_instance.stub(:get_matches)
  end

  context 'for talent profile' do
    let(:talent_profile) { create(:talent_profile, user: user) }
    let(:positions) { create_list(:position, 5) }

    before do
      header "Authorization", encode_token(user.auth_token)
      talent_profile.matches << positions
      @talent_profile_match = talent_profile.talent_profile_matches.first
    end

    get "/talent_profiles/:talent_profile_id/matches" do
      let(:talent_profile_id) { talent_profile.id }
      let(:raw_post) { params.to_json }

      example_request "Getting a list of matches for a talent_profile_id" do
        expect(status).to eq(200)
        json = JSON.parse(response_body)
        expect(json['data'].length).to eq(5)
      end
    end

    post "/talent_profiles/:talent_profile_id/matches/:id/like" do
      let(:talent_profile_id) { talent_profile.id }
      let(:id) { @talent_profile_match.id }
      let(:raw_post) { params.to_json }

      example_request "Like a TalentProfileMatch" do
        expect(status).to eq(201)
        json = JSON.parse(response_body)
        expect(json['data']['attributes']['status']).to eq('liked')
      end
    end

    post "/talent_profiles/:talent_profile_id/matches/:id/reject" do
      let(:talent_profile_id) { talent_profile.id }
      let(:id) { @talent_profile_match.id }
      let(:raw_post) { params.to_json }

      example_request "Reject a TalentProfileMatch" do
        expect(status).to eq(201)
        json = JSON.parse(response_body)
        expect(json['data']['attributes']['status']).to eq('rejected')
      end
    end
  end

  context 'for employer contact' do
    let(:talent_profile) { create(:talent_profile) }
    let(:employer_contact) { create(:employer_contact, user: user) }
    let(:positions) { create_list(:position, 5, employer_contact: employer_contact) }

    before do
      header "Authorization", encode_token(user.auth_token)
      user.add_role :employer_contact
      talent_profile.matches << positions
      @talent_profile_match = talent_profile.talent_profile_matches.first


      matches_new = create_list(:talent_profile_match, 2, status: 'new_match', position: @talent_profile_match.position)
      matches_liked = create_list(:talent_profile_match, 3, status: 'liked', position: @talent_profile_match.position)
      matches_interested = create_list(:talent_profile_match, 2, status: 'interested', position: @talent_profile_match.position)
      matches_offer = create_list(:talent_profile_match, 1, status: 'offer', position: @talent_profile_match.position)
    end

    get "/positions/:position_id/matches" do
      parameter :position_id, "Position Id"

      let(:position_id) { @talent_profile_match.position.id }

      let(:raw_post) { params.to_json }

      example_request "Gets all the matches with status of liked, interested and offer" do
        expect(status).to eq(200)
        json = JSON.parse(response_body)
        expect(json['data'].count).to eq 6
      end
    end

    post "/positions/:position_id/matches/:match_id/interest" do
      parameter :position_id, "Position Id"
      parameter :match_id, "Match Id"

      let(:position_id) { @talent_profile_match.position.id }
      let(:match_id) { @talent_profile_match.id }

      let(:raw_post) { params.to_json }

      example_request "Employer contact marks a TalentProfileMatch as interested" do
        expect(status).to eq(201)
        json = JSON.parse(response_body)
        expect(json['data']['attributes']['status']).to eq('interested')
      end
    end
  end
end
