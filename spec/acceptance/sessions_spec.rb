require 'acceptance_helper'

resource "Sessions" do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user) }

  let(:user) { create(:user) }

  post "/sessions" do
    parameter :email, "Email", scope: :session
    parameter :password, "Password", scope: :session

    response_field :email, "Email"
    response_field :auth_token, "Auth Token"

    let(:email) { user.email }
    let(:password) { user.password }

    let(:raw_post) { params.to_json }

    example_request "Sign in" do
      user.reload
      hash = JSON.parse response_body
      auth_token = hash["data"]["attributes"]["auth_token"]
      expect(auth_token).to eq user.auth_token
      expect(status).to eq(200)
    end
  end

  delete "/sessions/:id" do
    before do
      header "Authorization", encode_token(user.auth_token)
    end
    parameter :id, "Auth Token"

    let(:id) { user.auth_token }

    example_request "Sign out" do
      expect(status).to eq(204)
    end
  end

  delete "/sessions/:id" do
    before do
      another_user = create(:user)
      header "Authorization", encode_token(another_user.auth_token)
    end
    parameter :id, "Auth Token"

    let(:id) { user.auth_token }

    example_request "Users get an error when signing out from another user's account" do
      expect(status).to eq(403)
    end
  end
end
