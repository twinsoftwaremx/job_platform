require 'acceptance_helper'

resource "Interests" do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user) }

  before do
    header "Authorization", encode_token(user.auth_token)
  end

  let(:interest) { create(:interest) }

  get "/interests" do
    before do
      user.add_role :talent
    end
    response_field :id, "Id"
    response_field :name, "Name"

    before do
      create_list(:interest, 3)
    end

    let(:raw_post) { params.to_json }

    example_request "User with talent role getting a list of interests" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data].count).to eq 0
    end
  end

  get "/interests" do
    before do
      user.add_role :admin
    end
    response_field :id, "Id"
    response_field :name, "Name"

    before do
      create_list(:interest, 3)
    end

    let(:raw_post) { params.to_json }

    example_request "User with admin role getting a list of interests" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data].count).to eq 3
    end
  end

  get "/interests" do
    before do
      user.add_role :account_admin
    end
    response_field :id, "Id"
    response_field :name, "Name"

    before do
      create_list(:interest, 3)
    end

    let(:raw_post) { params.to_json }

    example_request "User with account_admin role getting a list of interests" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data].count).to eq 3
    end
  end

  get "/interests" do
    before do
      user.add_role :employer_contact
    end
    response_field :id, "Id"
    response_field :name, "Name"

    before do
      create_list(:interest, 3)
    end

    let(:raw_post) { params.to_json }

    example_request "User with employer_contact role getting a list of interests" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data].count).to eq 3
    end
  end

  get "/interests/:id" do
    before do
      user.add_role :admin
    end
    parameter :id, "Id", scope: :interest

    response_field :id, "Id"
    response_field :name, "Name"

    let(:id) { interest.id }

    let(:raw_post) { params.to_json }

    example_request "User with admin role getting an interest by Id" do
      expect(status).to eq(200)
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
        InterestSerializer.new(interest)).to_json
    end
  end

  get "/interests/:id" do
    before do
      user.add_role :account_admin
    end
    parameter :id, "Id", scope: :interest

    response_field :id, "Id"
    response_field :name, "Name"

    let(:id) { interest.id }

    let(:raw_post) { params.to_json }

    example_request "User with account_admin role getting an interest by Id" do
      expect(status).to eq(200)
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
        InterestSerializer.new(interest)).to_json
    end
  end

  get "/interests/:id" do
    before do
      user.add_role :employer_contact
    end
    parameter :id, "Id", scope: :interest

    response_field :id, "Id"
    response_field :name, "Name"

    let(:id) { interest.id }

    let(:raw_post) { params.to_json }

    example_request "User with employer_contact role getting an interest by Id" do
      expect(status).to eq(200)
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
        InterestSerializer.new(interest)).to_json
    end
  end

  get "/interests/:id" do
    before do
      user.add_role :talent
    end
    parameter :id, "Id", scope: :interest

    response_field :id, "Id"
    response_field :name, "Name"

    let(:id) { interest.id }

    let(:raw_post) { params.to_json }

    example_request "User with talent role gets an error when getting an interest by Id" do
      expect(status).to eq(403)
    end
  end

  post "/interests" do
    before do
      user.add_role :admin
    end
    parameter :name, "Name", scope: :interest

    response_field :id, "Id"
    response_field :name, "Name"

    let(:name) { "Interest name" }

    let(:raw_post) { params.to_json }

    example_request "Creating an interest" do
      expect(status).to eq(201)
    end
  end

  post "/interests" do
    parameter :name, "Name", scope: :interest

    response_field :id, "Id"
    response_field :name, "Name"

    let(:name) { "Interest name" }

    let(:raw_post) { params.to_json }

    example_request "Non-admin user gets an error when creating an interest" do
      expect(status).to eq(403)
    end
  end

  put "/interests/:id" do
    before do
      user.add_role :admin
    end
    parameter :id, "Id"

    parameter :name, "Name", scope: :interest

    response_field :id, "Id"
    response_field :name, "Name"

    let(:id) { interest.id }
    let(:name) { "Updated name" }

    let(:raw_post) { params.to_json }

    example_request "Updating an interest by Id" do
      expect(status).to eq(204)
    end
  end

  put "/interests/:id" do
    parameter :id, "Id"

    parameter :name, "Name", scope: :interest

    response_field :id, "Id"
    response_field :name, "Name"

    let(:id) { interest.id }
    let(:name) { "Updated name" }

    let(:raw_post) { params.to_json }

    example_request "Non-admin user gets an error when updating an interest by Id" do
      expect(status).to eq(403)
    end
  end

  delete "/interests/:id" do
    before do
      user.add_role :admin
    end
    parameter :id, "Id"

    let(:id) { interest.id }

    example_request "Deleting an interest by Id" do
      expect(status).to eq(204)
    end
  end

  delete "/interests/:id" do
    parameter :id, "Id"

    let(:id) { interest.id }

    example_request "Non-admin user gets error when deleting an interest by Id" do
      expect(status).to eq(403)
    end
  end
end