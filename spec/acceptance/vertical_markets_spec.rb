require 'acceptance_helper'

resource 'Vertical Markets' do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user) }

  before do
    header "Authorization", encode_token(user.auth_token)
  end

  get '/vertical_markets' do
    response_field :id, 'Id'
    response_field :name, 'Name'

    before do
      header 'Authorization', ''
      create_list(:vertical_market, 4)
    end

    let(:raw_post) { params.to_json }

    example_request 'Getting a list of vertical markets by a non-authorized user' do
      expect(status).to eq 200
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data].count).to eq 4
    end
  end

  get '/vertical_markets/:id' do
    parameter :id, 'Id'

    response_field :id, 'Id'
    response_field :name, 'Name'

    let(:vertical_market) { create(:vertical_market) }

    let(:id) { vertical_market.id }

    let(:raw_post) { params.to_json }

    example_request 'Getting a vertical market by Id' do
      expect(status).to eq 200
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
          VerticalMarketSerializer.new(vertical_market)).to_json
    end
  end

  post '/vertical_markets' do
    before do
      user.add_role :admin
    end
    parameter :name, 'Name', scope: :vertical_market

    response_field :id, 'Id'
    response_field :name, 'Name'

    let(:name) { 'Vertical Market 1' }

    let(:raw_post) { params.to_json }

    example_request 'Creating a vertical market by admin user' do
      expect(status).to eq 201
    end

  end

  post '/vertical_markets' do
    parameter :name, 'Name', scope: :vertical_market

    response_field :id, 'Id'
    response_field :name, 'Name'

    let(:name) { 'Vertical Market 1' }

    let(:raw_post) { params.to_json }

    example_request 'Non-admin user gets an error when creating a vertical market' do
      expect(status).to eq 403
    end
  end

  put '/vertical_markets/:id' do
    before do
      user.add_role :admin
    end
    parameter :id, 'Id'

    parameter :name, 'Name', scope: :vertical_market

    response_field :id, 'Id'
    response_field :name, 'Name'

    let(:vertical_market) { create(:vertical_market) }
    let(:id) { vertical_market.id }

    let(:name) { 'Vertical Market 1' }

    let(:raw_post) { params.to_json }

    example_request 'Updating a vertical market by admin user' do
      expect(status).to eq 204
    end
  end

  put '/vertical_markets/:id' do
    parameter :id, 'Id'

    parameter :name, 'Name', scope: :vertical_market

    response_field :id, 'Id'
    response_field :name, 'Name'

    let(:vertical_market) { create(:vertical_market) }
    let(:id) { vertical_market.id }

    let(:name) { 'Vertical Market 1' }

    let(:raw_post) { params.to_json }

    example_request 'Non-admin user gets an error when updating a vertical market' do
      expect(status).to eq 403
    end
  end

  delete "/vertical_markets/:id" do
    before do
      user.add_role :admin
    end
    parameter :id, "Id"

    let(:vertical_market) { create(:vertical_market) }
    let(:id) { vertical_market.id }

    example_request "Deleting a vertical market by admin user" do
      expect(status).to eq 204
    end
  end

  delete "/vertical_markets/:id" do
    before do
      user.roles.clear
    end
    parameter :id, "Id"

    let(:vertical_market) { create(:vertical_market) }
    let(:id) { vertical_market.id }

    example_request "Non-admin user gets an error when deleting a vertical market" do
      expect(status).to eq 403
    end
  end
end