require 'acceptance_helper'

resource "Fee Structures" do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user) }

  before do
    header "Authorization", encode_token(user.auth_token)
  end

  get "/get_fees/:salary" do
    before do
      user.add_role :account_admin
    end

    response_field :standard_fee, "Standard Fee"
    response_field :monthly_subscription, "Monthly Subscription"
    response_field :upfront_fee, "Upfront Fetch Fee"

    parameter :salary, "Salary"

    let(:salary) { 55000 }

    let(:raw_post) { params.to_json }

    example_request "User can get the calculated fees passing a salary" do
      expect(status).to eq 200
      result = JSON.parse(response_body)
      expect(result['standard_fee']).to eq 8850
      expect(result['upfront_fee']).to eq 1000
      expect(result['monthly_subscription']).to eq 250
    end
  end

end