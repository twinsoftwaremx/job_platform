require 'acceptance_helper'

resource "PreferredCompanySizeAssociations" do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user) }

  before do
    header "Authorization", encode_token(user.auth_token)
  end

  get "/talent_profiles/:talent_profile_id/pref_company_sizes" do
    parameter :talent_profile_id, "Talent Profile Id"

    response_field :id, "Id"
    response_field :company_size, "Company Size"

    let(:talent_profile) { create(:talent_profile, :with_pref_company_sizes, number_of_pref_company_sizes: 3, user: user) }

    let(:talent_profile_id) { talent_profile.id }

    let(:raw_post) { params.to_json }

    example_request "Getting a list of preferred company sizes for the talent profile" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data].count).to eq 3
    end
  end

  get "/talent_profiles/:talent_profile_id/pref_company_sizes" do
    parameter :talent_profile_id, "Talent Profile Id"

    response_field :id, "Id"
    response_field :company_size, "Company Size"

    let(:talent_profile) { create(:talent_profile, :with_pref_company_sizes, number_of_pref_company_sizes: 3) }

    let(:talent_profile_id) { talent_profile.id }

    let(:raw_post) { params.to_json }

    example_request "User gets an error when getting a list of preferred company sizes from another user" do
      expect(status).to eq(403)
    end
  end

  get "/talent_profiles/:talent_profile_id/pref_company_sizes/:id" do
    parameter :talent_profile_id, "Talent Profile Id"
    parameter :id, "Id"

    response_field :id, "Id"
    response_field :company_size, "Company Size"

    let(:talent_profile) { create(:talent_profile, user: user) }
    let(:preferred_company_size_association) { create(:preferred_company_size_association, talent_profile: talent_profile) }

    let(:talent_profile_id) { talent_profile.id }
    let(:id) { preferred_company_size_association.id }

    let(:raw_post) { params.to_json }

    example_request "Getting a preferred company size by Id for the talent profile" do
      expect(status).to eq(200)
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
        PreferredCompanySizeAssociationSerializer.new(preferred_company_size_association)).to_json
    end
  end

  get "/talent_profiles/:talent_profile_id/pref_company_sizes/:id" do
    parameter :talent_profile_id, "Talent Profile Id"
    parameter :id, "Id"

    response_field :id, "Id"
    response_field :company_size, "Company Size"

    let(:talent_profile) { create(:talent_profile) }
    let(:preferred_company_size_association) { create(:preferred_company_size_association, talent_profile: talent_profile) }

    let(:talent_profile_id) { talent_profile.id }
    let(:id) { preferred_company_size_association.id }

    let(:raw_post) { params.to_json }

    example_request "User gets an error when getting a preferred company size by Id from another user" do
      expect(status).to eq(403)
    end
  end

  post "/talent_profiles/:talent_profile_id/pref_company_sizes" do
    parameter :talent_profile_id, "Talent Profile Id"

    parameter :id, "Id"
    parameter :company_size_id, "Company Size Id", scope: :preferred_company_size_association

    response_field :id, "Id"
    response_field :company_size, "Company Size"

    let(:talent_profile) { create(:talent_profile, user: user) }
    let(:talent_profile_id) { talent_profile.id }

    let(:company_size) { create(:company_size) }

    let(:company_size_id) { company_size.id }

    let(:raw_post) { params.to_json }

    example_request "Adding a preferred company size to the talent profile" do
      expect(status).to eq(201)
    end
  end

  post "/talent_profiles/:talent_profile_id/pref_company_sizes" do
    parameter :talent_profile_id, "Talent Profile Id"

    parameter :id, "Id"
    parameter :company_size_id, "Company Size Id", scope: :preferred_company_size_association

    response_field :id, "Id"
    response_field :company_size, "Company Size"

    let(:talent_profile) { create(:talent_profile) }
    let(:talent_profile_id) { talent_profile.id }

    let(:company_size) { create(:company_size) }

    let(:company_size_id) { company_size.id }

    let(:raw_post) { params.to_json }

    example_request "User gets an error when adding a preferred company size to another user" do
      expect(status).to eq(403)
    end
  end

  delete "/talent_profiles/:talent_profile_id/pref_company_sizes/:id" do
    parameter :id, "Id"
    parameter :talent_profile_id, "Id"

    let(:talent_profile) { create(:talent_profile, user: user) }
    let(:talent_profile_id) { talent_profile.id }

    let(:preferred_company_size_association) { create(:preferred_company_size_association, talent_profile: talent_profile) }
    let(:id) { preferred_company_size_association.id }

    example_request "Removing a preferred company size by Id from a talent profile" do
      expect(status).to eq(204)
    end
  end

  delete "/talent_profiles/:talent_profile_id/pref_company_sizes/:id" do
    parameter :id, "Id"
    parameter :talent_profile_id, "Id"

    let(:talent_profile) { create(:talent_profile) }
    let(:talent_profile_id) { talent_profile.id }

    let(:preferred_company_size_association) { create(:preferred_company_size_association, talent_profile: talent_profile) }
    let(:id) { preferred_company_size_association.id }

    example_request "User gets an error when removing a preferred company size by Id from another user" do
      expect(status).to eq(403)
    end
  end
end
