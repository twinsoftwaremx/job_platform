require 'acceptance_helper'

resource "Users" do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user) }

  post "/users" do
    parameter :email, "Email", scope: :user
    parameter :password, "Password", scope: :user
    parameter :password_confirmation, "Password Confirmation", scope: :user
    parameter :role, "User type", scope: :user

    response_field :email, "Email"
    response_field :token_auth, "Token Auth"
    response_field :role, "User role"

    let(:email) { Faker::Internet.email }
    let(:password) { 'password123?' }
    let(:password_confirmation) { 'password123?' }
    let(:role) { 'talent' }

    let(:raw_post) { params.to_json }

    example_request "Creating a user" do
      expect(status).to eq 201
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data][:attributes][:email]).to eq(params["user"]["email"])
      expect(json_response[:data][:attributes][:role]).to eq('talent')
    end
  end

  post "/users" do
    parameter :email, "Email", scope: :user
    parameter :password, "Password", scope: :user
    parameter :password_confirmation, "Password Confirmation", scope: :user
    parameter :role, "User type", scope: :user

    response_field :email, "Email"
    response_field :token_auth, "Token Auth"
    response_field :role, "User role"

    let(:email) { Faker::Internet.email }
    let(:password) { 'password123?' }
    let(:password_confirmation) { 'password123?' }
    let(:role) { 'admin' }

    let(:raw_post) { params.to_json }

    example_request "User role is set to talent if he tries to set to admin when creating a user" do
      expect(status).to eq 201
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data][:attributes][:email]).to eq(params["user"]["email"])
      expect(json_response[:data][:attributes][:role]).to eq('talent')
    end
  end

  get "/users/:id" do
    parameter :id, "Id"

    response_field :id, "Id"
    response_field :email, "Email"

    let(:id) { user.id }

    let(:raw_post) { params.to_json }

    example "Getting a user" do
      header "Authorization", encode_token(user.auth_token)
      do_request
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
        UserSerializer.new(user)).to_json
      expect(status).to eq(200)
    end
  end

  get "/users/:id" do
    parameter :id, "Id"

    response_field :id, "Id"
    response_field :email, "Email"

    let(:another_user) { create(:user) }

    let(:id) { user.id }

    let(:raw_post) { params.to_json }

    example "User gets an error when trying to see details from another user" do
      header "Authorization", encode_token(another_user.auth_token)
      do_request
      expect(status).to eq(403)
    end
  end
end
