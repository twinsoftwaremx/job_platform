require 'acceptance_helper'

resource "PreviousEmployments" do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user) }

  before do
    header "Authorization", encode_token(user.auth_token)
  end

  get "/talent_profiles/:talent_profile_id/previous_employments" do
    parameter :talent_profile_id, "Talent Profile Id"

    response_field :id, "Id"
    response_field :company, "Company"
    response_field :position, "Position"
    response_field :start_date, "Start Date"
    response_field :end_date, "End Date"
    response_field :description, "Description"

    let(:talent_profile) { create(:talent_profile, :with_previous_employments, number_of_previous_employments: 3, user: user) }

    let(:talent_profile_id) { talent_profile.id }

    let(:raw_post) { params.to_json }

    example_request "Getting a list of previous employments for the talent profile" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data].count).to eq 3
    end
  end

  get "/talent_profiles/:talent_profile_id/previous_employments" do
    parameter :talent_profile_id, "Talent Profile Id"

    response_field :id, "Id"
    response_field :company, "Company"
    response_field :position, "Position"
    response_field :start_date, "Start Date"
    response_field :end_date, "End Date"
    response_field :description, "Description"

    let(:talent_profile) { create(:talent_profile, :with_previous_employments, number_of_previous_employments: 3) }

    let(:talent_profile_id) { talent_profile.id }

    let(:raw_post) { params.to_json }

    example_request "Error when getting a list of previous employments from another talent profile" do
      expect(status).to eq(403)
    end
  end

  get "/talent_profiles/:talent_profile_id/previous_employments/:id" do
    parameter :talent_profile_id, "Talent Profile Id"
    parameter :id, "Id"

    response_field :id, "Id"
    response_field :company, "Company"
    response_field :position, "Position"
    response_field :start_date, "Start Date"
    response_field :end_date, "End Date"
    response_field :description, "Description"

    let(:talent_profile) { create(:talent_profile, user: user) }
    let(:previous_employment) { create(:previous_employment, talent_profile: talent_profile) }

    let(:talent_profile_id) { talent_profile.id }
    let(:id) { previous_employment.id }

    let(:raw_post) { params.to_json }

    example_request "Getting a previous employment by Id for the talent profile" do
      expect(status).to eq(200)
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
        PreviousEmploymentSerializer.new(previous_employment)).to_json
    end
  end

  get "/talent_profiles/:talent_profile_id/previous_employments/:id" do
    parameter :talent_profile_id, "Talent Profile Id"
    parameter :id, "Id"

    response_field :id, "Id"
    response_field :company, "Company"
    response_field :position, "Position"
    response_field :start_date, "Start Date"
    response_field :end_date, "End Date"
    response_field :description, "Description"

    let(:talent_profile) { create(:talent_profile) }
    let(:previous_employment) { create(:previous_employment, talent_profile: talent_profile) }

    let(:talent_profile_id) { talent_profile.id }
    let(:id) { previous_employment.id }

    let(:raw_post) { params.to_json }

    example_request "User gets error when getting a previous employment by Id from another talent profile" do
      expect(status).to eq(403)
    end
  end

  post "/talent_profiles/:talent_profile_id/previous_employments" do
    parameter :talent_profile_id, "Talent Profile Id"

    parameter :id, "Id", scope: :previous_employment
    parameter :company, "Company", scope: :previous_employment
    parameter :position, "Position", scope: :previous_employment
    parameter :start_date, "Start Date", scope: :previous_employment
    parameter :end_date, "End Date", scope: :previous_employment
    parameter :description, "Description", scope: :previous_employment

    response_field :company, "Company"
    response_field :position, "Position"
    response_field :start_date, "Start Date"
    response_field :end_date, "End Date"
    response_field :description, "Description"

    let(:company) { "Company name" }
    let(:position) { "Position name" }
    let(:start_date) { Time.new }
    let(:end_date) { Time.new }
    let(:description) { "Previous Employment Description" }

    let(:talent_profile) { create(:talent_profile, user: user) }
    let(:talent_profile_id) { talent_profile.id }

    let(:raw_post) { params.to_json }

    example_request "Creating a previous employment for the talent profile" do
      expect(status).to eq(201)
    end
  end

  post "/talent_profiles/:talent_profile_id/previous_employments" do
    parameter :talent_profile_id, "Talent Profile Id"

    parameter :id, "Id", scope: :previous_employment
    parameter :company, "Company", scope: :previous_employment
    parameter :position, "Position", scope: :previous_employment
    parameter :start_date, "Start Date", scope: :previous_employment
    parameter :end_date, "End Date", scope: :previous_employment
    parameter :description, "Description", scope: :previous_employment

    response_field :company, "Company"
    response_field :position, "Position"
    response_field :start_date, "Start Date"
    response_field :end_date, "End Date"
    response_field :description, "Description"

    let(:company) { "Company name" }
    let(:position) { "Position name" }
    let(:start_date) { Time.new }
    let(:end_date) { Time.new }
    let(:description) { "Previous Employment Description" }

    let(:talent_profile) { create(:talent_profile) }
    let(:talent_profile_id) { talent_profile.id }

    let(:raw_post) { params.to_json }

    example_request "User gets error when creating a previous employment for another talent profile" do
      expect(status).to eq(403)
    end
  end

  put "/talent_profiles/:talent_profile_id/previous_employments/:id" do
    parameter :talent_profile_id, "Talent Profile Id"

    parameter :id, "Id", scope: :previous_employment
    parameter :company, "Company", scope: :previous_employment
    parameter :position, "Position", scope: :previous_employment
    parameter :start_date, "Start Date", scope: :previous_employment
    parameter :end_date, "End Date", scope: :previous_employment
    parameter :description, "Description", scope: :previous_employment

    response_field :company, "Company"
    response_field :position, "Position"
    response_field :start_date, "Start Date"
    response_field :end_date, "End Date"
    response_field :description, "Description"

    let(:company) { "New company name" }
    let(:position) { "New position name" }
    let(:description) { "Updated Employment Description" }

    let(:talent_profile) { create(:talent_profile, user: user) }
    let(:talent_profile_id) { talent_profile.id }

    let(:previous_employment) { create(:previous_employment, talent_profile: talent_profile) }
    let(:id) { previous_employment.id }

    let(:raw_post) { params.to_json }

    example_request "Creating a previous_employment for the talent profile" do
      expect(status).to eq(204)
    end
  end

  put "/talent_profiles/:talent_profile_id/previous_employments/:id" do
    parameter :talent_profile_id, "Talent Profile Id"

    parameter :id, "Id", scope: :previous_employment
    parameter :company, "Company", scope: :previous_employment
    parameter :position, "Position", scope: :previous_employment
    parameter :start_date, "Start Date", scope: :previous_employment
    parameter :end_date, "End Date", scope: :previous_employment
    parameter :description, "Description", scope: :previous_employment

    response_field :company, "Company"
    response_field :position, "Position"
    response_field :start_date, "Start Date"
    response_field :end_date, "End Date"
    response_field :description, "Description"

    let(:company) { "New company name" }
    let(:position) { "New position name" }
    let(:description) { "Updated Employment Description" }

    let(:talent_profile) { create(:talent_profile) }
    let(:talent_profile_id) { talent_profile.id }

    let(:previous_employment) { create(:previous_employment, talent_profile: talent_profile) }
    let(:id) { previous_employment.id }

    let(:raw_post) { params.to_json }

    example_request "User gets error when creating a previous_employment for another talent profile" do
      expect(status).to eq(403)
    end
  end
  delete "/talent_profiles/:talent_profile_id/previous_employments/:id" do
    parameter :id, "Id"
    parameter :talent_profile_id, "Id"

    let(:talent_profile) { create(:talent_profile, user: user) }
    let(:talent_profile_id) { talent_profile.id }

    let(:previous_employment) { create(:previous_employment, talent_profile: talent_profile) }
    let(:id) { previous_employment.id }

    example_request "Deleting a previous employment by Id for talent profile" do
      expect(status).to eq(204)
    end
  end

  delete "/talent_profiles/:talent_profile_id/previous_employments/:id" do
    parameter :id, "Id"
    parameter :talent_profile_id, "Id"

    let(:talent_profile) { create(:talent_profile) }
    let(:talent_profile_id) { talent_profile.id }

    let(:previous_employment) { create(:previous_employment, talent_profile: talent_profile) }
    let(:id) { previous_employment.id }

    example_request "User gets error when deleting a previous employment from another user" do
      expect(status).to eq(403)
    end
  end

end
