require 'acceptance_helper'

resource "InterestedLocations" do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user) }

  before do
    header "Authorization", encode_token(user.auth_token)
  end

  let(:interested_location) { create(:interested_location) }

  get "/interested_locations" do
    before do
      user.add_role :admin
    end
    response_field :id, "Id"
    response_field :name, "Name"

    before do
      create_list(:interested_location, 3)
    end

    let(:raw_post) { params.to_json }

    example_request "Getting a list of interested locations by admin user" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data].count).to eq 3
    end
  end

  get "/interested_locations" do
    before do
      user.add_role :account_admin
    end
    response_field :id, "Id"
    response_field :name, "Name"

    before do
      create_list(:interested_location, 3)
    end

    let(:raw_post) { params.to_json }

    example_request "Getting a list of interested locations by account_admin user" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data].count).to eq 3
    end
  end

  get "/interested_locations" do
    before do
      user.add_role :employer_contact
    end
    response_field :id, "Id"
    response_field :name, "Name"

    before do
      create_list(:interested_location, 3)
    end

    let(:raw_post) { params.to_json }

    example_request "Getting a list of interested locations by employer_contact user" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data].count).to eq 3
    end
  end

  get "/interested_locations" do
    before do
      user.add_role :talent
    end
    response_field :id, "Id"
    response_field :name, "Name"

    before do
      create_list(:interested_location, 3)
    end

    let(:raw_post) { params.to_json }

    example_request "User with talent role gets an empty list when getting a list of interested locations" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data].count).to eq 0
    end
  end

  get "/interested_locations/:id" do
    before do
      user.add_role :admin
    end
    parameter :id, "Id", scope: :interested_location

    response_field :id, "Id"
    response_field :name, "Name"

    let(:id) { interested_location.id }

    let(:raw_post) { params.to_json }

    example_request "Getting an interested location by a user with admin role" do
      expect(status).to eq(200)
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
        InterestedLocationSerializer.new(interested_location)).to_json
    end
  end

  get "/interested_locations/:id" do
    before do
      user.add_role :account_admin
    end
    parameter :id, "Id", scope: :interested_location

    response_field :id, "Id"
    response_field :name, "Name"

    let(:id) { interested_location.id }

    let(:raw_post) { params.to_json }

    example_request "Getting an interested location by a user with account_admin role" do
      expect(status).to eq(200)
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
        InterestedLocationSerializer.new(interested_location)).to_json
    end
  end

  get "/interested_locations/:id" do
    before do
      user.add_role :employer_contact
    end
    parameter :id, "Id", scope: :interested_location

    response_field :id, "Id"
    response_field :name, "Name"

    let(:id) { interested_location.id }

    let(:raw_post) { params.to_json }

    example_request "Getting an interested location by a user with employer_contact role" do
      expect(status).to eq(200)
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
        InterestedLocationSerializer.new(interested_location)).to_json
    end
  end

  get "/interested_locations/:id" do
    before do
      user.add_role :talent
    end
    parameter :id, "Id", scope: :interested_location

    response_field :id, "Id"
    response_field :name, "Name"

    let(:id) { interested_location.id }

    let(:raw_post) { params.to_json }

    example_request "User with talent role gets an error when getting an interested location by Id" do
      expect(status).to eq(403)
    end
  end

  post "/interested_locations" do
    before do
      user.add_role :admin
    end
    parameter :name, "Name", scope: :interested_location

    response_field :id, "Id"
    response_field :name, "Name"

    let(:name) { "Interested Location name" }

    let(:raw_post) { params.to_json }

    example_request "Creating an interested location" do
      expect(status).to eq(201)
    end
  end

  post "/interested_locations" do
    parameter :name, "Name", scope: :interested_location

    response_field :id, "Id"
    response_field :name, "Name"

    let(:name) { "Interested Location name" }

    let(:raw_post) { params.to_json }

    example_request "Non-admin user gets an error when creating an interested location" do
      expect(status).to eq(403)
    end
  end

  put "/interested_locations/:id" do
    before do
      user.add_role :admin
    end
    parameter :id, "Id"

    parameter :name, "Name", scope: :interested_location

    response_field :id, "Id"
    response_field :name, "Name"

    let(:id) { interested_location.id }
    let(:name) { "Updated name" }

    let(:raw_post) { params.to_json }

    example_request "Updating an interested location by Id" do
      expect(status).to eq(204)
    end
  end

  put "/interested_locations/:id" do
    parameter :id, "Id"

    parameter :name, "Name", scope: :interested_location

    response_field :id, "Id"
    response_field :name, "Name"

    let(:id) { interested_location.id }
    let(:name) { "Updated name" }

    let(:raw_post) { params.to_json }

    example_request "Non-admin user gets error when updating an interested location by Id" do
      expect(status).to eq(403)
    end
  end

  delete "/interested_locations/:id" do
    before do
      user.add_role :admin
    end
    parameter :id, "Id"

    let(:id) { interested_location.id }

    example_request "Deleting an interested location by Id" do
      expect(status).to eq(204)
    end
  end

  delete "/interested_locations/:id" do
    parameter :id, "Id"

    let(:id) { interested_location.id }

    example_request "Non-admin user gets an error when deleting an interested location by Id" do
      expect(status).to eq(403)
    end
  end
end