require 'acceptance_helper'

resource "EducationItems" do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user) }

  before do
    header "Authorization", encode_token(user.auth_token)
  end

  get "/talent_profiles/:talent_profile_id/education_items" do
    parameter :talent_profile_id, "Talent Profile Id"

    response_field :id, "Id"
    response_field :college, "College"
    response_field :year, "Year"
    response_field :degree, "Degree"

    let(:talent_profile) { create(:talent_profile, :with_education_items, number_of_education_items: 3, user: user) }

    let(:talent_profile_id) { talent_profile.id }

    let(:raw_post) { params.to_json }

    example_request "Getting a list of education items for the talent profile" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data].count).to eq 3
    end
  end

  get "/talent_profiles/:talent_profile_id/education_items" do
    parameter :talent_profile_id, "Talent Profile Id"

    response_field :id, "Id"
    response_field :college, "College"
    response_field :year, "Year"
    response_field :degree, "Degree"

    let(:talent_profile) { create(:talent_profile, :with_education_items, number_of_education_items: 3) }

    let(:talent_profile_id) { talent_profile.id }

    let(:raw_post) { params.to_json }

    example_request "User gets an error when getting a list of education items for the talent profile from another user" do
      expect(status).to eq(403)
    end
  end

  get "/talent_profiles/:talent_profile_id/education_items/:id" do
    parameter :talent_profile_id, "Talent Profile Id"
    parameter :id, "Id"

    response_field :id, "Id"
    response_field :college, "College"
    response_field :year, "Year"
    response_field :degree, "Degree"

    let(:talent_profile) { create(:talent_profile, user: user) }
    let(:education_item) { create(:education_item, talent_profile: talent_profile) }

    let(:talent_profile_id) { talent_profile.id }
    let(:id) { education_item.id }

    let(:raw_post) { params.to_json }

    example_request "Getting an education item by Id for the talent profile" do
      expect(status).to eq(200)
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
        EducationItemSerializer.new(education_item)).to_json
    end
  end

  get "/talent_profiles/:talent_profile_id/education_items/:id" do
    parameter :talent_profile_id, "Talent Profile Id"
    parameter :id, "Id"

    response_field :id, "Id"
    response_field :college, "College"
    response_field :year, "Year"
    response_field :degree, "Degree"

    let(:talent_profile) { create(:talent_profile) }
    let(:education_item) { create(:education_item, talent_profile: talent_profile) }

    let(:talent_profile_id) { talent_profile.id }
    let(:id) { education_item.id }

    let(:raw_post) { params.to_json }

    example_request "User gets an error when getting an education item by Id from another user" do
      expect(status).to eq(403)
    end
  end

  post "/talent_profiles/:talent_profile_id/education_items" do
    parameter :talent_profile_id, "Talent Profile Id"

    parameter :id, "Id", scope: :education_item
    parameter :college, "College", scope: :education_item
    parameter :year, "Year", scope: :education_item
    parameter :degree, "Degree", scope: :education_item

    response_field :id, "Id"
    response_field :college, "College"
    response_field :year, "Year"
    response_field :degree, "Degree"

    let(:college) { "College name" }
    let(:year) { 2005 }
    let(:degree) { "Degree" }

    let(:talent_profile) { create(:talent_profile, user: user) }
    let(:talent_profile_id) { talent_profile.id }

    let(:raw_post) { params.to_json }

    example_request "Creating an education item for the talent profile" do
      expect(status).to eq(201)
    end
  end

  post "/talent_profiles/:talent_profile_id/education_items" do
    parameter :talent_profile_id, "Talent Profile Id"

    parameter :id, "Id", scope: :education_item
    parameter :college, "College", scope: :education_item
    parameter :year, "Year", scope: :education_item
    parameter :degree, "Degree", scope: :education_item

    response_field :id, "Id"
    response_field :college, "College"
    response_field :year, "Year"
    response_field :degree, "Degree"

    let(:college) { "College name" }
    let(:year) { 2005 }
    let(:degree) { "Degree" }

    let(:talent_profile) { create(:talent_profile) }
    let(:talent_profile_id) { talent_profile.id }

    let(:raw_post) { params.to_json }

    example_request "User gets an error when creating an education item for the talent profile from another user" do
      expect(status).to eq(403)
    end
  end

  put "/talent_profiles/:talent_profile_id/education_items/:id" do
    parameter :talent_profile_id, "Talent Profile Id"

    parameter :id, "Id", scope: :education_item
    parameter :college, "College", scope: :education_item
    parameter :year, "Year", scope: :education_item
    parameter :degree, "Degree", scope: :education_item

    response_field :id, "Id"
    response_field :college, "College"
    response_field :year, "Year"
    response_field :degree, "Degree"

    let(:college) { "New college name" }
    let(:year) { 2006 }
    let(:degree) { "Updated Degree" }

    let(:talent_profile) { create(:talent_profile, user: user) }
    let(:talent_profile_id) { talent_profile.id }

    let(:education_item) { create(:education_item, talent_profile: talent_profile) }
    let(:id) { education_item.id }

    let(:raw_post) { params.to_json }

    example_request "Updating an education_item for the talent profile by id" do
      expect(status).to eq(204)
    end
  end

  put "/talent_profiles/:talent_profile_id/education_items/:id" do
    parameter :talent_profile_id, "Talent Profile Id"

    parameter :id, "Id", scope: :education_item
    parameter :college, "College", scope: :education_item
    parameter :year, "Year", scope: :education_item
    parameter :degree, "Degree", scope: :education_item

    response_field :id, "Id"
    response_field :college, "College"
    response_field :year, "Year"
    response_field :degree, "Degree"

    let(:college) { "New college name" }
    let(:year) { 2006 }
    let(:degree) { "Updated Degree" }

    let(:talent_profile) { create(:talent_profile) }
    let(:talent_profile_id) { talent_profile.id }

    let(:education_item) { create(:education_item, talent_profile: talent_profile) }
    let(:id) { education_item.id }

    let(:raw_post) { params.to_json }

    example_request "User gets an error when updating an education_item from another user" do
      expect(status).to eq(403)
    end
  end

  delete "/talent_profiles/:talent_profile_id/education_items/:id" do
    parameter :id, "Id"
    parameter :talent_profile_id, "Id"

    let(:talent_profile) { create(:talent_profile, user: user) }
    let(:talent_profile_id) { talent_profile.id }

    let(:education_item) { create(:education_item, talent_profile: talent_profile) }
    let(:id) { education_item.id }

    example_request "Deleting an education item by Id for talent profile" do
      expect(status).to eq(204)
    end
  end

  delete "/talent_profiles/:talent_profile_id/education_items/:id" do
    parameter :id, "Id"
    parameter :talent_profile_id, "Id"

    let(:talent_profile) { create(:talent_profile) }
    let(:talent_profile_id) { talent_profile.id }

    let(:education_item) { create(:education_item, talent_profile: talent_profile) }
    let(:id) { education_item.id }

    example_request "User gets an error when deleting an education item by Id from another user" do
      expect(status).to eq(403)
    end
  end
end
