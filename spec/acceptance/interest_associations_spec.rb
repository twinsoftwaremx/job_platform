require 'acceptance_helper'

resource "InterestAssociations" do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user) }

  before do
    header "Authorization", encode_token(user.auth_token)
  end

  get "/talent_profiles/:talent_profile_id/interests" do
    parameter :talent_profile_id, "Talent Profile Id"

    response_field :id, "Id"
    response_field :interest, "Interest"

    let(:talent_profile) { create(:talent_profile, :with_interests, number_of_interests: 3, user: user) }

    let(:talent_profile_id) { talent_profile.id }

    let(:raw_post) { params.to_json }

    example_request "Getting a list of interests for the talent profile" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data].count).to eq 3
    end
  end

  get "/talent_profiles/:talent_profile_id/interests" do
    parameter :talent_profile_id, "Talent Profile Id"

    response_field :id, "Id"
    response_field :interest, "Interest"

    let(:talent_profile) { create(:talent_profile, :with_interests, number_of_interests: 3) }

    let(:talent_profile_id) { talent_profile.id }

    let(:raw_post) { params.to_json }

    example_request "User gets error when getting a list of interests from another user" do
      expect(status).to eq(403)
    end
  end

  get "/talent_profiles/:talent_profile_id/interests/:id" do
    parameter :talent_profile_id, "Talent Profile Id"
    parameter :id, "Id"

    response_field :id, "Id"
    response_field :interest, "Interest"

    let(:talent_profile) { create(:talent_profile, user: user) }
    let(:interest_association) { create(:interest_association, talent_profile: talent_profile) }

    let(:talent_profile_id) { talent_profile.id }
    let(:id) { interest_association.id }

    let(:raw_post) { params.to_json }

    example_request "Getting an interest by Id for the talent profile" do
      expect(status).to eq(200)
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
        InterestAssociationSerializer.new(interest_association)).to_json
    end
  end

  get "/talent_profiles/:talent_profile_id/interests/:id" do
    parameter :talent_profile_id, "Talent Profile Id"
    parameter :id, "Id"

    response_field :id, "Id"
    response_field :interest, "Interest"

    let(:talent_profile) { create(:talent_profile) }
    let(:interest_association) { create(:interest_association, talent_profile: talent_profile) }

    let(:talent_profile_id) { talent_profile.id }
    let(:id) { interest_association.id }

    let(:raw_post) { params.to_json }

    example_request "User gets error when getting an interest by Id from another user" do
      expect(status).to eq(403)
    end
  end

  post "/talent_profiles/:talent_profile_id/interests" do
    parameter :talent_profile_id, "Talent Profile Id"

    parameter :interest_id, "Interest Id", scope: :interest_association

    response_field :id, "Id"
    response_field :interest, "Interest"

    let(:talent_profile) { create(:talent_profile, user: user) }
    let(:talent_profile_id) { talent_profile.id }

    let(:interest) { create(:interest) }

    let(:interest_id) { interest.id }

    let(:raw_post) { params.to_json }

    example_request "Adding an interest to the talent profile" do
      expect(status).to eq(201)
    end
  end

  post "/talent_profiles/:talent_profile_id/interests" do
    parameter :talent_profile_id, "Talent Profile Id"

    parameter :id, "Id"
    parameter :interest_id, "Interest Id", scope: :interest_association

    response_field :id, "Id"
    response_field :interest, "Interest"

    let(:talent_profile) { create(:talent_profile) }
    let(:talent_profile_id) { talent_profile.id }

    let(:interest) { create(:interest) }

    let(:interest_id) { interest.id }

    let(:raw_post) { params.to_json }

    example_request "User gets an error when adding an interest to another user" do
      expect(status).to eq(403)
    end
  end

  delete "/talent_profiles/:talent_profile_id/interests/:id" do
    parameter :id, "Id"
    parameter :talent_profile_id, "Id"

    let(:talent_profile) { create(:talent_profile, user: user) }
    let(:talent_profile_id) { talent_profile.id }

    let(:interest_association) { create(:interest_association, talent_profile: talent_profile) }
    let(:id) { interest_association.id }

    example_request "Removing an interest by Id from a talent profile" do
      expect(status).to eq(204)
    end
  end

  delete "/talent_profiles/:talent_profile_id/interests/:id" do
    parameter :id, "Id"
    parameter :talent_profile_id, "Id"

    let(:talent_profile) { create(:talent_profile) }
    let(:talent_profile_id) { talent_profile.id }

    let(:interest_association) { create(:interest_association, talent_profile: talent_profile) }
    let(:id) { interest_association.id }

    example_request "User gets an error when removing an interest by Id from another user" do
      expect(status).to eq(403)
    end
  end
end
