require 'acceptance_helper'

resource "Employment Types" do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user) }

  before do
    header "Authorization", encode_token(user.auth_token)
  end

  get "/employment_types" do
    response_field :id, "Id"
    response_field :name, "Name"

    before do
      header "Authorization", ""
      create_list(:employment_type, 3)
    end

    let(:raw_post) { params.to_json }

    example_request "Getting a list of employment types by a non-authorized user" do
      expect(status).to eq 200
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data].count).to eq 3
    end
  end

  get "/employment_types/:id" do
    parameter :id, "Id"

    response_field :id, "Id"
    response_field :name, "Name"

    let(:employment_type) { create(:employment_type) }

    let(:id) { employment_type.id }

    let(:raw_post) { params.to_json }

    example_request "Getting an employment type by Id" do
      expect(status).to eq 200
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
        EmploymentTypeSerializer.new(employment_type)).to_json
    end
  end

  post "/employment_types" do
    before do
      user.add_role :admin
    end
    parameter :name, "Name", scope: :employment_type

    response_field :id, "Id"
    response_field :name, "Name"

    let(:name) { "New name" }

    let(:raw_post) { params.to_json }

    example_request "Creating an employment_type by admin user" do
      expect(status).to eq 201
    end
  end

  put "/employment_types/:id" do
    before do
      user.add_role :admin
    end
    parameter :id, "Id"
    parameter :name, "Name", scope: :employment_type

    response_field :id, "Id"
    response_field :name, "Name"

    let(:employment_type) { create(:employment_type) }

    let(:id) { employment_type.id }
    let(:name) { "Updated name" }

    let(:raw_post) { params.to_json }

    example_request "Updating an employment_type by admin user" do
      expect(status).to eq 204
    end
  end

  delete "/employment_types/:id" do
    before do
      user.add_role :admin
    end
    parameter :id, "Id"

    let(:employment_type) { create(:employment_type) }
    let(:id) { employment_type.id }

    example_request "Deleting an employment_type by admin user" do
      expect(status).to eq 204
    end
  end
end
