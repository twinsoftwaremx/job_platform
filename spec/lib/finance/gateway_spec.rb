require 'rails_helper'


module Finance
  module Plugins
    module FakePlugin
      module ClassMethods
        def foo
          :bar
        end
      end

      module InstanceMethods
        def bar
          :foo
        end
      end
    end
  end
end


RSpec.describe Finance::Gateway do
  let(:sut) { Finance::Gateway }

  describe '#use' do
    let(:plugin) { Finance::Plugins::FakePlugin }

    it 'extends the gateway with the plugins functionality' do
      sut.use plugin

      expect(sut.foo).to eql(:bar)
      expect(sut.new.bar).to eql(:foo)
    end

  end
end
