require 'rails_helper'

RSpec.describe Finance::Plugins::ConfigurationPlugin do
  let(:sut) { Finance::Gateway }

  describe '#set' do
    it 'adds a key/value pair to the gateway configuration' do
      sut.set :foo, :bar
      expect(sut.config).to include foo: :bar
    end
  end
end
