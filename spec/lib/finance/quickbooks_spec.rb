require 'rails_helper'

RSpec.describe Finance::QuickbooksGateway do

  class Finance::Gateway
    set :company_id,   1
    set :access_token, 1234567890
  end

  let(:service)    { double(:service) }
  let(:quickbooks) { double('quickbooks') }
  let(:sut)        { Finance::QuickbooksGateway.new Finance::Gateway, mapper: Finance::QuickbooksInvoiceMapper, service: service }

  context 'creating invoices' do
    before(:each) do
      allow(service).to receive(:new).and_return(quickbooks)
      allow(quickbooks).to receive(:company_id=)
      allow(quickbooks).to receive(:access_token=)
      allow(quickbooks).to receive(:create)

      sut.create_invoice create(:invoice)
    end

    it 'properly creates a quickbooks invoice service' do
      expect(service).to have_received(:new)
      expect(quickbooks).to have_received(:company_id=).with(1)
      expect(quickbooks).to have_received(:access_token=).with(1234567890)
      expect(quickbooks).to have_received(:create).with(Quickbooks::Model::Invoice)
    end
  end
end
