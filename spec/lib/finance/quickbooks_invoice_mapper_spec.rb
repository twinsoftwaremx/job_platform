require 'rails_helper'

RSpec.describe Finance::QuickbooksInvoiceMapper do

  let(:sut) { Finance::QuickbooksInvoiceMapper }

  before(:each) do
    @fetch_invoice      = create(:invoice)
    @quickbooks_invoice = sut.from @fetch_invoice
  end

  it 'maps an Invoice to a Quickbooks::Model::Invoice' do
    expect(@quickbooks_invoice).to be_a Quickbooks::Model::Invoice
  end

  it 'maps the correct values' do
    expect(@quickbooks_invoice.txn_date).to eql @fetch_invoice.due_at.to_date
    expect(@quickbooks_invoice.doc_number).to eql @fetch_invoice.id
    expect(@quickbooks_invoice.line_items.count).to eql 1
    expect(@quickbooks_invoice.line_items.sum(&:amount)).to eql @fetch_invoice.amount
    expect(@quickbooks_invoice.line_items.first.description).to eql @fetch_invoice.name
  end

end
