require 'rails_helper'

describe 'Round Robin Agent Assignment' do
  before(:all) do
    @agent_007 = Agent.create! name: 'James Bond'
    @agent_006 = Agent.create! name: 'Alec Trevelyan'
  end

  it 'should assign the next agent in line' do
    agent  = Agents::NextAgentCommand.new.perform
    talent = agent.talent_profiles.create(
      name: 'Natalya Simonova',
      summary: 'Level 2 programmer at the Severnaya facility of the Russian Space Forces.',
      time_zone: 'MSK'
    )

    expect(talent.agent).to eql(@agent_007)

    agent  = Agents::NextAgentCommand.new.perform
    talent = agent.talent_profiles.create(
      name: 'Miranda Frost',
      summary: 'Undercover MI6 agent.',
      time_zone: 'GMT'
    )

    expect(talent.agent).to eql(@agent_006)

    agent  = Agents::NextAgentCommand.new.perform
    talent = agent.talent_profiles.create(
      name: 'Valenka',
      time_zone: 'MSK'
    )

    expect(talent.agent).to eql(@agent_007)
  end
end
