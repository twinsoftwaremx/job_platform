require 'rails_helper'

RSpec.describe Invoices::CancelBillingPlan do

  context 'when canceling a billing plan for a direct hire offer' do

    before do
      @offer   = create(:direct_hire_offer)
      schedule = Invoices::ScheduleBillingPlan.new(offer_id: @offer.id)
      sut      = Invoices::CancelBillingPlan.new(offer_id: @offer.id)

      schedule.perform
      sut.perform
    end

    it 'cancels all existing invoices' do
      expect(@offer.invoices.count).to eql(0)
    end
  end

  context 'when canceling a billing plan for a contract offer' do

    before do
      @offer   = create(:contract_offer)
      schedule = Invoices::ScheduleBillingPlan.new(offer_id: @offer.id)
      sut      = Invoices::CancelBillingPlan.new(offer_id: @offer.id)

      schedule.perform
      sut.perform
    end

    it 'cancels all existing invoices' do
      expect(@offer.invoices.count).to eql(0)
    end
  end
end
