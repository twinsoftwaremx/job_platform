require 'rails_helper'

RSpec.describe Invoices::ScheduleBillingPlan do

  subject { Invoices::ScheduleBillingPlan }

  context 'when a direct hire offer is accepted' do

    before do
      @offer = create(:direct_hire_offer)
      subject.new(offer_id: @offer.id).perform
    end

    it 'schedules invoices with the correct status' do
      expect(@offer.invoices.where(status:   Invoice.statuses[:due]).count).to eql(37)
      expect(@offer.invoices.where(category: Invoice.categories[:upfront]).count).to   eql(1)
      expect(@offer.invoices.where(category: Invoice.categories[:recurring]).count).to eql(36)
    end

    it 'schedules invoices with the correct due dates' do
      start_date      = Time.parse(@offer.start_date)
      upfront         = @offer.invoices.where(category: Invoice.categories[:upfront]).first
      first_recurring = @offer.invoices.where(category: Invoice.categories[:recurring]).first
      last_recurring  = @offer.invoices.where(category: Invoice.categories[:recurring]).last

      expect(upfront.due_at).to eql(start_date)
      expect(first_recurring.due_at).to eql(start_date + 1.month)
      expect(last_recurring.due_at).to eql(start_date + 36.months)
    end
  end

  context 'when a contract offer is accepted' do

    before do
      @offer = create(:contract_offer)
      subject.new(offer_id: @offer.id).perform
    end

    it 'schedules invoices with the correct status' do
      expect(@offer.invoices.where(status:   Invoice.statuses[:due]).count).to   eql(1)
      expect(@offer.invoices.where(category: Invoice.categories[:upfront]).count).to eql(1)
    end

    it 'schedules no recurring invoices' do
      expect(@offer.invoices.where(category: Invoice.categories[:recurring]).count).to eql(0)
    end
  end
end
