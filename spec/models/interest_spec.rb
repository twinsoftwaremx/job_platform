# == Schema Information
#
# Table name: interests
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Interest, type: :model do
  let(:interest) {build(:interest)}

  it "factory should be valid" do
    expect(interest).to be_valid
  end
end
