require 'rails_helper'

RSpec.describe TalentFinder, type: :model do
  before do
    TalentProfile.any_instance.stub(:get_matches)
    Position.any_instance.stub(:get_matches)
  end

  describe '.for_position', elasticsearch: true do
    context "when cultures match" do
      before do
        TalentProfile.__elasticsearch__.create_index! index: TalentProfile.index_name
        photo = create(:photo)
        account = create(:account, logo: photo)
        account.cultures = [create(:culture, name:"PROFESSIONAL")]
        employer_contact = create(:employer_contact, account: account)
        @position = FactoryGirl.create(:position, :with_bonus_skill_list, :with_required_skill_list, number_of_skills: 3, employer_contact: employer_contact)
        required_skills = @position.required_skills
        bonus_skills = @position.bonus_skills
        user1 = User.create! email: "talent1@test.com", password: "password", password_confirmation: "password"
        user2 = User.create! email: "talent2@test.com", password: "password", password_confirmation: "password"
        user3 = User.create! email: "talent3@test.com", password: "password", password_confirmation: "password"
        @talent_profile1 = FactoryGirl.create(:talent_profile, :with_skill_list, salary_range: @position.salary_range)
        @talent_profile2 = FactoryGirl.create(:talent_profile, :with_skill_list, :with_city_list)
        @talent_profile3 = FactoryGirl.create(:talent_profile, :with_skill_list, :with_city_list)
        @talent_profile1.cultures = [account.cultures.first]
        @talent_profile2.cultures = [account.cultures.first]
        @talent_profile3.cultures = [account.cultures.first]
        user1.talent_profile = @talent_profile1
        user1.save
        user2.talent_profile = @talent_profile2
        user2.save
        user3.talent_profile = @talent_profile3
        user3.save
        @talent_profile1.cities_of_interest << @position.city
        @talent_profile1.skills << required_skills
        TalentProfile.import
        sleep 1
      end

      after do
        TalentProfile.__elasticsearch__.client.indices.delete index: TalentProfile.index_name
      end

      it 'should return correct results' do
        results = TalentFinder.for_position(@position)
        expect(results.count).to eq(1)
        first_record = results[0]
        expect(first_record[:id]).to eq(@talent_profile1.id)

        expect{
          TalentFinder.call(@position)
        }.to change{TalentProfileMatch.count}.by(1)
      end

      it 'should return [] on no matching positions' do
        @talent_profile1.destroy
        TalentProfile.__elasticsearch__.client.indices.delete index: TalentProfile.index_name
        TalentProfile.__elasticsearch__.create_index! index: TalentProfile.index_name
        TalentProfile.import
        results = TalentFinder.for_position(@position)
        expect(results.count).to eq(0)
      end
    end

    context "when cultures don't match" do
      before do
        TalentProfile.__elasticsearch__.create_index! index: TalentProfile.index_name
        photo = create(:photo)
        account = create(:account, logo: photo)
        account.cultures = [create(:culture, name:"PROFESSIONAL")]
        employer_contact = create(:employer_contact, account: account)
        @position = FactoryGirl.create(:position, :with_bonus_skill_list, :with_required_skill_list, number_of_skills: 3, employer_contact: employer_contact)
        required_skills = @position.required_skills
        bonus_skills = @position.bonus_skills
        user1 = User.create! email: "talent1@test.com", password: "password", password_confirmation: "password"
        user2 = User.create! email: "talent2@test.com", password: "password", password_confirmation: "password"
        user3 = User.create! email: "talent3@test.com", password: "password", password_confirmation: "password"
        @talent_profile1 = FactoryGirl.create(:talent_profile, :with_skill_list, salary_range: @position.salary_range)
        @talent_profile2 = FactoryGirl.create(:talent_profile, :with_skill_list, :with_city_list)
        @talent_profile3 = FactoryGirl.create(:talent_profile, :with_skill_list, :with_city_list)
        @talent_profile1.cultures = [create(:culture, name:"CULTURE1")]
        @talent_profile2.cultures = [create(:culture, name:"CULTURE2")]
        @talent_profile3.cultures = [create(:culture, name:"CULTURE3")]
        user1.talent_profile = @talent_profile1
        user1.save
        user2.talent_profile = @talent_profile2
        user2.save
        user3.talent_profile = @talent_profile3
        user3.save
        @talent_profile1.cities_of_interest << @position.city
        @talent_profile1.skills << required_skills
        TalentProfile.import
        sleep 1
      end

      after do
        TalentProfile.__elasticsearch__.client.indices.delete index: TalentProfile.index_name
      end

      it "should not return any match" do
        expect {
          TalentFinder.call(@position)
        }.to change{TalentProfileMatch.count}.by(0)
      end
    end

    context "when there are already some existing matches" do
      before do
        TalentProfile.__elasticsearch__.create_index! index: TalentProfile.index_name
        photo = create(:photo)
        account = create(:account, logo: photo)
        account.cultures = [create(:culture, name:"PROFESSIONAL")]
        employer_contact = create(:employer_contact, account: account)
        @position = FactoryGirl.create(:position, :with_bonus_skill_list, :with_required_skill_list, number_of_skills: 3, employer_contact: employer_contact)
        required_skills = @position.required_skills
        bonus_skills = @position.bonus_skills
        user1 = User.create! email: "talent1@test.com", password: "password", password_confirmation: "password"
        user2 = User.create! email: "talent2@test.com", password: "password", password_confirmation: "password"
        user3 = User.create! email: "talent3@test.com", password: "password", password_confirmation: "password"
        @talent_profile1 = FactoryGirl.create(:talent_profile, :with_skill_list, salary_range: @position.salary_range)
        @talent_profile2 = FactoryGirl.create(:talent_profile, :with_skill_list, :with_city_list)
        @talent_profile3 = FactoryGirl.create(:talent_profile, :with_skill_list, :with_city_list)
        @talent_profile1.cultures = [create(:culture, name:"PROFESSIONAL")]
        @talent_profile2.cultures = [create(:culture, name:"PROFESSIONAL")]
        @talent_profile3.cultures = [create(:culture, name:"PROFESSIONAL")]
        user1.talent_profile = @talent_profile1
        user1.save
        user2.talent_profile = @talent_profile2
        user2.save
        user3.talent_profile = @talent_profile3
        user3.save
        @talent_profile1.cities_of_interest << @position.city
        @talent_profile1.skills << required_skills
        TalentProfile.import
        sleep 1
      end

      after do
        TalentProfile.__elasticsearch__.client.indices.delete index: TalentProfile.index_name
      end

      it "should not create a match if there are one match for the position/talent" do
        create(:talent_profile_match, position:@position, talent_profile:@talent_profile1)

        expect(TalentProfileMatch.count).to eq 1
        expect {
          TalentFinder.call(@position)
        }.to change{TalentProfileMatch.count}.by(0)
        expect(TalentProfileMatch.count).to eq 1
      end
    end
  end
end
