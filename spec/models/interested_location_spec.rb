# == Schema Information
#
# Table name: interested_locations
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe InterestedLocation, type: :model do
  describe "factory" do
    let(:interested_location) {build(:interested_location)}

    it "should have a valid factory" do
      expect(interested_location).to be_valid
    end
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:name)}
  end

  describe "associations" do
    it { is_expected.to have_many(:talent_profiles).through(:location_of_interest_association)}
  end
end
