require 'rails_helper'

RSpec.describe UpfrontFee do
  let(:sut) { UpfrontFee }

  describe 'calculate' do
    context 'when payrate is less than $76,000' do
      it 'should calculate the correct fee' do
        expect(sut.calculate '$76,000').to eql(1000)
      end
    end

    context 'when payrate is more than $76,000' do
      it 'should calculate the correct fee' do
        expect(sut.calculate '$76,001').to eql(2000)
      end
    end
  end
end
