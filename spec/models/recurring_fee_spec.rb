require 'rails_helper'

RSpec.describe RecurringFee do
  let(:sut) { RecurringFee }

  describe 'calculate' do
    it 'should calculate the correct amount' do
      expect(sut.calculate '$75,000').to eql(320)
    end
  end
end
