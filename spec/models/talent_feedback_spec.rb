# == Schema Information
#
# Table name: talent_feedbacks
#
#  id                :integer          not null, primary key
#  text              :text
#  talent_profile_id :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_talent_feedbacks_on_talent_profile_id  (talent_profile_id)
#

require 'rails_helper'

RSpec.describe TalentFeedback, type: :model do
  let(:talent_feedback) { build(:talent_feedback) }

  describe "associations" do
    it { is_expected.to belong_to(:talent_profile) }
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:text) }
  end
end
