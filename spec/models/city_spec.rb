# == Schema Information
#
# Table name: cities
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  state_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  deleted_at :datetime
#
# Indexes
#
#  index_cities_on_deleted_at  (deleted_at)
#  index_cities_on_state_id    (state_id)
#

require 'rails_helper'

RSpec.describe City, type: :model do
  describe "factory" do
    let(:city) { build(:city) }

    it "should be valid" do
      expect(city).to be_valid
    end
  end

  describe "full_name" do
    let(:state) { create(:state, abbreviation: 'AL') }
    let(:city) { create(:city, name: 'Birmingham', state: state) }

    it "should concatenate city name and state abbreviation" do
      expect(city.full_name).to eq "#{city.name}, #{state.abbreviation}"
    end
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:name) }
  end

  describe "associations" do
    it { is_expected.to belong_to(:state) }
  end
end
