# == Schema Information
#
# Table name: linkedin_assets
#
#  id              :integer          not null, primary key
#  recommendations :json
#  skills          :json
#  positions       :json
#  user_id         :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  profile         :json
#  deleted_at      :datetime
#
# Indexes
#
#  index_linkedin_assets_on_deleted_at  (deleted_at)
#  index_linkedin_assets_on_user_id     (user_id)
#

require 'rails_helper'

RSpec.describe LinkedinAsset, type: :model do
  let(:user) { FactoryGirl.create(:user) }
  let(:user_id) { user.id }

  it 'should relate to a user' do
    g_asset = LinkedinAsset.new(user_id: user_id)
    expect(g_asset.user).to eq(user)
  end
end
