# == Schema Information
#
# Table name: messages
#
#  id             :integer          not null, primary key
#  body           :text             not null
#  to_user_id     :integer          not null
#  from_user_id   :integer          not null
#  position_id    :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  from_user_type :string           default("User")
#  to_user_type   :string           default("User")
#  deleted_at     :datetime
#
# Indexes
#
#  index_messages_on_deleted_at    (deleted_at)
#  index_messages_on_from_user_id  (from_user_id)
#  index_messages_on_position_id   (position_id)
#  index_messages_on_to_user_id    (to_user_id)
#

require 'rails_helper'

RSpec.describe Message, type: :model do
  before do
    TalentProfile.any_instance.stub(:get_matches)
    Position.any_instance.stub(:get_matches)
  end

  describe "factory" do
    let(:message) { build(:message) }

    it "should be valid" do
      expect(message).to be_valid
    end
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:body) }
    it { is_expected.to validate_presence_of(:to_user) }
    it { is_expected.to validate_presence_of(:from_user) }
  end

  describe "associations" do
    it { is_expected.to belong_to(:to_user) }
    it { is_expected.to belong_to(:from_user) }
    it { is_expected.to belong_to(:position) }
  end
end
