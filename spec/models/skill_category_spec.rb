# == Schema Information
#
# Table name: skill_categories
#
#  id                 :integer          not null, primary key
#  name               :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  deleted_at         :datetime
#  vertical_market_id :integer
#
# Indexes
#
#  index_skill_categories_on_deleted_at          (deleted_at)
#  index_skill_categories_on_vertical_market_id  (vertical_market_id)
#

require 'rails_helper'

RSpec.describe SkillCategory, type: :model do
  let(:skill_category) {build(:skill_category)}

  it "factory should be valid" do
    expect(skill_category).to be_valid
  end

  describe "associations" do
    it { is_expected.to have_many(:skills) }
    it { is_expected.to belong_to(:vertical_market) }
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:name) }
  end
end
