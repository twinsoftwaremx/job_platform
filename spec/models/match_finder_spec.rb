require 'rails_helper'

RSpec.describe MatchFinder, type: :model do
  before do
    TalentProfile.any_instance.stub(:get_matches)
    Position.any_instance.stub(:get_matches)
  end

  describe '.for_talent', elasticsearch: true do
    context "when there are no existing matches" do
      before do
        Position.__elasticsearch__.create_index! index: Position.index_name
        @profile = create_talent_profile
        @positions = create_positions
        Position.import
        sleep 1
      end

      after do
        Position.__elasticsearch__.client.indices.delete index: Position.index_name
      end

      it "should return matches" do
        matches = MatchFinder.for_talent(@profile)
        expect{
          MatchFinder.call(@profile)
        }.to change{TalentProfileMatch.count}.by(2)
      end

      it "should not fail if salary_range is missing" do
        @profile.salary_range = nil
        @profile.save!
        expect{
          MatchFinder.for_talent(@profile)
        }.to_not raise_error(NoMethodError)
      end

      it "should not fail if .for_talent returns [] and there are exisiting matches" do
        matches = MatchFinder.for_talent(@profile)
        expect(matches.count).to eq(@positions.count)
        expect{
          MatchFinder.call(@profile)
        }.to change{TalentProfileMatch.count}.by(2)
        # call again to check
        expect{
          MatchFinder.call(@profile)
        }.to_not raise_error(NoMethodError)
      end

      it "should not fail if one talent_profile_match is missing some fields" do
        matches = MatchFinder.for_talent(@profile)
        create(:talent_profile_match, talent_profile:@profile)

        expect{
          MatchFinder.call(@profile)
        }.to change{TalentProfileMatch.count}.by(3)
      end

      it "should not match if the cultures are different" do
        @profile.cultures = [create(:culture, name:'REMOTE')]
        matches = MatchFinder.for_talent(@profile)

        expect {
          MatchFinder.call(@profile)
        }.to change{TalentProfileMatch.count}.by(0)
      end
    end
  end

  context "when there are already some existing matches" do
    before do
      Position.__elasticsearch__.create_index! index: Position.index_name
      @profile = create_talent_profile
      @positions = create_positions
      Position.import
      sleep 1
    end

    after do
      Position.__elasticsearch__.client.indices.delete index: Position.index_name
    end

    it "should not create a match if there is already a match for the position/talent" do
      create(:talent_profile_match, position:@positions.first, talent_profile:@profile)

      expect(TalentProfileMatch.count).to eq 1
      expect {
        MatchFinder.call(@profile)
      }.to change{TalentProfileMatch.count}.by(1)
    expect(TalentProfileMatch.count).to eq 2
    end
  end

  def create_talent_profile
    user1 = User.create! email: "talent1@test.com", password: "password", password_confirmation: "password"
    profile = TalentProfile.create! name: "Chase Allen Hutson", summary: "Chief Development Officer of Janitorial Services",
      time_zone: "EST", salary_range: SalaryRange.first_or_create!(name: "$0 - $20k"), agent: FactoryGirl.create(:agent)
    user1.talent_profile = profile
    profile.skill_list = "C++,Java,SAS"
    profile.culture_list = "START UP,PROFESSIONAL"
    l1 = Location.create! city: "Birmingham", state: "AL", zip: "35201"
    l2 = Location.create! city: "Huntsville", state: "AL", zip: "35801"
    profile.locations_of_interest << [l1, l2]
    alabama = State.create(name: "Alabama", abbreviation: "AL")
    c1 = City.create(name: "Birmingham", state: alabama)
    c2 = City.create(name: "Huntsville", state: alabama)
    profile.cities_of_interest << [c1, c2]
    profile.save
    profile
  end

  def create_positions
    city = City.find_by(name: "Birmingham")
    location = Location.first
    logo = create(:photo)
    user = User.create! email: "employer_shell@test.com", password: "password", password_confirmation: "password", role: "account_admin"
    account = Account.create! company_name: "Shell", logo: logo
    account.culture_list = "START UP,PROFESSIONAL"
    ec = EmployerContact.create! user: user, account: account, name: "Joe Smith"
    position = Position.create! employment_type: create(:employment_type), employer_contact: ec,
      job_title: "Ice Cream Taster", salary_range: SalaryRange.first_or_create!(name: "$0 - $20k"), location: location, city: city
    position.required_skill_list = "C++,Java,SAS"
    position2 = Position.create! employment_type: create(:employment_type), employer_contact: ec,
      job_title: "Ice Truck Driver", salary_range: SalaryRange.first_or_create!(name: "$0 - $20k"), location: location, city: city
    position2.required_skill_list = "C++,Java,SAS"
    position3 = Position.create! employment_type: create(:employment_type), employer_contact: ec,
      job_title: "Ice Truck Driver", salary_range: SalaryRange.first_or_create!(name: "$20k - $4a0k"), location: location, city: city
    position3.required_skill_list = "Ruby"
    [position, position2]
  end
end
