# == Schema Information
#
# Table name: positions
#
#  id                       :integer          not null, primary key
#  job_title                :string           not null
#  description              :text
#  hours                    :integer          default(0)
#  desired_years_experience :integer          default(0)
#  team_size                :string
#  employment_type_id       :integer          not null
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  employer_contact_id      :integer
#  remote                   :boolean          default(FALSE)
#  location_id              :integer
#  salary_range_id          :integer
#  city_id                  :integer
#  hiring_manager           :string
#  is_deleted               :boolean          default(FALSE)
#  deleted_at               :datetime
#
# Indexes
#
#  index_positions_on_city_id              (city_id)
#  index_positions_on_deleted_at           (deleted_at)
#  index_positions_on_employer_contact_id  (employer_contact_id)
#  index_positions_on_employment_type_id   (employment_type_id)
#

require 'rails_helper'

RSpec.describe Position, type: :model do
  before do
    TalentProfile.any_instance.stub(:get_matches)
    Position.any_instance.stub(:get_matches)
  end

  describe "factory" do
    let(:position) {build(:position)}

    it "should be valid" do
      expect(position).to be_valid
    end
  end

  describe "delete" do
    let(:position) { create(:position) }

    it "should change the is_deleted attribute to true" do
      expect(position.deleted_at).to be nil
      position.delete
      expect(position.deleted_at).to_not be nil
      expect(Position.only_deleted.count).to eq 1
    end
  end

  describe "destroy" do
    let(:position) { create(:position) }

    it "should change the is_deleted attribute to true" do
      expect(position.deleted_at).to be nil
      position.destroy
      expect(position.deleted_at).to_not be_nil
      expect(Position.only_deleted.count).to eq 1
    end

    it "should delete the associated matches" do
      create_list(:talent_profile_match, 3, position:position)
      expect(position.talent_profile_matches.count).to eq 3
      expect(TalentProfileMatch.count).to eq 3
      position.destroy
      expect(TalentProfileMatch.count).to eq 0
    end
  end

  describe "restore!" do
    let(:position) { create(:position) }

    it "should restore the position" do
      position.destroy
      expect(position.deleted_at).to_not be nil
      position.restore
      expect(position.deleted_at).to be nil
    end
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:job_title) }
    it { is_expected.to validate_presence_of(:employment_type) }
  end

  describe "associations" do
    it { is_expected.to have_many(:bonus_skills).through(:bonus_skill_associations) }
    it { is_expected.to have_many(:required_skills).through(:required_skill_associations) }
    it { is_expected.to belong_to(:location)}
    it { is_expected.to belong_to(:employer_contact)}
    it { is_expected.to have_many(:talent_profiles).through(:talent_profile_matches) }
    it { is_expected.to belong_to(:salary_range)}
    it { is_expected.to belong_to(:city) }
  end

  describe "#bonus_skill_list" do
    let(:position) {create(:position, :with_bonus_skill_list, number_of_bonus_skills: 2)}

    it "returns a string of bonus skills" do
      expect(position.bonus_skills.count).to eq 2
      expect(position.bonus_skill_list).to include(Skill.first.name)
    end

    it "should be able to set the bonus skill list" do
      position.bonus_skill_list="Rails,Angular"
      expect(position.bonus_skills.count).to eq 2
      expect(position.bonus_skill_list).to include("Rails")
    end
  end

  describe "#required_skill_list" do
    let(:position) {create(:position, :with_required_skill_list, number_of_skills: 2)}

    it "return a string of required skills" do
      expect(position.required_skills.count).to eq 2
      expect(position.required_skill_list).to include(Skill.first.name)
    end

    it "should be able to set the required skill list" do
      position.required_skill_list="Ruby,JavaScript"
      expect(position.required_skills.count).to eq 2
      expect(position.required_skill_list).to include("JavaScript")
    end
  end
end
