# == Schema Information
#
# Table name: vertical_markets
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  deleted_at :datetime
#
# Indexes
#
#  index_vertical_markets_on_deleted_at  (deleted_at)
#

require 'rails_helper'

RSpec.describe VerticalMarket, type: :model do
  let(:vertical_market) {build(:vertical_market)}

  it "factory should be valid" do
    expect(vertical_market).to be_valid
  end

  describe "associations" do
    it { is_expected.to have_many(:skill_categories) }
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:name) }
  end
end
