# == Schema Information
#
# Table name: invoices
#
#  id         :integer          not null, primary key
#  offer_id   :integer
#  name       :string           not null
#  category   :integer          not null
#  status     :integer          default(0), not null
#  amount     :decimal(, )      default(0.0), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  due_at     :datetime         not null
#  paid_at    :datetime
#  deleted_at :datetime
#
# Indexes
#
#  index_invoices_on_offer_id  (offer_id)
#

require 'rails_helper'

RSpec.describe Invoice, type: :model do

  describe 'factory' do
    let(:invoice) { build :invoice }

    it 'should be valid' do
      expect(invoice).to        be_valid
      expect(invoice.status).to eq('pending')
      expect(invoice.type).to   eq('upfront')
    end


    describe 'validations' do
      it { is_expected.to validate_presence_of(:name) }
      it { is_expected.to validate_numericality_of(:amount).is_greater_than(0) }
    end

    describe 'associations' do
      it { is_expected.to belong_to(:offer) }
    end
  end
end
