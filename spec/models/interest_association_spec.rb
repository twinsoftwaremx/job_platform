# == Schema Information
#
# Table name: interest_associations
#
#  id                :integer          not null, primary key
#  talent_profile_id :integer
#  interest_id       :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_interest_associations_on_interest_id        (interest_id)
#  index_interest_associations_on_talent_profile_id  (talent_profile_id)
#

require 'rails_helper'

RSpec.describe InterestAssociation, type: :model do
  describe "factory" do
    let(:interest_association) { build(:interest_association) }

    it "should be valid" do
      expect(interest_association).to be_valid
    end
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:talent_profile) }
    it { is_expected.to validate_presence_of(:interest) }
  end
end
