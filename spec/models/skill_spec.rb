# == Schema Information
#
# Table name: skills
#
#  id                :integer          not null, primary key
#  name              :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  skill_category_id :integer
#  deleted_at        :datetime
#
# Indexes
#
#  index_skills_on_deleted_at  (deleted_at)
#

require 'rails_helper'

RSpec.describe Skill, type: :model do
  let(:skill) {build(:skill)}

  it "factory should be valid" do
    expect(skill).to be_valid
  end

  describe "::by_category_list" do
    let(:skill_category1) { create(:skill_category, :with_skill_list, number_of_skills: 3) }
    let(:skill_category2) { create(:skill_category, :with_skill_list, number_of_skills: 2) }
    let(:skill_category3) { create(:skill_category, :with_skill_list, number_of_skills: 4) }

    it "should return the correct list of skills for each category" do
      skills = Skill.all
      category_list = "#{skill_category1.id}, #{skill_category2.id}"
      result = Skill.by_category_list(skills, category_list)
      expect(result.count).to eq 2
      expect(result[0].count).to eq 3
      expect(result[1].count).to eq 2
    end
  end
end
