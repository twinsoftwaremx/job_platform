# == Schema Information
#
# Table name: employment_types
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  deleted_at :datetime
#
# Indexes
#
#  index_employment_types_on_deleted_at  (deleted_at)
#

require 'rails_helper'

RSpec.describe EmploymentType, type: :model do
  let(:employment_type) { build(:employment_type) }

  it "factory should be valid" do
    expect(employment_type).to be_valid
  end
end
