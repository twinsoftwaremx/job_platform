# == Schema Information
#
# Table name: accounts
#
#  id                  :integer          not null, primary key
#  company_name        :string
#  phone               :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  summary             :text
#  benefits            :text
#  perks               :text
#  github_url          :string
#  company_site_url    :string
#  address_id          :integer
#  culture             :integer          default(0)
#  agent_id            :integer
#  company_size_id     :integer
#  year_founded        :integer
#  revenue             :string
#  who_we_are          :text
#  what_we_do          :text
#  video_url           :string
#  deleted_at          :datetime
#  billing_customer_id :string
#
# Indexes
#
#  index_accounts_on_company_size_id  (company_size_id)
#  index_accounts_on_deleted_at       (deleted_at)
#

require 'rails_helper'

RSpec.describe Account, type: :model do
  before do
    TalentProfile.any_instance.stub(:get_matches)
    Position.any_instance.stub(:get_matches)
  end

  describe "factory" do
    let(:account) {build(:account)}

    it "should be valid" do
      expect(account).to be_valid
    end
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:company_name) }
  end

  describe "associations" do
    it { is_expected.to belong_to(:address) }
    it { is_expected.to have_many(:employer_contacts) }
    it { is_expected.to have_many(:positions).through(:employer_contacts) }
    it { is_expected.to have_one(:logo).class_name('Photo') }
    it { is_expected.to belong_to(:agent) }
    it { is_expected.to belong_to(:company_size) }
  end

  def create_logo(base64_string)
    self.logo = Asset.new(type:'Photo')
    header, data = base64_string.split(',')
    file = Tempfile.new('logo')
    file.binmode
    tmp_file_name = file.path
    file.write(Base64.decode64(data))
    file.close
    file = File.open(tmp_file_name)
    self.logo.file = file
    file.close
  end

  describe "#create_logo" do
    let(:base64_encoded_string) { '
data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAD9JJREFUeNrsnT1sHMcVx3f54Q/GEonIQOT47LCxpCCNaMBKIdghYqQJIFhVWidOm8KGyxSK2iCAEyCtP9KmokAgQBA4OjlQYwHSsbJ0NpClRTsBQjony6Y+7mOzs7yTTwzJu9252Zl58/sBC8EwybudefOf9968mYkiuaQFnosF/u7Fgn97N8sFf3/Sz8UJfaflEn2ykD3/nFDf6PYDZEzRBFCSd/sDugivZ88iTYcAgP8s9gd0kZ8/R7MhACCHcwVm9HdpLgQAZIYCo/h5yZwBIADgOGpgnz3g/6s8wVs0EwIAsr2AhQPChAWaCAEAuSxEeyf4TkbFEoWAAICnvN4f8EXzA4AAgBDeGiEIgACAYJajnYz/fiEBOMYMTfAgVr1Y4Gd1SLLn/Bg/973+YCryd/805s+Z9gJeiUj8gWVSRx6d2dTUfgbdz7K9dyGK2AtACAAACAAAIAAAgAAAAAIAAAgAmGFF43ffo/kQAPCbP2RPo8TvtbLnDZoPAQD/+UXJ32nRdAgA+I/yAM4X+PkVzdABEABwjN+MGQq0SnoMgABUQj174jGfOs31EOPE9OcdcP11S5KXEQCAvcXz9xr/HxAA8Bw1wye4/ggAhMl+A30/YQAEAASGAu8N/XcD1x8BgLB4I/om2Yfr7wGcCAQmQoHFqFylICAA4DkU+xACAAACAAAIAACQA6iaeoGfbRj6WR1aBt9B97NaDvRN1UlGdjECAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAECVxC5+qY0zx97K/jlJ94AgGrXVpnNXo7t6IIga/MvYDIBZXD0SLKFrQBgJAjA+69gLCGMdAQAABGAMuFUGpNFAAMaHE1hBGi0EAACcInb1i22cOZbSPSCF2mrTybGGBwAQMAgAAALgJHW6B4RQRwAAAAEoAEuBIIUWAlCcNewGhOCsLc/QN2Ez922iwHG4ezuNem15K9MuC0CC2Zln+pGYRhiDOE5F2vIUAgAwmu59BAAAhOGyALAjEKTQQAAKUlttsgwIEtx/p22ZEACAEMBZ6nQR2CZN5dowHgDACHptPAAAQAAq5xJdBNY9gG4q1obxAABGCgAegC1YCgTfaSEA5aEYCOx7AJ1UrA0TAgCMIO0RAtgiwfzAc5y2Yef3gnI8uFkOHZ2mEQ5AlQFvf1HeBXD1OHBCAADwQgBIBIKvNBAAfVgKBIshgGzbJQQAIATAjQLYizSVWwbsiwDcwgzBWgjQlv1+HAt+AI++c1n0+83OzkaH5+dlz+Dbt6Pbr/8QY/bYA6jTTVCW3sZ1PQHR2wlYRwB8nj0+/ZhGCF1AurLfj2XAgwTgzleMAN8H8OZnQdquGAGorTZZBYDyArBVXgA0dwF6YbuEAIQAsF//9+S/oy8CkFj51G1CgJA9AC9tFgEAGJrFtz4v/buaZcAIgPezx42rNAIQAjgAiUAoN4vf/AibFSAAVsqBU3IA/ocA27dL/25H707AWwiA78bDKgAQAhACgKfuf/OK3gQg+E5A3wTAXjXg5r8YSYHSa8s/jpIQYJQAbP2bRvA2/v+SRpAgALXVZp2ugsIhwM3yOwF1NwH5YrN4AKNmERKBYfZ7N4zT6H0SADt5AJYC/Y3h7ZUBtxCAycNKABSbxXXKgDth2CohwKhZhBAg0I4P4zV9EoDEyqfeuc1g8BSLZcAJAjB51jFpKBQC2CsDXkcApHiC16/RCCAWn44FTyR2wL3XTtv77OyJuR3YBCQBRQmAwaXAqRNLDBcX8wd6IQDLgKLCgJusBIBMqAMA2IXmLkCvSte9EYCsUUXuCJw6/jwjzjWPr50G866EAOOwyY5AQABcoC6uB+aewApd8wC64dgoHsA4BmGwHDh+9jka2LUcQDecd/VNAOzkASgHBtdtNBABWJNmLfGRowwZx+jq3Qm4hgAQAowvAE8+RQO7FgL0wnlX3wQgsfKpHAoCrtsoAuBxGEAi0K0QQK8MGAEQGQbcMLcrMGYpEBCAcD0AEEUDATBEbbVpTwC2zS0FUg4sxv23WrJOCGCQ9NNPaAQgBMDFAtFCH8B9gL4LgBUXy+SOwJhDQZyh18YDgL0EYItLQgEBcIFL0johfpxlQGc8AL0rwS4hAFI9AINnAlAI5JIA4AGQA6g4BwBiaPn2hWc8bGSRqwCPvnO58s+cnZ2NDs/Pi2rHbvNK9PXvXi3vAejtBPTONgkBingB3BMov497Yb2vjwKQWDOOO+wKdD6G3/wsSNsMRgCslgOD+wKwVV4AJlAGjACIdg+5JxAIAcIOA8Btujev2/roBgIgXADSbQ4HdR6NA1y797U+uYUASA8BuCMQCAGcgB2BsCedGx+WF/g0rDJgnwXglhUPgCvCZOcP2uG9MyFAIQGgHNjp/iFHE4wA1Ok62E1vQ28FINXbCVhHAPACwGcB6Yb3zr4KgLUll3SLPICzA9huGTDLgFVRW22yCgD/LwAaZcCauwC9tUlCgKIeADsCZfZrL8z39lkA7Lhc3BPoLBbLgBMEoHoIA+Bh7JUBIwDBxJk3rtIIQAjgAAndBw+HAB/hjQYkAOs2PjQlB+AsOpWAHb3DQG4hAKEYGasAQAjgBCQB4Rv3v3lFT9gDuxNwwIzHfW6t8urea6dFDJp72RMfna7O2I6fiubefM/Jtui10yhECAGgOmOrnTAY/39JA4ckALXVZp3u84t47pC5EECjCEh3E5DPtogHAMGjuQ0YDyDEPACUywEYi+G3PsMGAxQAVgJgZxbf+rx8+NAJ1wYJAaA6Hj/k5vfqhdslhABQGdPPmFsFsFgGnCAA9lhjWEEeAtgrA15HAABGGdqRp2kEBAD3K1hDe9KcAFg8CEThdRJwBgEox6N//GsUzT1h5G+3f/urqMdNxONzR+8+AM1rwVkGDJEe9wQWMzSDZcAQrgBQB+AJJsuA9e4D1Pts30vSvRaArPHt3Q9g8IKQqePPoxhVeXLtNOj3JwQoCxeFFsJkGTB3AoYtAHVxvWIouSh2Ft+wthOwjgAEiskQIH72ORq4qn7shv3+EgTASh4g3eKS0CJMH3vBnAewyU7AkAVAXDlwfOQoilFEADS2Anf17gRcQwBCNTqDpwPHTz4lS9DmDjn73dJe2HZMCFAW7gcY3/1/5vvG/rblMuAEAbCPyGIgEoFjYrcMGAEIOgy4Ya5ePxa0FBgf+S7GggDghgVrZEdM7gT8yOarNRAAy9RWm/YEwGAFGuXA46FTBajp/lstRUcAXDC+Tz+hEcbA5FFggACIcMUkE88dNhcCaNwJGOp9gBIFwE41oMly4BNLKIdhem3aYIYm0BAAg+XAU888F8388tdRev1avtpgUmyMfP8jT0czJ1+OZo6/YLQMmDsBEQDFpexZFtUzc09E06d/GkXqyfMNH+dC0MsF4apzhUiq2i8f8MdORdPZoK/qEFC9OwFTXZtDAIL2ACo8E0AVBk2r5yc/2zFe5RVkYtC99kEuDlaM5/ipbND/OJvhT3mZ6Ot1sWEEwNEcwEgX+/hSFGXP9Cuv5d6A8gp6V/9hNFxQg1wN9tmll4269WMPYHv3ASpaCIA71LPnXLBKlIULU0sv5c9AmPJwIReE8uGCcuOVO6/ceuXeu7apR+c+wFxA9HYCNhAAyN1v1+r21W7CafUM5w+u7YjBQceNqwGuZvidxN0p8ev3oe8ElCQA9g4HvfNVFDveOA/yB9FQuNBfXZj+ziP9bP0pJ9z6IlguA04QAEeorTYbG2eOIecFw4XZ2dnoW/Pz/s7gdsuARQgApcC6RsgNPoAAOEFCd4aFxcNAGggAAqDthoImd3RCAK1PbiEAsCMA3BEICIATsCMwMPTuBKQMWJoA3LLiAXBFmJ/5A3YCEgJMRgC4IMRKu5N7QQB2Uac7w0HnPsBcQPR2AtYRAMAL8FlA2AmIAExMALbIA1Q+gDfZCTgJYklGsXHmWGrrs/Py2hNL2b8venO1lyoFnveoFFgV/nSbH0adxt+1VgDULsCvN8vvBKqtNmMEAAHYv1EzAVD79aeef3HneG9HL/lwXQDUfv/ujStRJx/0708s8af2AWx/gQAopG0HVq7Zggs5ga56Lv9lxztQnkEmBLl3wJVfBw/O5pWofe39fKa3fO/ffiSS2luaAKhioGXnZrLrO2f5RRfe3tmNp8Qg9w6WxN0EXGaWV7N7R8302b+ViMx9BECqALiP2o9/7YP8ycOFzCPIw4U8f/CS+NdXbrya3dvZYFfuveVjvYJHmgB4p87qtJ6uev72551wYZBMVN6BkHBBufJqdlfuvU7yziEvEwFwlHXvXeJh78CTZOJebr2J5N2k6OgdBnILAYBqvINdycT8aK/8NB/3komDON7h5B0EIACidwSqcKGj7gBwIJn4YE2+wuTdxNoxxcakCkArCoWKk4mSkne9doqNEQLI8w4mnUwcJO/yBB5uvUikVQKqIqD/0q27OnmfZOLuSsBB8i6f5bPZXuKWW7UJ6Ov/lN8JJKkKUJwA9EUgZciP6PR+MnH2By9Ehx/rBZW8owyYEIBwoZ9M7Fx4O5o5Ok2DBBr/56GiwE6qY6ewrwfQ0fr1BgIA4HUSgCaQLgAtuhUMkSAA7rOGncJ+aJYBryMAAIAA4KaBb6gSYMqAH2YGAQDJqHV/dQBI+16qWwKsaCEAAA6jKv0699L+wE+jlKx/cALAHYGBufWDwd65m5o+71+cbcUSjYJy4PE55GEloDrWu5O59WrAd+9X19XSyoAJAcCbWX4w2JV7j1uPAIzjqp2ke/1lMNjVTD+B5N0kqCMA/kA1oG9uPck7BADCcusrTN4xqQQmAJciBy8ICX6WV8m7uzvluFUm7ybEGgIAUNCtfzDLk7xDAHDX5ONg8m5SJAiAP6xkz6sRKwGVzPKD5J2K5YVSj4SuAsSSjbN/SOjZ7PlRPyewyJB9mKKFQIPk3WBd3oPkXdnZXk0iKpdUr602xXqUcUjGngnCyb4QvBKRJBxbADxP3o0bMtb7A34lG/BJKP0fh2z8mSAMvIOzoXoHewlAIMm7Rn/QX8gGfD3UMRC0AOwSg8Vd3sFCSALwII6Xl7wbnuVXhmZ5EsUIwMhw4WxfEMQmE2cei6Un7y7043h2iSIApcVgYZd3sEirOEkymOWzAb9CcyAAJr2D5aH8Adhz64dn+YQmQQBsCMLwUiO1B2ZpDA34Os2BALgmBotRgMlEw269Gugk7xAAb8MF8cnECTPs1pO8QwDEiAHJxP1neZJ3CECQ3sFyFF4ykeQdAgB7CILkZOIgebeCW48AwGgxWIz8TiYmu2Z5kncIAGiGC64nE4d30DHLIwBgSAxcSSYONtSQvEMAwLJ3sByZTyaSvEMAwANBmGQykeQdAgAei8FiVCyZmEQk7xAAEB0u7E4mDu+Tx60PiP8JMABBKA31QZfC4AAAAABJRU5ErkJggg==
' }
    let(:account) { create(:account, logo: nil) }

    it "should assign a valid logo file to the account" do
      expect(account.logo).to be nil
      account.create_logo(base64_encoded_string)
      expect(account.logo).to_not be nil
    end
  end

  describe "building up an account" do
    it "can build in steps" do
      attrs = attributes_for(:account)
      account = Account.new(attrs)
      expect(account.save).to eq(true)

      employer_contacts = create_list(:employer_contact, 3)
      account.employer_contacts = employer_contacts
      expect(account.employer_contacts.count).to eq 3

      positions_for_contact1 = create_list(:position, 5)
      employer_contacts.first.positions = positions_for_contact1
      expect(employer_contacts.first.positions.count).to eq 5

      positions_for_contact2 = create_list(:position, 3)
      employer_contacts.last.positions = positions_for_contact2
      expect(employer_contacts.last.positions.count).to eq 3

      expect(account.positions.count).to eq 8

      first_position = positions_for_contact1.first
      first_position.required_skill_list = 'Ruby,Rails'
      first_position.save
      expect(first_position.required_skills.count).to eq 2

      first_position.bonus_skill_list = 'JavaScript,MongoDB,TDD'
      first_position.save
      expect(first_position.bonus_skills.count).to eq 3
    end
  end
end
