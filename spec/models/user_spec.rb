# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  auth_token             :string           default("")
#  last_activity_at       :datetime
#  deleted_at             :datetime
#
# Indexes
#
#  index_users_on_auth_token            (auth_token) UNIQUE
#  index_users_on_deleted_at            (deleted_at)
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_last_activity_at      (last_activity_at)
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#

require 'rails_helper'

RSpec.describe User, type: :model do
  describe "associations" do
    it { is_expected.to have_one(:talent_profile).through(:talent) }
    it { is_expected.to have_many(:identities).dependent(:destroy) }
    it { is_expected.to have_one(:github_asset).dependent(:destroy) }
    it { is_expected.to have_one(:linkedin_asset).dependent(:destroy) }
    it { is_expected.to have_many(:messages_sent).class_name('Message').dependent(:destroy) }
    it { is_expected.to have_many(:messages_received).class_name('Message').dependent(:destroy) }
  end

  describe "validations" do
    it { is_expected.to validate_uniqueness_of(:auth_token) }
  end

  describe "#role" do
    it "should return the role name for this user" do
      user = create(:user)
      user.add_role :admin
      expect(user.role).to eq "admin"
    end

    it "should return the role names as a list if more than one role" do
      user = create(:user)
      user.add_role :account_admin
      user.add_role :employer_contact
      expect(user.role).to eq "account_admin employer_contact"
    end
  end

  describe "#role=" do
    it "should add role to the user in case is talent" do
      user = create(:user)
      expect(user.roles.count).to eq 0
      user.role = 'talent'
      expect(user.roles.count).to eq 1
      expect(user.has_role? :talent).to be_truthy
    end

    it "should add role to the user in case is employer_contact" do
      user = create(:user)
      expect(user.roles.count).to eq 0
      user.role = 'employer_contact'
      expect(user.roles.count).to eq 1
      expect(user.has_role? :employer_contact).to be_truthy
    end

    it "should add roles employer_contact and account_admin to the user in case is account_admin" do
      user = create(:user)
      expect(user.roles.count).to eq 0
      user.role = 'account_admin'
      expect(user.roles.count).to eq 2
      expect(user.has_role? :account_admin).to be_truthy
      expect(user.has_role? :employer_contact).to be_truthy
    end

    it "should not be allowed to add admin role" do
      user = create(:user)
      expect(user.roles.count).to eq 0
      user.role = 'admin'
      expect(user.roles.count).to eq 1
      expect(user.has_role? :admin).to be false
    end
  end

  describe "#generate_authentication_token!" do
    before(:all) do
      @user = build(:user)
    end

    it "generates a unique token" do
      allow(Devise).to receive(:friendly_token).and_return("randomtoken123?")
      @user.generate_authentication_token!
      expect(@user.auth_token).to eql("randomtoken123?")
    end

    it "generates another token when one already has been taken" do
      existing_user = FactoryGirl.create(:user, auth_token: "randomtoken123?")
      @user.generate_authentication_token!
      expect(@user.auth_token).not_to eql existing_user.auth_token
    end

    it "generates a token with length greater than 0" do
      allow(Devise).to receive(:friendly_token).and_return("randomtoken123?")
      @user.generate_authentication_token!
      expect(@user.auth_token.length).to be > 0
    end
  end

end
