# == Schema Information
#
# Table name: offers
#
#  id                      :integer          not null, primary key
#  talent_profile_match_id :integer          not null
#  employer_contact_id     :integer          not null
#  status                  :integer          default(0)
#  company_name            :string
#  position_title          :string
#  manager_name            :string
#  salary_pay_rate         :string
#  start_date              :string
#  benefits                :text
#  legal_verbiage          :text
#  hiring_managers_name    :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  details                 :text
#  job_type                :string
#  decline_reason          :text
#  deleted_at              :datetime
#  upfront_fee             :decimal(, )      default(0.0)
#  recurring_fee           :decimal(, )      default(0.0)
#
# Indexes
#
#  index_offers_on_deleted_at               (deleted_at)
#  index_offers_on_employer_contact_id      (employer_contact_id)
#  index_offers_on_talent_profile_match_id  (talent_profile_match_id)
#

require 'rails_helper'

RSpec.describe Offer, type: :model do
  before do
    TalentProfile.any_instance.stub(:get_matches)
    Position.any_instance.stub(:get_matches)
  end

  describe "factory" do
    let(:offer) { build(:offer) }

    it "should be valid" do
      expect(offer).to be_valid
      expect(offer.status).to eq("unanswered")
    end
  end

  describe "after_create" do
    let(:talent_profile_match) { create(:talent_profile_match, status: TalentProfileMatch.statuses[:interested]) }
    let(:offer) { build(:offer, talent_profile_match: talent_profile_match) }
    it "should change the status of the associated talent_profile_match" do
      expect(talent_profile_match.status).to eq 'interested'
      offer.save
      expect(talent_profile_match.status).to eq 'offer'
    end
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:talent_profile_match) }
    it { is_expected.to validate_presence_of(:employer_contact) }
    it { is_expected.to validate_presence_of(:company_name) }
    it { is_expected.to validate_presence_of(:position_title) }
  end

  describe "associations" do
    it { is_expected.to belong_to(:talent_profile_match) }
    it { is_expected.to belong_to(:employer_contact) }
  end
end
