require 'rails_helper'

describe WeeklyMailerHelper do
  before do
    TalentProfile.any_instance.stub(:get_matches)
    Position.any_instance.stub(:get_matches)
  end

  context '#job_details_link' do
    it "should return a valid link" do
      match = create(:talent_profile_match)
      user = create(:user)

      link = job_details_link(user.email, match.position.id, match.id)

      parameter_string = "user=#{user.email}&role=talent&screen=job_details&position_id=#{match.position.id}&match_id=#{match.id}"
      encoded_data = Base64.encode64(parameter_string).chomp!
      expect(link).to eq "#{ENV['FRONTEND_URL']}#/from_email?data=#{encoded_data}"
    end
  end
end
