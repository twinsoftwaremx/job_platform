require 'rails_helper'

RSpec.describe "Fill out profile", type: :request do

  before do
    @interest = create(:interest)
    @interested_location = create(:location)
    @company_size = create(:company_size)
  end

  describe 'fill out profile process' do
    it 'should let the user fill out the profile' do
      user_data = {
        email:"test@email.com",
        password:"password123?",
        password_confirmation:"password123?",
        role:"talent"
      }
      headers_without_auth = {
        "Accept":"application/vnd.fetchapi.v1+json",
        "Content-Type":"application/json"
      }
      # Signup new talent user
      post '/users', {user:user_data}.to_json, headers_without_auth
      expect(response.status).to eq(201)
      json_response = JSON.parse(response.body)
      auth_token = json_response['data']['attributes']['auth_token']

      # Add authorization token to headers
      headers_with_token = headers_without_auth
      headers_with_token['Authorization'] = ActionController::HttpAuthentication::Token.encode_credentials(auth_token)

      # Create the talent profile
      talent_profile_params = {
        'talent_profile': {
          'name': Faker::Name.name,
          'time_zone': 'CST',
          'phone': Faker::PhoneNumber.phone_number,
          'skill_list': 'ruby,javascript',
        },
        'location': {
          'line1': 'address line 1',
          'city': 'City',
          'state': 'State',
          'zip': '32001'
        }
      }.to_json
      post '/talent_profiles', talent_profile_params, headers_with_token
      json_response = JSON.parse(response.body)
      talent_profile_id = json_response['data']['id']
      talent_profile = json_response['data']['attributes']
      expect(json_response['data']['type']).to eq 'talent_profiles'
      expect(talent_profile_id).to_not be_nil

      # Create an interest for the talent profile
      interest_params = {
        'interest_association': {
          'interest_id': @interest.id
        }
      }.to_json
      post "/talent_profiles/#{talent_profile_id}/interests", interest_params, headers_with_token
      json_response = JSON.parse(response.body)
      expect(json_response['data']['type']).to eq 'interest_associations'
      expect(json_response['data']['id']).to_not be_nil

      # Create an interested location for the talent profile
      interested_location_params = {
        'location_of_interest_association': {
          'location_id': @interested_location.id
        }
      }.to_json
      post "/talent_profiles/#{talent_profile_id}/interested_locations", interested_location_params, headers_with_token
      json_response = JSON.parse(response.body)
      expect(json_response['data']['type']).to eq 'location_of_interest_associations'
      expect(json_response['data']['id']).to_not be_nil

      # Create a previous employment for the talent profile
      previous_employment_params = {
        'previous_employment': {
          'company': 'Company name',
          'position': 'Position name',
          'start_date': '01/01/2010',
          'end_date': '10/10/2014',
          'description': 'Previous employment description'
        }
      }.to_json
      post "/talent_profiles/#{talent_profile_id}/previous_employments", previous_employment_params, headers_with_token
      json_response = JSON.parse(response.body)
      expect(json_response['data']['type']).to eq 'previous_employments'
      expect(json_response['data']['id']).to_not be_nil

      # Create an education item for the talent profile
      education_item_params = {
        'education_item': {
          'college': 'College name',
          'year': '2008',
          'degree': 'Degree'
        }
      }.to_json
      post "/talent_profiles/#{talent_profile_id}/education_items", education_item_params, headers_with_token
      json_response = JSON.parse(response.body)
      expect(json_response['data']['type']).to eq 'education_items'
      expect(json_response['data']['id']).to_not be_nil

      # Create a preferred company size for the talent profile
      preferred_company_size_params = {
        'preferred_company_size_association': {
          'company_size_id': @company_size.id
        }
      }.to_json
      post "/talent_profiles/#{talent_profile_id}/pref_company_sizes", preferred_company_size_params, headers_with_token
      json_response = JSON.parse(response.body)
      expect(json_response['data']['type']).to eq 'preferred_company_size_associations'
      expect(json_response['data']['id']).to_not be_nil
    end
  end
end
